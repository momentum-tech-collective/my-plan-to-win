
-- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
    id uuid NOT NULL,
    name character varying(150) COLLATE pg_catalog."default" NOT NULL,
    email character varying(300) COLLATE pg_catalog."default" NOT NULL,
    public_key uuid NOT NULL,
    email_confirmed timestamp without time zone,
    visited bigint NOT NULL DEFAULT 0,
    revisited bigint NOT NULL DEFAULT 0,
    extra jsonb,
    mobile character(12) COLLATE pg_catalog."default",
    postcode character(8) COLLATE pg_catalog."default",
    can_contact boolean,
    canvassing_constituencies text COLLATE pg_catalog."default",
    canvassing_miles integer,
    gotv_constituency character(40) COLLATE pg_catalog."default",
    social_share_image_url character(500) COLLATE pg_catalog."default",
    avatar_url character(500) COLLATE pg_catalog."default",
    gotv_miles integer,
    driver_with_car boolean,
    one_time_code uuid,
    slug text UNIQUE,
    reminded_until date,
    CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


-- Index: idx_user_email

-- DROP INDEX public.idx_user_email;

CREATE UNIQUE INDEX idx_user_email
    ON public."user" USING btree
    (email COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: idx_user_public_key_id

-- DROP INDEX public.idx_user_public_key_id;

CREATE INDEX idx_user_public_key_id
    ON public."user" USING btree
    (public_key)
    TABLESPACE pg_default;

-- Table: public.category

-- DROP TABLE public.category;

CREATE TABLE public.category
(
    id uuid NOT NULL,
    name character varying(400) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    url character varying(400) COLLATE pg_catalog."default",
    size "char" NOT NULL,
    suppressed boolean,
    extra jsonb,
    max_count integer,
    CONSTRAINT category_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- Table: public.activity

-- DROP TABLE public.activity;

CREATE TABLE public.activity
(
    id uuid NOT NULL,
    date date NOT NULL,
    user_id uuid NOT NULL,
    category_id uuid NOT NULL,
    location character varying(50) COLLATE pg_catalog."default",
    extra jsonb,
    CONSTRAINT activity_pkey PRIMARY KEY (id),
    CONSTRAINT activity_category_id FOREIGN KEY (category_id)
        REFERENCES public.category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT activity_user FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- Index: idx_activity_category_id

-- DROP INDEX public.idx_activity_category_id;

CREATE INDEX idx_activity_category_id
    ON public.activity USING btree
    (category_id)
    TABLESPACE pg_default;

-- Index: idx_activity_user_id

-- DROP INDEX public.idx_activity_user_id;

CREATE INDEX idx_activity_user_id
    ON public.activity USING btree
    (user_id)
    TABLESPACE pg_default;


INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14e6bf0-111a-11ea-a78a-71ee5e611a62'::uuid, 'Go canvassing in a marginal', 'https://i.imgur.com/dBpI6J2.jpg', 'Our top priority: <a class="description-link" href="http://mycampaignmap.com" target="_blank">find a session</a> and go door-to-door ✊', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14e6bf1-111a-11ea-a78a-71ee5e611a62'::uuid, 'Book as much time off work as possible 🕛', 'https://i.imgur.com/crfee5M.jpg', 'Every hour matters for winning a Labour government', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14e6bf2-111a-11ea-a78a-71ee5e611a62'::uuid, 'Take a road trip to a marginal', 'https://i.imgur.com/3IKiq8l.jpg', 'Fill a car with friends and head to your highest-impact marginal, which may be further away  🚗', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14e6bf3-111a-11ea-a78a-71ee5e611a62'::uuid, 'Paint your campus red', 'https://i.imgur.com/9gV8ria.jpg', 'If you’re a student, flyer and poster your halls or campus with a group of friends 🖌️', 'b', 'TRUE');
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14e6bf4-111a-11ea-a78a-71ee5e611a62'::uuid, 'Ask 5 friends to make their Plan to Win', 'https://i.imgur.com/sM4ri4V.jpg', 'The more people plan out their time, url, the more ef, suppressedfective they will be. 💪', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba10-111a-11ea-a78a-71ee5e611a62'::uuid, 'Organise a phonebank party', 'https://i.imgur.com/YxMSwNa.jpg', '<a class="description-link" href="https://drive.google.com/file/d/1XxZWw8FszJKeFlxuG2AQjRFnrPnP9Fxm/view" target="_blank">Host a get-together</a> at home or in public to make phone calls for Labour ☎️', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba11-111a-11ea-a78a-71ee5e611a62'::uuid, 'Paint Your Town or Campus Red', 'https://i.imgur.com/VHC44eB.jpg', 'Reach newly-registered voters in marginal city-centres or campuses with <a class="description-link" href="https://docs.google.com/document/d/1rabUkkj0wqytNKRPtD1Q3yZMsNvRQRKdW0t8HCVvrDY/edit" target="_blank">posters, flyers and stickers 🖌️</a>', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba12-111a-11ea-a78a-71ee5e611a62'::uuid, 'Spend 2 hours phoning voters in marginals', 'https://i.imgur.com/8aJihJN.jpg', 'Use the <a class="description-link" href="https://dialogue.labour.org.uk/first-time-video" target="_blank">Dialogue App</a> 📞', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba13-111a-11ea-a78a-71ee5e611a62'::uuid, 'Attend a phonebank party near you', 'https://i.imgur.com/7RFcBfs.jpg', 'Find a phonebank party <a ref="https://www.mycampaignmap.com/" target="_blank">near you</a> and call voters in marginals 🎉', 'b', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26de5dd0-145e-11ea-ac95-afb5b367201d'::uuid, 'Door knock in a marginal', 'https://i.imgur.com/dBpI6J2.jpg', 'Our most important action: get voters in marginals out to polling stations 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26deabf0-145e-11ea-ac95-afb5b367201d'::uuid, 'Vote! 🗳️', 'https://i.imgur.com/9gV8ria.jpg', 'If you haven’t postal voted, get out as early as possible and vote! 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26deabf1-145e-11ea-ac95-afb5b367201d'::uuid, 'Remind all your friends to vote 🗳️', 'https://i.imgur.com/9gV8ria.jpg', 'Message and call as many friends as possible to make sure they’ve voted! 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26deabf2-145e-11ea-ac95-afb5b367201d'::uuid, 'Message 10 friends asking them to door knock 🗳️', 'https://i.imgur.com/sM4ri4V.jpg', 'Ask 10 friends to join you canvassing or to get people out to vote in another marginal 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26deabf3-145e-11ea-ac95-afb5b367201d'::uuid, 'Share on social media that you’ve voted 🗳️', 'https://i.imgur.com/9gV8ria.jpg', 'Let everyone know that you’ve voted 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('26deabf4-145e-11ea-ac95-afb5b367201d'::uuid, 'Join an early morning leaflet drop', 'https://i.imgur.com/dBpI6J2.jpg', 'Put leaflets reminding people to vote through letter-boxes in the early morning 🗳️', 'g', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba14-111a-11ea-a78a-71ee5e611a62'::uuid, 'Call 5 people you know and ask them to vote Labour 📞', 'https://i.imgur.com/8aJihJN.jpg', '', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba15-111a-11ea-a78a-71ee5e611a62'::uuid, 'Start a Let’s Go group', 'https://i.imgur.com/BTFm27h.jpg', '<a class="description-link" href="https://drive.google.com/file/d/1KIjNnJ0c5-rgkYlp4PwU9pi29bzvZqx7/view" target="_blank">Start a WhatsApp or email group</a> with friends and family and organise canvassing together ✊ ', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba16-111a-11ea-a78a-71ee5e611a62'::uuid, 'Tell us about your Let''s Go groups', 'https://i.imgur.com/mdGZ9Iy.jpg', 'Post your group <a class="description-link" href="https://www.mycampaignmap.com/" target="_blank">here</a> so others can join', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba17-111a-11ea-a78a-71ee5e611a62'::uuid, 'Message 10 friends asking them to vote Labour 🤝', 'https://i.imgur.com/sM4ri4V.jpg', '', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba18-111a-11ea-a78a-71ee5e611a62'::uuid, 'Attend a university event', 'https://i.imgur.com/9gV8ria.jpg', 'If you’re a student, attend a university event and talk to other students about Labour 🤝', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba19-111a-11ea-a78a-71ee5e611a62'::uuid, 'Share a selfie video 🤳', 'https://i.imgur.com/PdQIgXL.jpg', 'Make a <a class="description-link" href="https://www.videosbythemany.com/" target="_blank">Video By the Many</a> explaining why you’re voting for Labour.', 'm', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1a-111a-11ea-a78a-71ee5e611a62'::uuid, 'Donate to Momentum', 'https://i.imgur.com/9gV8ria.jpg', 'Donate <a class="description-link" href="https://momentum.nationbuilder.com/momentum_donate" target="_blank">here</a> 💸', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1b-111a-11ea-a78a-71ee5e611a62'::uuid, 'Share your Plan to Win 💪', 'https://i.imgur.com/9gV8ria.jpg', '', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1c-111a-11ea-a78a-71ee5e611a62'::uuid, 'Put a Labour poster up in your window', 'https://i.imgur.com/VHC44eB.jpg', '<a class="description-link" href="https://shop.labour.org.uk/category/ge2019-poster" target="_blank">Buy Labour posters</a> or get them from your local CLP 🧧', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1d-111a-11ea-a78a-71ee5e611a62'::uuid, 'Join Momentum''s Digital Army', 'https://i.imgur.com/9gV8ria.jpg', 'Help <a class="description-link" href="https://peoplesmomentum.com/digiarmy" target="_blank">Momentum’s online Digital Army</a> flood social media with election content 📱', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1e-111a-11ea-a78a-71ee5e611a62'::uuid, 'Join Momentum''s Activist Alerts', 'https://i.imgur.com/9gV8ria.jpg', 'Get campaign and canvassing alerts straight to your phone: <a class="description-link" href="https://api.whatsapp.com/send?phone=447494164535&text=I%27m%20in" target="_blank">Whatsapp link</a>', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba1f-111a-11ea-a78a-71ee5e611a62'::uuid, 'Post on social media explaining why you’re voting Labour 💻', 'https://i.imgur.com/9gV8ria.jpg', '', 's', FALSE);
INSERT INTO public.category (id, name, url, description, size, suppressed) VALUES ('b14eba20-111a-11ea-a78a-71ee5e611a62'::uuid, 'Spend 20 minutes calling voters in marginals', 'https://i.imgur.com/8aJihJN.jpg', 'Use the <a class="description-link" href="https://dialogue.labour.org.uk/first-time-video" target="_blank">Dialogue App</a> ', 's', FALSE);

UPDATE public.category set description = 'Our top priority: <a class="description-link"href="http://mycampaignmap.com" target="_blank">find a session</a> and go door-to-door ✊' WHERE id='b14e6bf0-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Find a phonebank party <a ref="https://www.mycampaignmap.com/" target="_blank">near you</a> and talk with voters in marginals 🎉' WHERE id='b14eba13-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Use the <a class="description-link"href="https://dialogue.labour.org.uk/first-time-video" target="_blank">Dialogue App</a> to speak with voters in marginals 📞' WHERE id='b14eba12-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = '<a class="description-link"href="https://drive.google.com/file/d/1XxZWw8FszJKeFlxuG2AQjRFnrPnP9Fxm/view" target="_blank">Host a get-together</a> at home or in public to make phone calls for Labour ☎️' WHERE id='b14eba10-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = '<a class="description-link"href="https://docs.google.com/document/d/1rabUkkj0wqytNKRPtD1Q3yZMsNvRQRKdW0t8HCVvrDY/edit" target="_blank">Hit a marginal</a> city-centre with posters, flyers and stickers and reach newly-registered young voters 🖌️' WHERE id='b14eba11-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = '<a class="description-link"href="https://drive.google.com/file/d/1KIjNnJ0c5-rgkYlp4PwU9pi29bzvZqx7/view" target="_blank">Start a WhatsApp or email Let’s Go group</a> with friends and family and organise canvassing together ✊' WHERE id='b14eba15-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'We can put your Let''s Go group <a class="description-link"href="https://www.mycampaignmap.com/" target="_blank">online</a> so others can join you 💪' WHERE id='b14eba16-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Make a <a class="description-link"href="https://www.videosbythemany.com/" target="_blank">Video By the Many</a> explaining why you’re voting for Labour. 🤳' WHERE id='b14eba19-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Use the <a class="description-link"href="https://dialogue.labour.org.uk/first-time-video" target="_blank">Dialogue App</a> to speak with voters in marginals 📞' WHERE id='b14eba20-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = '<a class="description-link"href="https://shop.labour.org.uk/category/ge2019-poster" target="_blank">Buy Labour posters</a> or get them from your local CLP 🧧' WHERE id='b14eba1c-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Help <a class="description-link"href="https://peoplesmomentum.com/digiarmy" target="_blank">Momentum’s online Digital Army</a> flood social media with election content 📱' WHERE id='b14eba1d-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Get campaign and canvassing alerts straight to your phone - <a class="description-link"href="https://api.whatsapp.com/send?phone=447494164535&text=I%27m%20in" target="_blank">open this on WhatsApp</a> 💪' WHERE id='b14eba1e-111a-11ea-a78a-71ee5e611a62';
UPDATE public.category set description = 'Donate <a class="description-link"href="https://momentum.nationbuilder.com/momentum_donate" target="_blank">here</a> 💸' WHERE id='b14eba1a-111a-11ea-a78a-71ee5e611a62';




-- spare uuids
--26deabf5-145e-11ea-ac95-afb5b367201d
--26ded300-145e-11ea-ac95-afb5b367201d
--26ded301-145e-11ea-ac95-afb5b367201d
--26ded302-145e-11ea-ac95-afb5b367201d
