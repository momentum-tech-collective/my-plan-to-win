import { IUser } from './api/db';
var fs = require("fs");
var path = require("path");
const util = require('util');
const promiseRead = util.promisify(fs.readFile);
const { handlePrivateGetUserByPublicKey } = require('./api/user/user.lib')
const { handleGetActivities } = require('./api/plan/plan.lib')

export const plural = (n: number, label: string, suffix: string = "s") =>
  n + " " + label + (n > 1 ? suffix : "");

export const customSocialSharing = async (req, res) => {
	try {
		// Content
		const { public_key } = req.params
		const user: IUser | null = await handlePrivateGetUserByPublicKey(public_key);
		const activities = await handleGetActivities(user.id);
		const title = `${user.name}'s Plan to Win`
		const description = `I've committed to ${plural(activities.length, 'action')} to help win a transformative Labour government by December 12. Make your Plan to Win now!`
		const facebookImage = `${process.env.SITE_URL}/share-card.png`
		const twitterImage = `${process.env.SITE_URL}/share-card.png`
		const canonicalUrl = `${process.env.SITE_URL}/plan/${user.slug || user.public_key}`

		// Meta
		const meta = `
			<meta property="og:title" content="${title}" />
			<title>${title}</title>
			<meta
				name="description"
				content="${description}"
			/>
			<meta property="og:url" content="${canonicalUrl}" />
			<meta
				property="og:description"
				content="${description}"
			/>
			<meta
				property="og:image"
				content="${user.social_share_image_url || facebookImage}"
			/>
			<meta property="og:type" content="article" />
			<meta name="twitter:card" content="summary_large_image">
			<meta name="twitter:title" content="${title}" />
			<meta
				name="twitter:description"
				content="${description}"
			/>
			<meta
				name="twitter:image"
				content="${user.social_share_image_url || twitterImage}"
			/>
			<meta name="twitter:site" content="${process.env.SITE_URL}" />
			<meta name="twitter:creator" content="@peoplesmomentum" />
			<meta name="twitter:site" content="@peoplesmomentum" />
		`.replace(/\n|\t|\s{2}/g, ' ')

		// HTML
		const index = (await promiseRead(path.join(__dirname,'../client/build/index.html'))).toString();
		const [first, after] = index.split('<meta property="og:title"')
		const [__, last] = after.split('@peoplesmomentum"/>')
		const html = first + meta + last

		res.send(html)
	} catch (e) {
		console.error(e)
		return res.sendFile(path.join(__dirname, "../client/build/index.html"));
	}
}