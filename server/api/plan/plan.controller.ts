import {handlePostActivity, handleGetActivities, handleDeleteActivity, handleCountAllActivity } from "./plan.lib"
import { IActivityCount } from "../db";

export const postActivity = async function(req, res) {
    handlePostActivity(req.body)
        .then(() => {
            res.status(200).send();
        })
        .catch((err) => {
            res.status(400).send(err);
            console.log(err);
        })
};

export const getActivities = async function(req, res) {
    handleGetActivities(req.params.id)
        .then((result) => {
            res.status(200).send(result)
        })
        .catch((err) => {
            res.status(400).send(err);
            console.log(err);
        })
};

export const deleteActivity = async function(req, res) {
    handleDeleteActivity(req.params.id)
        .then(() => {
            res.status(200).send();
        })
        .catch((err) => {
            res.status(404).send(err);
            console.log(err);
        })
};


export const countAllActivity = async function(req,res) {
    handleCountAllActivity()
      .then(count => {
        const body: IActivityCount = { count };

        res.status(200).send(body)
      })
      .catch(err => {
        res.status(400).send(err);
        console.log(err);
      })
}   