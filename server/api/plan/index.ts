import {postActivity, getActivities, deleteActivity, countAllActivity} from './plan.controller'

export const doRouting = function(app) {
    app.post("/activity", postActivity);
    app.get("/activities/:id", getActivities);
    app.get("/activity/count", countAllActivity);
    app.delete("/activity/:id", deleteActivity);
};
