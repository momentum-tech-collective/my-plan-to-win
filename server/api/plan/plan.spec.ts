import {postActivity, getActivities, deleteActivity, countAllActivity} from "./plan.controller"
import {handlePostActivity} from "./plan.lib"
import {anonymousUser, client, IActivity, uuid, uuid1} from "../db";

const category_id = 'b14e6bf0-111a-11ea-a78a-71ee5e611a62';

const assert = require('assert');

describe('Activities', function () {

    describe('Querying and deleting', function () {

        const id: uuid = uuid1();
        const public_key: uuid = uuid1();
        let anId: uuid;

        before(function () {
            anId = uuid1();
            return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                VALUES ('${id}', 'John Smith', 'john@smith.com', '${public_key}');`)
                .then(() => {
                    return client.query(`INSERT INTO public.activity(id, date, user_id, category_id)
                    VALUES
                    ('${uuid1()}', '2019-12-11', '${id}', '${category_id}'),
                    ('${anId}',    '2019-12-10', '${id}', '${category_id}'),
                    ('${uuid1()}', '2019-12-09', '${id}', '${category_id}');`)
                })
        });

        after(function () {
            return client.query(`DELETE from public."activity" where user_id = '${id}';`)
                .then(() => {
                    return client.query(`DELETE from public."user" where id = '${id}';`);
                })
        });

        it('Returns the activities from private key and updates user revisited count', function (done) {
            const req = {
                params: {id}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (results) {
                    assert.strictEqual(results.length, 3);
                    client.query(`SELECT revisited FROM public."user" where id = '${id}'`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, 1);
                            assert.strictEqual(result.rows[0].revisited, "1");
                            done();
                        });
                }
            };
            getActivities(req, res);
        });


        it('Returns the activities from public key and updates user visited count', function (done) {
            const req = {
                params: {id: public_key}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (results) {
                    assert.strictEqual(results.length, 3);
                    client.query(`SELECT visited FROM public."user" where public_key = '${public_key}'`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, 1);
                            assert.strictEqual(result.rows[0].visited, "1");
                            done();
                        });
                }
            };
            getActivities(req, res);
        });

        it('deletes an activity', function(done) {

            function countActivities(): Promise<number> {
                return client.query(`SELECT count(*) FROM public."activity" where user_id = '${id}'`)
                    .then((result) => parseInt(result.rows[0].count, 10));
            }

            const req = {
                params: {
                    id: anId
                }
            };
            const res = {
                status: function(code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function() {
                    countActivities().then(countAfter => {
                        assert.strictEqual(countAfter, 2);
                        done();
                    })
                }
            };
            countActivities().then(count => {
                assert.strictEqual(count, 3);
                deleteActivity(req, res)
            });
        });
    });

    describe('Saving', function () {

        describe('with an existing user', function () {

            const id: uuid = uuid1();
            const user_id: uuid = uuid1();

            beforeEach(function () {
                return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                VALUES ('${user_id}', 'John Smith', 'john@smith.com', '${uuid1()}');`);
            });

            afterEach(function () {
                return client.query(`DELETE from public."activity" where id = '${id}';`)
                    .then(() => {
                        return client.query(`DELETE from public."user" where id = '${user_id}';`)
                    });
            });

            it('Should add an activity', function (done) {
                let activity = {
                    id,
                    date: new Date(2019, 11, 11).toISOString(),
                    category_id,
                    user_id,
                    extra: {
                        just: 'some',
                        stuff: 333
                    }
                };
                const req = {
                    body: activity
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function() {
                        client.query(`SELECT * FROM public."activity" where id = '${id}'`)
                            .then(results => {
                                assert.strictEqual(results.rows.length, 1);
                                assert.strictEqual(results.rows[0].extra.just, 'some');
                                assert.strictEqual(results.rows[0].extra.stuff, 333);
                                done();
                            })
                            .catch((err) => {
                                assert(false, `Err ${err}`)
                            });
                    }
                };
                postActivity(req, res);
            });

            it('Should handle missing date by raising error', function () {
                let activity = {
                    id: uuid1(),
                    category_id,
                    user_id: anonymousUser
                };
                return handlePostActivity(activity as IActivity)
                    .then(() => {
                        assert.fail('Fail - should not be able to write the activity')
                    })
                    .catch((err) => {
                        assert.strictEqual(err.message, 'Date must be provided');
                    })
            });

        });

        describe('with a new user', function () {

            const id: uuid = uuid1();
            const user_id: uuid = uuid1();

            afterEach(function () {
                return client.query(`DELETE from public."activity" where id = '${id}';`)
                    .then(() => {
                        return client.query(`DELETE from public."user" where id = '${user_id}';`)
                    });
            });

            it('Should add an activity', function (done) {
                let activity = {
                    id,
                    date: new Date(2019, 11, 11).toISOString(),
                    category_id,
                    user_id
                };
                const req = {
                    body: activity
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function() {
                        done();
                    },
                };
                postActivity(req, res);
            });

        });

    });

    describe("Count all activity",function() {
        const id: uuid = uuid1();
        const public_key: uuid = uuid1();
        let anId: uuid;

        before(function () {
            anId = uuid1();
            return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                VALUES ('${id}', 'John Smith', 'john@smith.com', '${public_key}');`)
                .then(() => {
                    return client.query(`INSERT INTO public.activity(id, date, user_id, category_id)
                    VALUES
                    ('${uuid1()}', '2019-12-11', '${id}', '${category_id}'),
                    ('${anId}',    '2019-12-10', '${id}', '${category_id}'),
                    ('${uuid1()}', '2019-12-09', '${id}', '${category_id}');`)
                })
        });

        after(function () {
            return client.query(`DELETE from public."activity" where user_id = '${id}';`)
                .then(() => {
                    return client.query(`DELETE from public."user" where id = '${id}';`);
                })
        });

        it("Should count all activity",  function(done) {
            const req = {
                params: {}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (body) {                   
                    assert.strictEqual(body.count, 3);
                    client.query(`SELECT * FROM public."activity"`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, body.count);
                            done();
                        });
                }
            };
            countAllActivity(req,res);
        })
    })

});
