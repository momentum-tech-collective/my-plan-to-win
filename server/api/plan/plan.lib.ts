import {anonymousUser, client, IActivity, uuid} from "../db"
import {handleUserType, selectOrInsertUser} from "../user/user.lib";

export async function handlePostActivity(activity: IActivity): Promise<null | Error> {
    // Check if user exists.
    if (!activity.user_id) {
        throw new Error('User id must be provided')
    } else if (!activity.date) {
        throw new Error('Date must be provided');
    } else {
        return selectOrInsertUser(activity.user_id)
            .then(() => {
                let dateObj = new Date(activity.date);
                let fields = ['id', 'date', 'user_id', 'category_id'];
                let values = [activity.id, dateObj.toISOString().slice(0, 10), activity.user_id || anonymousUser, activity.category_id];
                if (activity.location) {
                    fields.push('location');
                    values.push(activity.location);
                }
                if (activity.extra) {
                    fields.push('extra');
                    values.push(JSON.stringify(activity.extra));
                }
                let queryString = 'INSERT INTO public.activity (' + fields.join(', ') +
                    ') values (\'' + values.join('\', \'') + '\')';
                return client.query(queryString);
            })
    }
}

export async function handleGetActivities(id: uuid): Promise<IActivity[] | Error> {
    let userInfo: any = {};
    let user_id = id;
    let visitedField = 'revisited';
    return handleUserType(id, userInfo)
        .then((result) => {
            if (result === 'public') {
                user_id = userInfo.id;
                visitedField = 'visited';
            }
            return client.query(`UPDATE public."user" set ${visitedField}=${visitedField}+1 where id = '${user_id}'`)
                .then(() => {
                    return client.query(`SELECT * FROM public."activity" where user_id = '${user_id}'`)
                        .then((result) => {
                            return result.rows
                        });
                })
        });
}

export async function handleDeleteActivity(id: uuid): Promise<null | Error> {
    return client.query(`DELETE from public."activity" where id = '${id}'`)
}


export async function handleCountAllActivity(): Promise<number>  {
    return client
        .query('select count(*) from public."activity"')
        .then((result) => {
            const count = parseInt(result.rows[0].count)

            return count
        })
}