import {client, uuid, uuid1} from "../db";
import {
    addFields,
    getUser,
    getUserByPublicKey,
    usertype
} from "./user.controller";

const assert = require("assert");

const id: uuid = uuid1();
const public_key: uuid = uuid1();
const onetimekey: uuid = uuid1();
const name = "John Smith";
const email = "john@smith.com";

describe("Users", function () {

    describe("GET", function () {

        beforeEach(function () {
            return client.query(`INSERT INTO public."user"(id, name, email, public_key, one_time_code)
                VALUES ('${id}', '${name}', '${email}', '${public_key}', '${onetimekey}');`);
        });

        afterEach(function () {
            return client.query(`DELETE from public."user" where id = '${id}';`);
        });

        describe('usertype', function () {

            it('usertype - converts valid one time key to user id', function (done) {
                const req = {
                    params: {
                        uuid: onetimekey
                    }
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function (string) {
                        assert.strictEqual(id, string);
                        done();
                    }
                };
                usertype(req, res);
            });

            it('handles email confirm', function (done) {
                const req = {
                    params: {
                        uuid: onetimekey
                    }
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function (string) {
                        assert.strictEqual(id, string);
                        client.query(`SELECT * from public."user" where id = '${id}'`)
                            .then(result => {
                                assert.strictEqual(result.rows.length, 1);
                                assert.notStrictEqual(result.rows[0].email_confirmed, null);
                                done();
                            });
                    }
                };
                usertype(req, res);
            });

            it('removes one time code', function (done) {
                const req = {
                    params: {
                        uuid: onetimekey
                    }
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function (string) {
                        assert.strictEqual(id, string);
                        client.query(`SELECT * from public."user" where id = '${id}'`)
                            .then(result => {
                                assert.strictEqual(result.rows.length, 1);
                                assert.strictEqual(result.rows[0].one_time_code, null);
                                done();
                            });
                    }
                };
                usertype(req, res);
            });

            it('Returns public when the public key is passed', function (done) {
                const req = {
                    params: {uuid: public_key}
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 200);
                        return this;
                    },
                    send: function (string) {
                        assert.strictEqual("public", string);
                        done();
                    }
                };
                usertype(req, res);
            });

            it("Returns 404 if the key is not found", function (done) {
                const req = {
                    params: {uuid: uuid1()}
                };
                const res = {
                    status: function (code) {
                        assert.strictEqual(code, 404);
                        return this;
                    },
                    send: function () {
                        done();
                    }
                };
                usertype(req, res);
            });
        });
    });

    describe("return record", function () {

        beforeEach(function () {
            return client.query(`INSERT INTO public."user"(id, name, email, public_key, one_time_code)
                VALUES ('${id}', '${name}', '${email}', '${public_key}', '${onetimekey}');`);
        });

        afterEach(function () {
            return client.query(`DELETE from public."user" where id = '${id}';`);
        });

        it("returns a record if id is found", function (done) {
            const req = {
                params: {id: id}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (result) {
                    assert.strictEqual(result.id, id);
                    assert.strictEqual(result.name, name);
                    assert.strictEqual(result.email, email);
                    assert.strictEqual(result.public_key, public_key);
                    done();
                }
            };
            getUser(req, res);
        });

        it("returns 404 if id is not found", function (done) {
            const req = {
                params: {id: uuid1()}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 404);
                    return this;
                },
                send: function () {
                    done();
                }
            };
            getUser(req, res);
        });
    });

    describe("return public users", function () {

        beforeEach(function () {
            return client.query(`INSERT INTO public."user"(id, name, email, public_key, one_time_code)
                VALUES ('${id}', '${name}', '${email}', '${public_key}', '${onetimekey}');`);
        });

        afterEach(function () {
            return client.query(`DELETE from public."user" where id = '${id}';`);
        });

        it("returns a record if id is found", function (done) {
            const req = {
                params: {id: public_key}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (result) {
                    assert.strictEqual(result.name, name);
                    assert.strictEqual(result.public_key, public_key);
                    done();
                }
            };
            getUserByPublicKey(req, res);
        });

        it("hides sensitive information", function (done) {
            const req = {
                params: {id: public_key}
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (result) {
                    assert.strictEqual(result.id, undefined);
                    assert.strictEqual(result.email, undefined);
                    done();
                }
            };
            getUserByPublicKey(req, res);
        });
    });
});

describe("PATCH", function () {
    before(function () {
        return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                VALUES ('${id}', '${name}', '${email}', '${public_key}');`);
    });

    after(function () {
        return client.query(`DELETE from public."user" where id = '${id}';`);
    });

    it("patches an existing record", function (done) {
        const req = {
            params: {id},
            body: {
                visited: 340,
                driver_with_car: true,
                extra: {
                    some: "attribute",
                    number: 278362
                }
            }
        };
        const res = {
            status: function (code) {
                assert.strictEqual(code, 200);
                client
                    .query(`SELECT * FROM public."user" where id = '${id}'`)
                    .then(results => {
                        assert.strictEqual(results.rows.length, 1);
                        assert.strictEqual(results.rows[0].visited, "340");
                        assert.strictEqual(results.rows[0].driver_with_car, true);
                        assert.strictEqual(results.rows[0].extra.some, "attribute");
                        assert.strictEqual(results.rows[0].extra.number, 278362);
                        done();
                    })
                    .catch(err => {
                        assert(false, `Err ${err}`);
                    });
                return this;
            },
            send: function () {
            }
        };
        addFields(req, res);
    });

    it("returns 400 if you omit data", function (done) {
        const req = {
            params: {id}
        };
        const res = {
            status: function (code) {
                assert.strictEqual(code, 400);
                return this;
            },
            send: function () {
                done();
            }
        };
        addFields(req, res);
    });

    it("returns 404 for a record not found", function (done) {
        const req = {
            params: {id: uuid1()},
            body: {visited: 340}
        };
        const res = {
            status: function (code) {
                assert.strictEqual(code, 404);
                return this;
            },
            send: function (string) {
                assert.strictEqual("Record not found", string);
                done();
            }
        };
        addFields(req, res);
    });
});
