import { client, IUser, uuid, uuid1 } from "../db";

export async function handleUserType(token: uuid, userInfo?: any) : Promise<string | null> {
    return client.query(`SELECT id, slug, public_key, one_time_code, email_confirmed FROM public."user" where one_time_code = '${token}' or public_key = '${token}'`)
    .then((result) => {
        if (result.rows.length === 1){
            let id = result.rows[0].id;
            if (result.rows[0].one_time_code === token) {
                let query = 'UPDATE public."user" set ';
                // say the email has been confirmed
                if (result.rows[0].email_confirmed === null) {
                    query += `email_confirmed = '${new Date().toISOString()}', `
                }
                query += 'one_time_code=NULL';
                query += ` where id = '${id}'`;
                return client.query(query)
                    .then(() => id);
            } else if (result.rows[0].public_key === token) {
                // Pass the user id back if anyone said they wanted it
                if (userInfo) {
                    userInfo.id = id;
                }
                return 'public';
            }
        } else {
            return null;
        }
    })
}

export async function selectOrInsertUser(id: uuid): Promise<null> {
  return client
    .query(`SELECT id FROM public."user" where id = '${id}'`)
    .then(result => {
      switch (result.rows.length) {
        case 1:
          return null;
        case 0:
          let public_key: uuid = uuid1();
          return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                        VALUES ('${id}', 'unknown', 'unknown-${id}', '${public_key}');`);
        default:
          throw new Error(`Multiple users with id ${id}`);
      }
    });
}

export async function patchUser(userAttributes: any) : Promise<any> {
    let query = 'UPDATE public."user" SET ';
    let connective = '';
    for (let attr in userAttributes) {
        if (userAttributes.hasOwnProperty(attr) && attr !== 'id') {
            if (attr === 'extra') {
                query += `${connective}${attr}='${JSON.stringify(userAttributes[attr])}'`;
            } else {
                query += `${connective}${attr}='${userAttributes[attr]}'`;
            }
            connective = ', ';
        }
    }
    query += ` WHERE id='${userAttributes.id}'`;
    return client.query(query);
}

export async function findByEmail(email: string): Promise<IUser | null> {
    return client.query(`SELECT * from public."user" where email = '${email}'`)
        .then((results) => {
            return results.rows.length > 0 ? results.rows[0] : null;
        })
}

export async function handleGetUser(id: uuid): Promise<any> {
  return client
    .query(`SELECT * FROM public."user" where id = '${id}'`)
    .then(result => result.rows[0]);
}

export async function handleGetUserByPublicKey(id: uuid): Promise<IUser> {
	try {
		const keyedUsers = await client.query(`SELECT name, public_key, slug FROM public."user" where public_key = '${id}'`)
		if (keyedUsers.rows.length > 0) {
			return keyedUsers.rows[0]
		}
	} catch (e) {
		const sluggedUsers = await client.query(`SELECT name, public_key, slug, avatar_url FROM public."user" where slug = '${id}'`)
		if (sluggedUsers.rows.length > 0) {
			return sluggedUsers.rows[0]
		}
	}
}

export async function handlePrivateGetUserByPublicKey(id: uuid): Promise<IUser> {
	try {
		const keyedUsers = await client.query(`SELECT * FROM public."user" where public_key = '${id}'`)
		if (keyedUsers.rows.length > 0) {
			return keyedUsers.rows[0]
		}
	} catch(e) {
		const sluggedUsers = await client.query(`SELECT * FROM public."user" where slug = '${id}'`)
		if (sluggedUsers.rows.length > 0) {
			return sluggedUsers.rows[0]
		}
	}
}