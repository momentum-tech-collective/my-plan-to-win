import {
  usertype,
  getUser,
  addFields,
  getUserByPublicKey
} from "./user.controller";

export const doRouting = function(app) {
  app.get("/usertype/:uuid", usertype);
  app.get("/user/:id", getUser);
  app.get("/publicuser/:id", getUserByPublicKey);
  app.patch("/user/:id", addFields);
};
