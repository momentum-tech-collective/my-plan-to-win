import {
  handleUserType,
  handleGetUser,
  patchUser,
  handleGetUserByPublicKey
} from "./user.lib";

export const usertype = async function(req, res) {
    void handleUserType(req.params.uuid)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(404).send(err);
        })
};

export const getUser = async function(req, res) {
    void handleGetUser(req.params.id)
        .then(result => {
            if (result) {
                res.status(200).send(result);
            } else {
                res.status(404).send("User not found");
            }
        })
        .catch((err) => {
            res.status(400).send("Error " + err);
    });
};

export const getUserByPublicKey = async function(req, res) {
  void handleGetUserByPublicKey(req.params.id)
    .then(result => {
      if (result) {
        res.status(200).send(result);
      } else {
        res.status(404).send("User not found");
      }
    })
    .catch(err => {
      res.status(400).send("Error " + err);
    });
};

export const addFields = async function(req, res) {
    if (req.body) {
        req.body.id = req.params.id;
        void patchUser(req.body)
            .then(result => {
                if (result.rowCount === 1) {
                    res.status(200).send();
                } else {
                    res.status(404).send("Record not found");
                }
            })
      .catch(err => {
        res.status(400).send("Error " + err);
      });
  } else {
    res.status(400).send("Requests needs a body");
  }
};
