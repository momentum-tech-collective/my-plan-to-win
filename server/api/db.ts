const pg = require('pg-then');
const uuidv1 = require('uuid/v1');

export const anonymousUser = '00000000-0000-0000-0000-000000000000';
export type uuid = string;
export function uuid1(): uuid {
    return uuidv1();
}

export interface IActivityCount {
    count: number;
}
export interface IActivity {
    id: uuid;
    date: Date;
    category_id: uuid;
    user_id: uuid;
    location?: string;
    extra?: string;
}

export interface IUser {
    id: uuid;
    name: string;
    email: string;
    public_key: uuid;
    email_confirmed? :boolean;
    visited?: number;
    revisited?: number;
    extra?: any;
    mobile?: string;
    postcode?: string;
    can_contact?: boolean;
    canvassing_constituencies?: string;
    canvassing_miles?: number;
    gotv_constituency?: string;
    gotv_miles?: number;
    driver_with_car?: boolean;
	one_time_code: string;
	slug?: string;
	social_share_image_url?: string;
	avatar_url?: string;
}

export interface ICategory {
    id: uuid;
    name: string;
    description? : string;
    url? : string;
    size: 'b' | 'm' | 's' | 'g';
    suppressed: boolean;
    max_count: number | null;
}

export const pgClient = function() {
    const pgOptions: any = {
        connectionString: process.env.DATABASE_URL
    };
    if (!pgOptions.connectionString.includes('@localhost')) {
        pgOptions.ssl = true;
    }
    return pg.Client(pgOptions);
};

export const client = pgClient();

