const controller = require("./category.controller");

export const doRouting = function(app) {
    app.get("/categories", controller.categories);
};
