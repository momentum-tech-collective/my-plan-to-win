import {categories} from './category.controller';

var assert = require('assert');

describe('Categories', function() {

    it('Returns categories', function(done) {
        const res = {
            status: function (code) {
                assert.strictEqual(code, 200);
                return this;
            },
            send: function(categories) {
                assert(categories.length >= 18);
                done();
            }
        };
        categories({}, res);
    });

});
