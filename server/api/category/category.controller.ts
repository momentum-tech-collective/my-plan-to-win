import {handleCategories} from "./category.lib"

export const categories = async function(req, res) {
    void handleCategories()
        .then((result) => {
            res.send(result.rows)
        })
        .catch((err) => {
            res.status(404).send(`Error ${err}`);
        })
};
