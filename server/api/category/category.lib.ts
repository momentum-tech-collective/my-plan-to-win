import {client} from "../db"

export async function handleCategories() : Promise<any> {
    return client.query(`SELECT * FROM public."category"`)
}
