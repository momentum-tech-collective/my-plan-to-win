import {findByEmail, patchUser, handleGetUser} from '../user/user.lib';
import {uuid1, client} from "../db";

const fs = require('fs');
const util = require('util');
const path = require('path');

export const sgMail = require('@sendgrid/mail');
export const promiseRead = util.promisify(fs.readFile);

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

type NotFoundHandling = 'throw' | 'retain' | 'remove';

// A very simple template handler that replaces things between $ symbols with variables
// Please do not replace it with something more sophisticated that you are familiar with
// without running the tests
export function doReplace(input: string, replacements, onNotFound: NotFoundHandling = "throw" ) : string {
    let array = input.split('$');
    for (let i=1; i<array.length; i+=2) {
        let newText = replacements[array[i]];
        if (newText || newText === '') {
            array[i] = newText;
        } else if (onNotFound === "throw") {
            throw new Error(`Replacement text for ${array[i]} not found\nProcessing ${input}`)
        } else if (onNotFound === "remove") {
            array[i] = '';
        } else {
            // retain - put the delimiiters back in
            array[i] = `$${array[i]}$`
        }
    }
    return array.join('');
}

export async function constructHTMLEmail(filename: string): Promise<string> {
    return Promise.all([
        promiseRead(path.join(__dirname,'emails/_header.html')),
        promiseRead(path.join(__dirname,`emails/${filename}.html`))
    ])
        .then(results =>
`<!doctype html>
<html>
${results[0]}
${results[1]}
</html>`);
}

export async function handleSendPollingDayEmail(pollingDayInfo, testMailer): Promise<any> {
    return constructHTMLEmail('pollingday')
        .then((data) => {
            pollingDayInfo.subject = 'Thank you for your polling day pledge!';
            let html = doReplace(data.toString(), pollingDayInfo);
            let mailer = testMailer || sgMail;

            const msg = {
                to: pollingDayInfo.email,
                from: 'info@peoplesmomentum.com',
                subject: pollingDayInfo.subject,
                text: `
Dear ${pollingDayInfo.firstName}

THANKS SO MUCH FOR PLEDGING TO GO OUT ON POLLING DAY!

The 12th of December is the most important day of this election. We need hundreds of activists in every city, town and 
village across the country to help get out the vote for Labour.

By going to ${pollingDayInfo.constituencyName} you will be heading to a marginal that needs you most.

To make sure you have the biggest impact on polling day make sure you:

  1. Spend as much time as you can between now and the 12th in ${pollingDayInfo.constituencyName} — getting to know the area
     will make a big difference.
 
  2. Plan to spend as long as you can in ${pollingDayInfo.constituencyName} on the 12th — we'll need people there from first 
     thing until polling stations close at 10 pm.

Ask one friend to join you — pick someone who wouldn't go out by themselves!

Labour is surging. The polls are tightening. Together, we can win this.

In solidarity,

Team Momentum

PS CHECK OUT OUR CANVASSING RESOURCES at https://organise.peoplesmomentum.com/canvassing
`,
                html
            };
            mailer.send(msg);
        })
}

export async function handleSendConfirmEmail(userInfo, testMailer): Promise<any> {

    return findByEmail(userInfo.email)
        .then(async (user) => {
            if (user) {
                throw new Error(`You already have a plan - revisit at ${process.env.SITE_URL}/login`);
            } else {
                const oneTimeCode = uuid1();
                userInfo.one_time_code = oneTimeCode;
				const user = await handleGetUser(userInfo.id);
                const subject = 'You’ve almost completed your Plan to Win!';
                return patchUser(userInfo)
                    .then(() =>
                        constructHTMLEmail('confirm')
                            .then((data) => {
                                let html = doReplace(data.toString(), {
									...userInfo,
									...user,
									one_time_code: oneTimeCode,
									SITE_URL: process.env.SITE_URL,
                                    subject
								});
                                let mailer = testMailer || sgMail;

                                const msg = {
                                    to: userInfo.email,
                                    from: 'info@peoplesmomentum.com',
                                    subject,
                                    text: `
Hi

Thanks you for using My Plan to Win.

To confirm your email please visit ${process.env.SITE_URL}/sign-in/${oneTimeCode}

At any point you can go and add to your Plan to Win by visiting ${process.env.SITE_URL}/login.

Let's Make Change Happen!

Momentum`,
                                    html
                                };
                                mailer.send(msg);
                            })
                    );
            }
        })
}

export async function handleLoginEmail(email, testMailer): Promise<any> {

    let subject = 'Log back in to My Plan To Win';
    return findByEmail(email)
        .then((user) => {
            if (user) {
                let oneTimeCode = uuid1();
                return patchUser({id: user.id, one_time_code: oneTimeCode})
                    .then(() =>
                        constructHTMLEmail('login')
                            .then((data) => {
                                let html = doReplace(data.toString(),{
                                    one_time_code: oneTimeCode,
                                    SITE_URL: process.env.SITE_URL,
                                    subject,
                                    id: user.id
                                });
                                let mailer = testMailer || sgMail;

                                const msg = {
                                    to: email,
                                    from: 'info@peoplesmomentum.com',
                                    subject,
                                    text: `
Hi

Thanks you for using My Plan to Win.

To continue with your plan just visit ${process.env.SITE_URL}/sign-in/${oneTimeCode}

in Solidarity,

Momentum


Don't like these emails? <a href="http://www.myplantowin2019.com/unsubscribe/${user.id}">Unsubscribe</a>.
`,
                                    html
                                };
                                mailer.send(msg);
                            }));
            } else {
                throw new Error('Email not on file');
            }
        });
}

export async function handleUnsubscribe(id: string): Promise<boolean> {
    return client.query(`UPDATE public."user" set can_contact=FALSE where id = '${id}'`)
        .then(result => result.rowCount === 1 )
}
