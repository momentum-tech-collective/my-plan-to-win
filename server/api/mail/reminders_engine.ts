import {client} from '../db'
import {sgMail, constructHTMLEmail, doReplace} from "./mail.lib";

const cardinals = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen'];

export type ReminderOptions = {
    limit?: number;
    selectionCrit?: string;
    verbose?: boolean;
    inhibit_update?: boolean;
    include_unconfirmed? : boolean;
};

export async function sendReminders(opts?: ReminderOptions, mailer?: any): Promise<void> {
    opts = opts || {};
    let today = new Date();
    let date = `${today.getFullYear()}-${today.getMonth() + 1}-${('0' + today.getDate()).slice(-2)}`;
    let days = 12 - today.getDate();
    let dayText = `We're just ${cardinals[days - 1]} days from polling day.`;
    let dayBanner = 'My_Plan_to_Win_Email';
    switch (days) {
        case 0:
            dayText = 'Polling day is here';
            dayBanner = 'Encouragement-This-Is-It';
            break;
        case 1:
            dayText = 'Polling day is tomorrow';
            dayBanner = 'Encouragement-Do-Everything';
            break;
        case 2:
            dayBanner = 'Encouragement-We-all-need-to-step-up';
            break;
        case 3:
            dayBanner = 'Encouragement-This-Is-It';
            break;
        case 4:
            dayBanner = 'Encouragement-It-all-comes-down-to-this';
            break;
        case 5:
            dayBanner = 'Encouragement-Final-Weekend';
            break;
        case 6:
            dayBanner = 'Encouragement-This-Is-It';
            break;
        case 7:
            dayText = 'Just one week to go!';
            dayBanner = 'Encouragement-One-week-to-go';
            break;
    }

    let qry = `
        select
            activist.id,
            activist.name,
            activist.public_key,
            email,
            category.description,
            category.size,
            category.name as "task_name"
        from
             activity inner join 
             public."user" as activist on activist.id = activity.user_id inner join
             category on category.id = activity.category_id
        where ${opts.selectionCrit || ''}
             ${opts.include_unconfirmed ? '' : 'email_confirmed is not null and'}
            (can_contact is null or can_contact = true) and
            email not like 'unknown-%' and
            (reminded_until is null or reminded_until < '${date}') and
             date = '${date}'
        order by activist.id, category.size
        `;
        if (opts.limit) {
            qry += `LIMIT(${opts.limit})`;
        }
        qry += ';';
    if (opts.verbose) { console.log('******* Main Query *****\n', qry)}
    return Promise.all([
        client.query(qry),
        client.query('select count(*) as people from public."user" where email_confirmed is not null')
    ])
        .then(results => {
            let allActs = results[0].rows;
            let userCount = results[1].rows[0].people;
            return sendReminderEmails(opts, date, allActs, dayText, dayBanner, userCount, mailer)
                .then(() => {if (opts.verbose) {console.log( 'returning from sendReminders' )}})
        })
}

export async function sendReminderEmails(opts: ReminderOptions, date, allActs: any[], dayText: string, dayBanner: string, userCount, mailer: any) : Promise<any> {
    let lastActivist: string = null;
    let activities: string[] = [];
    let userBlobs: (string[])[] = [];
    for (const row of allActs) {
        if (row.id !== lastActivist) {
            if (lastActivist) {
                userBlobs = userBlobs.concat([[].concat(activities)]);
                activities.length = 0;
            }
            lastActivist = row.id;
        }
        activities.push(row);
    }
    if (activities.length > 0) {
        if (userBlobs.length > 0) {
            userBlobs = userBlobs.concat([activities]);
        } else {
            userBlobs = userBlobs.concat([[].concat(activities)]);
        }
    }

    return require('bluebird').map(userBlobs, async userActivities => {
        return sendReminderEmail(opts, date, userActivities, dayText, dayBanner, userCount, mailer);
    }, {concurrency: 5})
        .then(() => {
            if (opts.verbose) { console.log('Returning from sendreminder emails')}
        })
}

export async function sendReminderEmail(opts: ReminderOptions, date: string, activities: any[], dayText: string, dayBanner: string, usersCount: number, mailer: any) : Promise<any> {

    let activityCountText = `${activities.length} task${activities.length > 1 ? 's' : ''}`;
    let text = [
        dayText,
        ``,
        `This is it. We have a once-in-a-generation opportunity to transform Britain for the many.`,
        `You’ve signed up to do ${activityCountText} today – you’re a legend!`,
        `You’ve committed to do the following:`,
        ``
    ];

    const user = activities[0];
    if (opts.verbose) { console.log(`Generating email for ${user.email}`)}
    let quests = '';
    activities.forEach(a => {
        text = text.concat([
            a.task_name,
            a.description.replace(/<[^>]*>?/gm, ''),
            ''
        ]);
        quests += `
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="ask ask-${a.size}">
            <tbody>
            <tr>
                <td align="left">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td>
                            <div class="ask-name">${a.task_name}</div>
                            <div class="ask-desc">${ a.description.replace(/<[^>]*>?/gm, '') }</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

    `
    });
    text = text.concat([
        'Have you shared your Plan to Win?',
        '',
        'Sharing is crucial to make sure as many people plan their time our as possible.',
        '',
        `Every day before the election is absolutely crucial now. You’re joining an incredible ${usersCount} people who are carrying out their Plan to Win across the country.`,
        '',
        'Every one of us is a part of this incredible mass movement that is going to transform our society for the many.',
        '',
        'You’re making history – good luck!',
        '',
        'In solidarity',
        'Team Momentum ✊'
    ]);

    return constructHTMLEmail('reminder')
        .then((data) => {
            let html = doReplace(data.toString(), {
                activityCountText,
                SITE_URL: process.env.SITE_URL,
                subject: dayText,
                subjectUpperCase: dayText.toUpperCase(),
                preheader: 'Your pledged activities for the day',
                banner: dayBanner,
                public_key: user.public_key,
                id: user.id,
                quests,
                usersCount
            });

            const msg = {
                to: user.email,
                from: 'info@peoplesmomentum.com',
                subject: dayText,
                text: text.join('\n'),
                html
            };

            mailer = mailer || sgMail;
            if (opts.inhibit_update) {
                return mailer.send(msg)
            } else {
                return client.query(`update public."user" set reminded_until = '${date}' where id = '${user.id}'`)
                    .then(() => mailer.send(msg))
            }
        })
}




