import {sendConfirmEmail, sendLoginEmail, sendPollingDayEmail, unsubscribe} from "./mail.controller"

export const doRouting = function(app) {
    app.post("/send-confirm-email", sendConfirmEmail);
    app.post("/polling-day-email", sendPollingDayEmail);
    app.get("/email-login/:email", sendLoginEmail);
    app.get("/unsubscribe/:uuid", unsubscribe);
};

