import {handleSendConfirmEmail, handleLoginEmail, handleUnsubscribe, handleSendPollingDayEmail} from './mail.lib'

export const sendConfirmEmail = function(req, res) {
    handleSendConfirmEmail(req.body, req.mailer)
        .then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            res.status(400).send(err.message);
    })
};

export const sendPollingDayEmail = function(req, res) {
    handleSendPollingDayEmail(req.body, req.mailer)
        .then(() => {
            res.status(200).send('OK');
        }).catch((err) => {
            res.status(400).send(err.message);
    })
};

export const sendLoginEmail = async function(req, res) {
    // Check email on file.  If so generate a one time code and send it to the email.  If not send 404.
    handleLoginEmail(req.params.email, req.mailer)
        .then(() => {
            res.status(200).send()
        })
        .catch((err) => {
            res.status(404).send(err.message);
        })
};

export const unsubscribe = function(req, res) {
    handleUnsubscribe(req.params.uuid)
        .then((result) => {
            if (result) {
                res.status(200).send('OK');
            } else {
                res.status(404).send('Could not find subscriber ' + req.params.uuid);
            }
        });
};

