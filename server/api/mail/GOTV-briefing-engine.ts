import {client} from '../db'
import {sgMail, constructHTMLEmail, doReplace} from "./mail.lib";

export type GOTVOptions = {
    limit?: number;
    selectionCrit?: string;
    verbose?: boolean;
    include_unconfirmed? : boolean;
};

export async function sendBriefings(opts?: GOTVOptions, mailer?: any): Promise<void> {
    opts = opts || {};
    let qry = `
        select
            gotv_person.name, 
            constituency.name as constituency, 
            gotv_person.email, 
            constituency.start_time,
            constituency.address,
            
            constituency.whatsapp_url
        from
            gotv_person inner join
            constituency on gotv_person.constituencyid = constituency.constituencyid`;
        if (opts.selectionCrit) {
            qry += ' where ' + opts.selectionCrit + ' 1=1 ';
        }
        if (opts.limit) {
            qry += ` LIMIT(${opts.limit})`;
        }
        qry += ';';
    if (opts.verbose) { console.log('******* Main Query *****\n', qry)}
    return client.query(qry)
        .then(result => {
            return sendBriefingEmails(opts, result.rows, mailer)
                .then(() => {if (opts.verbose) {console.log( 'returning from sendReminders' )}})
        })
}

// export async function sendConstituencyEmails(opts: GOTVOptions, data, mailer: any) : Promise<any> {
//     let lastActivist: string = null;
//     let activities: string[] = [];
//     let userBlobs: (string[])[] = [];
//     for (const row of data) {
//         if (row.id !== lastActivist) {
//             if (lastActivist) {
//                 userBlobs = userBlobs.concat([[].concat(activities)]);
//                 activities.length = 0;
//             }
//             lastActivist = row.id;
//         }
//         activities.push(row);
//     }
//     if (activities.length > 0) {
//         if (userBlobs.length > 0) {
//             userBlobs = userBlobs.concat([activities]);
//         } else {
//             userBlobs = userBlobs.concat([[].concat(activities)]);
//         }
//     }
//
//     return require('bluebird').map(userBlobs, async userActivities => {
//         return sendCLPEmail(opts, data, mailer);
//     }, {concurrency: 5})
//         .then(() => {
//             if (opts.verbose) { console.log('Returning from sendreminder emails')}
//         })
// }

export async function sendBriefingEmails(opts: GOTVOptions, data, mailer: any) : Promise<any> {
    return require('bluebird').map(data, async row => {
        return sendBriefingEmail(opts, row, mailer);
    }, {concurrency: 2})
        .then(() => {
            if (opts.verbose) { console.log('Returning from sendreminder emails')}
        })
}

export async function sendBriefingEmail(opts: GOTVOptions, data: any, mailer: any) : Promise<any> {

    if (opts.verbose) { console.log(`Generating email for ${data.email}`)}
    let startTime = new Date(data.start_time);
    data.start_time = `${('0' + startTime.getHours()).slice(-2,9)}:${('0' + startTime.getMinutes()).slice(-2,9)}`;
    data.firstName = data.name.split(' ')[0];
    data.subject = 'Your Polling Day Pledge';
    if (data.whatsapp_url) {
        data.whatsappText = ['','Join the WhatsApp group at ' + data.whatsapp_url];
		data.whatsappHTML = `
			<div style='margin: 1em auto; text-align: center;'>
				<a href='${data.whatsapp_url}' target="_blank" style='display: inline-block; padding: 12px; background: #E20613; display: inline-block; border-radius: 4px; color: white; text-decoration: none; text-align: center; margin: 0 auto; font-weight: bold; font-size: 1.2em; margin: 0 auto;'>
					Join the WhatsApp group 💬
				</a>
			</div>
		`;
    } else {
        data.whatsappText = [];
        data.whatsappHTML = '';
    }
    let text = [
`Dear ${data.firstName}`,
'🗳THANKS SO MUCH FOR PLEDGING TO GO OUT ON POLLING DAY!🗳️',
'',
'To win this election for Labour, we need to ensure maximum voter turnout!',
`If you haven’t heard directly from an organiser in $constituency$ (the marginal you’ve`,
'signed up to head to for polling day) please head to the address detailed below:','',
'Campaign HQ Address: ' + data.address,
'',
'Campaign HQ opening time: ' + data.start_time,
''
        ];
    if (data.whatsappText) {
        text = text.concat(data.whatsappText)
    }
    text = text.concat([
'',
'The time between now and the closing of the polls is the most crucial of our whole campaign. Everything ',
'has been building up to this. We need hundreds of activists in every city, town and village across the ',
'country to help get out the Labour vote.',
'',
'Labour is surging. The polls have tightened. Together, we can win this.',
'',
'In solidarity,',
'Team Momentum'
    ]);

    data.address = data.address.split(',').join('<br />');
    return constructHTMLEmail('GOTV-briefing')
        .then((theHTML) => {
            let html = doReplace(theHTML.toString(), data);
            const msg = {
                to: data.email,
                from: 'info@peoplesmomentum.com',
                subject: 'Your Polling Day Pledge',
                text: text.join('\n'),
                html
            };

            mailer = mailer || sgMail;
            return mailer.send(msg)
        })
}
export async function sendCLPEmail(opts: GOTVOptions, data: any, mailer: any) : Promise<any> {
    return false
    // let activityCountText = `${activities.length} task${activities.length > 1 ? 's' : ''}`;
    // let text = [
    //     dayText,
    //     ``,
    //     `This is it. We have a once-in-a-generation opportunity to transform Britain for the many.`,
    //     `You’ve signed up to do ${activityCountText} today – you’re a legend!`,
    //     `You’ve committed to do the following:`,
    //     ``
    // ];
    //
    // const user = activities[0];
    // if (opts.verbose) { console.log(`Generating email for ${user.email}`)}
    // If there is a WhatsApp group for the marginal the link will appear here (Please add yourself by clicking the link): ✨[WHATSAPP LINK]✨
    // let quests = '';
    // activities.forEach(a => {
    //     text = text.concat([
    //         a.task_name,
    //         a.description.replace(/<[^>]*>?/gm, ''),
    //         ''
    //     ]);
    //     quests += `
    //     <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="ask ask-${a.size}">
    //         <tbody>
    //         <tr>
    //             <td align="left">
    //                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
    //                     <tbody>
    //                     <tr>
    //                         <td>
    //                         <div class="ask-name">${a.task_name}</div>
    //                         <div class="ask-desc">${ a.description.replace(/<[^>]*>?/gm, '') }</div>
    //                         </td>
    //                     </tr>
    //                     </tbody>
    //                 </table>
    //             </td>
    //         </tr>
    //         </tbody>
    //     </table>
    //
    // `
    // });
    // text = text.concat([
    //     'Have you shared your Plan to Win?',
    //     '',
    //     'Sharing is crucial to make sure as many people plan their time our as possible.',
    //     '',
    //     `Every day before the election is absolutely crucial now. You’re joining an incredible ${usersCount} people who are carrying out their Plan to Win across the country.`,
    //     '',
    //     'Every one of us is a part of this incredible mass movement that is going to transform our society for the many.',
    //     '',
    //     'You’re making history – good luck!',
    //     '',
    //     'In solidarity',
    //     'Team Momentum ✊'
    // ]);
    //
    // return constructHTMLEmail('GOTV-briefing')
    //     .then((data) => {
    //         let html = doReplace(data.toString(), {
    //             activityCountText,
    //             SITE_URL: process.env.SITE_URL,
    //             subject: dayText,
    //             subjectUpperCase: dayText.toUpperCase(),
    //             preheader: 'Your pledged activities for the day',
    //             banner: dayBanner,
    //             public_key: user.public_key,
    //             id: user.id,
    //             quests,
    //             usersCount
    //         });
    //
    //         const msg = {
    //             to: user.email,
    //             from: 'info@peoplesmomentum.com',
    //             subject: dayText,
    //             text: text.join('\n'),
    //             html
    //         };
    //
    //         mailer = mailer || sgMail;
    //         if (opts.inhibit_update) {
    //             return mailer.send(msg)
    //         } else {
    //             return client.query(`update public."user" set reminded_until = '${date}' where id = '${user.id}'`)
    //                 .then(() => mailer.send(msg))
    //         }
    //     })
}




