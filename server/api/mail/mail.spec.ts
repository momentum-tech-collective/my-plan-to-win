import {client, uuid, uuid1} from "../db";
import {sendConfirmEmail, sendLoginEmail, sendPollingDayEmail, unsubscribe} from "./mail.controller";
import {generalisedEmail, generalisedEmails} from "./mail_engine";
import {sendReminderEmail, sendReminderEmails, sendReminders} from "./reminders_engine";

const assert = require('assert');

const user_id: uuid = uuid1();
const name = 'John Smith';
const email = 'john@smith.com';

const bcat = 'b14eba12-111a-11ea-a78a-71ee5e611a62';  // Spend 2 hours phoning voters in marginals
const mcat = 'b14eba14-111a-11ea-a78a-71ee5e611a62';  // Call 5 people you know and ask them to vote Labour
const scat = 'b14eba1a-111a-11ea-a78a-71ee5e611a62';  // Donate to Momentum

function checkTextFound(searchFor: string, searchIn: string): boolean {
    const match = RegExp(searchFor).exec(searchIn);
    if (!match) {
        console.log('Could not find string ', searchFor);
    }
    return !!match;
}

describe('Mailing', function () {

    before(function () {
        return client.query(`
        delete from activity where 1=1;
        delete from public."user" where 1=1
        `)
    });

    describe('handles email login', function () {

        before(function () {
            return client.query(`INSERT INTO public."user" (id, name, email, public_key)
                VALUES ('${user_id}', '${name}', '${email}', '${uuid1()}');`)
        });

        after(function () {
            return client.query(`DELETE from public."user" where id = '${user_id}';`)
        });

        it('generates email and one time code if email on file', function (done) {
            let mailDone = false;
            let oneTimeCode: string;
            const req = {
                params: {
                    email
                },
                mailer: {
                    send: function (msg) {
                        assert.strictEqual(msg.to, email);
                        assert.strictEqual(msg.subject, 'Log back in to My Plan To Win');
                        let match = /\.com\/sign-in\/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/.exec(msg.text);
                        assert.strictEqual(match.length, 2);
                        oneTimeCode = match[1];
                        mailDone = true;
                    }
                }
            };
            const res = {
                status: function (status) {
                    assert.strictEqual(status, 200);
                    return this;
                },
                send: function () {
                    client.query(`SELECT * from public."user" where email = '${email}'`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, 1);
                            assert.strictEqual(result.rows[0].one_time_code, oneTimeCode);
                            assert(mailDone);
                            done();
                        });
                }
            };
            sendLoginEmail(req, res);
        });

        it('handles invalid email', function (done) {
            const req = {
                params: {
                    email: 'notonfile@nowhere.com'
                },
                mailer: {
                    send: function () {
                        assert(false, 'Mailer should not be called');
                    }
                }
            };
            const res = {
                status: function (status) {
                    assert.strictEqual(status, 404);
                    return this;
                },
                send: function (err) {
                    assert.strictEqual(err, 'Email not on file');
                    done();
                }
            };
            sendLoginEmail(req, res);
        })

    });

    describe('sends confirmation email', function () {

        before(function () {
            return client.query(`INSERT INTO public."user"(id, name, email, public_key)
                VALUES ('${user_id}', 'unknown', 'unknown-${user_id}', '${uuid1()}');`);
        });

        after(function () {
            return client.query(`DELETE from public."user" where id = '${user_id}';`)
        });

        it('handles an email not on file', function (done) {
            let mailDone = false;
            let oneTimeCode: string;
            const req = {
                body: {
                    id: user_id,
                    name,
                    email
                },
                mailer: {
                    send: function (msg) {
                        assert.strictEqual(msg.to, email);
                        assert.strictEqual(msg.subject, 'You’ve almost completed your Plan to Win!');
                        assert.strictEqual(msg.to, email);
                        let match = /\.com\/sign-in\/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/.exec(msg.text);
                        assert.strictEqual(match.length, 2);
                        oneTimeCode = match[1];
                        mailDone = true;
                    }
                }
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (status) {
                    assert.strictEqual(status, 'OK');
                    client.query(`SELECT email, name, one_time_code FROM public."user" where id = '${user_id}'`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, 1);
                            assert.strictEqual(result.rows[0].email, email);
                            assert.strictEqual(result.rows[0].name, name);
                            assert.strictEqual(result.rows[0].one_time_code, oneTimeCode);
                            assert(mailDone);
                            done();
                        });
                }
            };
            // update user with name and email and generate email (email 1) to user with a confirm button that links to /confirm-email/{uuid}.
            sendConfirmEmail(req, res);
        })

    });

    describe('barfs on a duplicate email address', function () {

        const original_id = uuid1();

        before(function () {
            return client.query(`
                INSERT INTO public."user"(id, name, email, public_key)
                VALUES 
                    ('${original_id}', '${name}', '${email}', '${uuid1()}'),
                    ('${user_id}', 'unknown', 'unknown-${user_id}', '${uuid1()}');`
            );
        });

        after(function () {
            return client.query(`
                DELETE from public."user" where id in ('${original_id}', '${user_id}');
                `);
        });

        it('barfs on a duplicate', function (done) {
            const req = {
                body: {
                    id: user_id,
                    name,
                    email
                }
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 400);
                    return this;
                },
                send: function (status) {
                    assert(status.includes(`You already have a plan - revisit at ${process.env.SITE_URL}/login`));
                    done();
                }
            };
            sendConfirmEmail(req, res);
        })

    });

    describe('sends reminders', function () {

        const tripleThreat = [
            {
                id: 'b8d014a0-145d-11ea-95ca-7320a9b658dd',
                public_key: '99999999-143b-11ea-8196-278c027a4ed9',
                name: 'chloe',
                email: 'c@b.com',
                task_name: 'Go canvassing in a marginal',
                description: 'Our top priority: <a class="description-link" href="http://mycampaignmap.com" target="_blank">find a session</a> and go door-to-door ✊"',
                size: 'b'
            },
            {
                id: 'b8d014a0-145d-11ea-95ca-7320a9b658dd',
                public_key: '99999999-143b-11ea-8196-278c027a4ed9',
                name: 'chloe',
                email: 'c@b.com',
                task_name: 'Start a Let’s Go group',
                description: '"<a class="description-link" href="https://drive.google.com/file/d/1KIjNnJ0c5-rgkYlp4PwU9pi29bzvZqx7/view" target="_blank">Start a WhatsApp or email Let’s Go group</a> with friends and family and organise canvassing together ✊"',
                size: 'm'
            },
            {
                id: 'b8d014a0-145d-11ea-95ca-7320a9b658dd',
                public_key: '99999999-143b-11ea-8196-278c027a4ed9',
                name: 'chloe',
                email: 'c@b.com',
                task_name: `Join Momentum's Activist Alerts`,
                description: `Get campaign and canvassing alerts straight to your phone - <a class="description-link" href="https://api.whatsapp.com/send?phone=447494164535&text=I%27m%20in" target="_blank">open this on WhatsApp</a> 💪`,
                size: 's'
            }
        ];

        const id1 = uuid1();
        const id2 = uuid1();
        const id3 = uuid1();
        const public_key = uuid1();

        before(function () {
            let today = new Date();
            let date = `${today.getFullYear()}-${today.getMonth() + 1}-${('0' + today.getDate()).slice(-2)}`;
            let qry = `
INSERT INTO public."user"(id, name, email, public_key, email_confirmed) 
VALUES 
('${id3}','Bill Unconfirmed', 'bill@nill.com', '${public_key}', NULL),
('${id1}','John Smith',       'john@smith.com','${public_key}', '${new Date().toISOString()}'),
('${id2}','Ann Jones',        'ann@jones.com', '${public_key}', '${new Date().toISOString()}');`;
            return client.query(qry)
                .then(() => {
                    qry = `INSERT INTO public.activity(id, date, user_id, category_id)
                    VALUES
                    ('${uuid1()}', '${date}', '${id1}',  '${bcat}'),
                    ('${uuid1()}', '${date}', '${id1}',  '${mcat}'),
                    ('${uuid1()}', '${date}', '${id3}',  '${bcat}'),
                    ('${uuid1()}', '${date}', '${id2}',  '${bcat}'),
                    ('${uuid1()}', '${date}', '${id2}',  '${mcat}'),
                    ('${uuid1()}', '${date}', '${id2}',  '${scat}');`;
                    return client.query(qry);
                })
                .catch((err) => {
                    console.log('Setup error', err);
                })
        });

        afterEach(() => {
            return client.query(`UPDATE public."user" set reminded_until = NULL where id in ('${id1}','${id2}','${id3}');`)
        });

        after(function () {
            return client.query(`DELETE from public."activity" where user_id in ('${id1}','${id2}','${id3}');`)
                .then(() => {
                    return client.query(`DELETE from public."user" where id in ('${id1}','${id2}','${id3}');`);
                })
        });

        it('sends a single reminder', function (done) {

            let dayText = `WE'RE JUST FIVE DAYS FROM POLLING DAY`;
            sendReminderEmail({}, '2019-12-12', [].concat(tripleThreat), dayText, 'My_Plan_to_Win_Email', 1234, {
                    send: async function (msg) {
                        // await require('util').promisify(require('fs').writeFile)('reminder.html', msg.html);
                        assert.strictEqual(msg.to, tripleThreat[0].email);
                        assert.strictEqual(msg.subject, dayText);
                        assert(/Go canvassing in a marginal/.exec(msg.text), 'could not find');
                        assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                        assert(!msg.html.includes('$'), 'Something failed in template replacement\n\nmsg.html');
                        assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                        done()
                    }
                }
            )
        });

        it('sends multiple reminders', function (done) {
            let dayText = `WE'RE JUST FIVE DAYS FROM POLLING DAY`;
            let emailCount = 0;
            // force two emails
            let data = [].concat(tripleThreat);
            data[0].id = uuid1();
            data[0].name = 'Ann';
            data[0].email = 'ann@jones.com';
            sendReminderEmails({}, '2019-12-12', data, dayText, 'My_Plan_to_Win_Email', 1000, {
                    send: function (msg) {
                        emailCount += 1;
                        switch (emailCount) {
                            case 1:
                                assert.strictEqual(msg.to, data[0].email);
                                assert.strictEqual(msg.subject, dayText);
                                assert(checkTextFound('Go canvassing in a marginal', msg.text));
                                assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                                assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                                assert(!msg.html.includes('$'), 'Something failed in template replacement\n\nmsg.html');
                                break;
                            case 2:
                                assert.strictEqual(msg.to, data[1].email);
                                assert.strictEqual(msg.subject, dayText);
                                assert(checkTextFound('Start a Let’s Go group', msg.text));
                                assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                                assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                                assert(!msg.html.includes('$'), 'Something failed in template replacement\n\nmsg.html');
                                done();
                                break;
                        }
                    }
                }
            )
        });

        it('drives the reminders', function (done) {
            let emailCount = 0;
            sendReminders({}, {
                send: function (msg) {
                    emailCount += 1;
                    switch (msg.email) {
                        case 'john@smith.com' :
                            assert(checkTextFound('Go canvassing in a marginal', msg.text));
                            assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                            assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                            break;
                        case 'ann@jones.com' :
                            assert(checkTextFound('Take a road trip to a marginal', msg.text));
                            assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                            assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                            break;
                    }
                    if (emailCount == 2) {
                        client.query(`select reminded_until from public."user" where id in  ('${id1}','${id2}')`)
                            .then(result => {
                                assert(result.rows[0].reminded_until, 'Must update reminded_until');
                                assert(result.rows[1].reminded_until, 'Must update reminded_until');
                                done();
                            });
                    }
                }
            });
        });

        it('reminded_until updates can be inhinited', function (done) {
            let emailCount = 0;
            sendReminders({inhibit_update: true}, {
                send: function () {
                    emailCount += 1;
                    if (emailCount == 2) {
                        client.query(`select reminded_until from public."user" where id in  ('${id1}','${id2}')`)
                            .then(result => {
                                assert(!result.rows[0].reminded_until, 'Must inhibit update reminded_until');
                                assert(!result.rows[1].reminded_until, 'Must inhibit update reminded_until');
                                done();
                            });
                    }
                }
            });
        });

        it('limits the reminders', function (done) {
            let emailCount = 0;
            sendReminders({limit: 1}, {
                send: function () {
                    emailCount += 1;
                }
            })
                .then(() => {
                    assert.strictEqual(emailCount, 1);
                    done();
                });
        });

        it('allows selection of email', function (done) {
            let emailCount = 0;
            sendReminders({selectionCrit: "email = 'john@smith.com' and"}, {
                send: function (msg) {
                    assert.strictEqual(msg.to, 'john@smith.com');
                    emailCount += 1;
                }
            })
                .then(() => {
                    assert.strictEqual(emailCount, 1);
                    done();
                });
        });

        it('allows non confirmed emails to be spammed', function (done) {
            let emailCount = 0;
            sendReminders({include_unconfirmed: true}, {
                send: function () {
                    emailCount += 1;
                }
            })
                .then(() => {
                    assert.strictEqual(emailCount, 3);
                    done();
                });
        });

    });

    describe('generalised email', function () {

        const id1 = uuid1();
        const id2 = uuid1();
        const id3 = uuid1();
        const public_key = uuid1();

        before(function () {
            let qry = `
INSERT INTO public."user"(id, name, email, public_key, email_confirmed) 
VALUES 
('${id3}','Bill Unconfirmed', 'bill@nill.com', '${public_key}', NULL),
('${id1}','John Smith',       'john@smith.com','${public_key}', '${new Date().toISOString()}'),
('${id2}','Ann Jones',        'ann@jones.com', '${public_key}', '${new Date().toISOString()}');`;
            return client.query(qry);
        });

        after(function () {
            return client.query(`DELETE from public."user" where id in ('${id1}','${id2}','${id3}');`);
        });


        it('fills the fields', function () {
            generalisedEmail({}, 'john@smith.com', 'test email', ['here is some text'], {id: '23423-23423-234234-23423'}, null, null, null, {
                send: function (msg) {
                    assert.strictEqual(msg.to, 'john@smith.com');
                    assert.strictEqual(msg.from, 'info@peoplesmomentum.com');
                    assert.strictEqual(msg.subject, 'test email');
                    assert.strictEqual(msg.text, 'here is some text')
                }
            })
        });

        it('generates html', function (done) {
            generalisedEmail({}, 'john@smith.com', 'test email', ['here is some text'], {id: '23423-23423-234234-23423'}, null, null, null, {
                send: function (msg) {
                    assert(msg.html.includes('<p>here is some text</p>'));
                    done();
                }
            })
        });

        it('replaces text', function (done) {
            generalisedEmail({},
                'john@smith.com',
                'test email',
                ['here is some $variable$ text'],
                {id: '23423-23423-234234-23423', variable: 'replaced'},
                null,
                null,
                null, {
                    send: function (msg) {
                        assert(msg.text.includes('here is some replaced text'));
                        assert(msg.html.includes('<p>here is some replaced text</p>'));
                        done();
                    }
                })
        });

        it('does simple links', function (done) {
            generalisedEmail({},
                'john@smith.com',
                'test email',
                ['please ^link^'],
                {variable: 'replaced', id: '23423-23423-234234-23423'},
                {link: {url: 'http://sdaas.com', text: 'click here'}},
                null,
                null, {
                    send: function (msg) {
                        assert(msg.text.includes('please <a href="http://sdaas.com">click here</a>'));
                        assert(msg.html.includes('<p>please <a href="http://sdaas.com">click here</a></p>'));
                        done();
                    }
                })
        });

        it('does flash links', function (done) {
            generalisedEmail({},
                'john@smith.com',
                'test email',
                ['please ^link^'],
                {variable: 'replaced', id: '23423-23423-234234-23423'},
                {link: {url: 'http://sdaas.com', text: 'click here', cta: true}},
                null,
                null, {
                    send: function (msg) {
                        assert(msg.text.includes('please <a href="http://sdaas.com">click here</a>'));
                        assert(msg.html.includes(`<td> <a style='color: black;' class='speciallink' href="http://sdaas.com" target="_blank">click here</a> </td>`));
                        done();
                    }
                })
        });

        it('throws when there is no replacement text found', function (done) {
            generalisedEmail({},
                'john@smith.com',
                'test email',
                ['The replacement will be <%= not_found =>'],
                {id: '23423-23423-234234-23423'},
                null,
                null,
                null,
                {send: 'not needed'})
                .catch(() => {
                    done();
                });
        });

        it('merges HTML fancy bits', function (done) {
            generalisedEmail({},
                'john@smith.com',
                'test email',
                ['nothin fancy'],
                {id: '23423-23423-234234-23423'},
                null,
                null,
                null, {
                    send: function (msg) {
                        // console.log(msg.html);
                        assert(msg.html.includes('My_Plan_to_Win_Email.png"'));
                        done();
                    }
                })
        });

        it('limits the number of emails sent', function (done) {
            let emailCount = 0;
            generalisedEmails({
                    query: 'select * from public."user"',
                    limit: 1,
                    subject: "testing",
                    content: ["testing"]
                },
                {
                    send: () => {
                        emailCount += 1;
                    }
                })
                .then(() => {
                    assert.strictEqual(emailCount, 1);
                    done();
                })
        });

    });

    describe('unsubscribe', function () {

        before(function () {
            return client.query(`INSERT INTO public."user" (id, name, email, public_key)
                VALUES ('${user_id}', '${name}', '${email}', '${uuid1()}');`)
        });

        after(function () {
            return client.query(`DELETE from public."user" where id = '${user_id}';`)
        });

        it('unsubscribes someone', function (done) {
            const req = {
                params: {
                    uuid: user_id
                }
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function () {
                    client.query(`SELECT * from public."user" where email = '${email}'`)
                        .then((result) => {
                            assert.strictEqual(result.rows.length, 1);
                            assert.strictEqual(result.rows[0].can_contact, false);
                            done();
                        });
                }
            };
            unsubscribe(req, res);
        });

        it('returns 404 if unsubscribing non existant user', function (done) {
            const req = {
                params: {
                    uuid: uuid1()
                }
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 404);
                    return this;
                },
                send: function (msg) {
                    assert(msg.includes('Could not find'));
                    done();
                }
            };
            unsubscribe(req, res);
        })

    });

    describe('does mailing for MyPollingDay', function () {

        it('sends email', function (done) {
            let mailDone = false;
            const email = 'mark.chapman+test@gmail.com';
            const req = {
                body: {
                    email,
                    firstName : 'Mark',
                    constituencyName: 'Hastings and Rye'
                },
                mailer: {
                    send: function (msg) {
                        assert.strictEqual(msg.to, email);
                        assert.strictEqual(msg.subject, 'Thank you for your polling day pledge!');
                        assert.strictEqual(msg.to, email);
                        assert(msg.text.includes('Mark'), 'First name missing in text');
                        assert(msg.html.includes('Mark'), 'First name missing in html');
                        assert(msg.text.includes('Hastings and Rye'), 'Constituency name missing in text');
                        assert(msg.html.includes('Hastings and Rye'), 'Constituency name missing in html');
                        assert(!msg.text.includes('undefined'), 'Something undefined in text\n\nmsg.text');
                        assert(!msg.html.includes('undefined'), 'Something undefined in HTML\n\nmsg.html');
                        assert(!msg.html.includes('$'), 'Something failed in template replacement\n\nmsg.html');
                        mailDone = true;
                    }
                }
            };
            const res = {
                status: function (code) {
                    assert.strictEqual(code, 200);
                    return this;
                },
                send: function (status) {
                    assert.strictEqual(status, 'OK');
                    assert(mailDone);
                    done();
                }
            };
            // update user with name and email and generate email (email 1) to user with a confirm button that links to /confirm-email/{uuid}.
            sendPollingDayEmail(req, res);
        });

    });

});
