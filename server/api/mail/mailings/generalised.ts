import {generalisedEmails, GeneralMailingOpts} from "../mail_engine";

const program = require('commander');

let mailer;

program
    .requiredOption('-c, --content <content text>', '(required) the content of the message with \\n instead of line breaks')
    .requiredOption('-s, --subject <subject text>', '(required) the subject of the message')
    .option('-e, --email <email>', 'just send to specified email address (this or -q must be present)')
    .option('-q, --query', 'specify SQL query which returns at least email field (this or -e must be present)')
    .option('-l, --limit <limit>', 'number of emails to be sent')
    // .option('-u, --include_unconfirmed', 'spam people who have not confirmed their emails')
    .option('-m, --html', 'display html markup email to console (inhibits sending')
    .option('-p, --plain_text', 'display text email to console (inhibits sending')
    .option('-k, --links <instructions>', 'replacement link instructions')
    .option('-v, --verbose', 'spits out debug info');

program.on('--help', function(){
    console.log('');
    console.log('Example:');
    console.log('');
    console.log('  $ generalised -c "Hello $name$\\n\\n^link^" -s "Important message" -e "mark@gmail.com" -k "{\\\"link\\\":{\\\"text\\\":\\\"Click Me\\\", \\\"url\\\":\\\"http://blah.com\\\", \\\"cta\\\":\\\"true\\\"}}" -p -v');
    console.log('');
    console.log('Will send a message to the user with the email John Smith with the text');
    console.log('');
    console.log('Hello John\n\nClick Me!');
    console.log('');
    console.log('where the click me is a link to http://blah.com, and an HTML equivalent with a pretty call to action button');
});

program.parse(process.argv);

let opts: GeneralMailingOpts = {
    subject: program.subject,
    content: program.content.split('\\n')
};

if (program.verbose) {
    console.log(program.opts());
    opts.verbose = true;
}

if (!program.query && !program.email) {
    console.log('You must specify either a query or an email');
    process.exit(1);
}

let query = program.query;
if (program.email) {
    if (program.verbose) { console.log(`Just sending to ${program.email}`)}
    if (!query) {
        query = `select * from public."user" where email = '${program.email}'`;
    }
}
opts.query = query;

if (program.limit) {
    if (program.verbose) { console.log(`Limiting to ${program.limit} emails`)}
    opts.limit = program.limit;
}

if (program.links) {
    try {
        opts.linkObject = JSON.parse(program.links);
    } catch(e) {
        console.log('Error in links - ' + e.message);
        process.exit(1);
    }
}

if (program.plain_text) {
    if (program.verbose) { console.log(`Just listing text mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.text);
        }
    }
}

if (program.html) {
    if (program.verbose) { console.log(`Just listing html mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.html);
        }
    }
}

generalisedEmails(opts, mailer)
    .then(() => {
       console.log('Done');
       process.exit(0);
    })
    .catch(err => {
        console.log('Error - ' + err.message);
        process.exit(1);
    });
