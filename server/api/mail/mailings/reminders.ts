import {sendReminders, ReminderOptions} from "../reminders_engine";
const program = require('commander');

let mailer;
let opts: ReminderOptions = {};
program
    .option('-v, --verbose', 'spits out debug info')
    .option('-e, --email <email>', 'just select one email')
    .option('-l, --limit <limit>', 'number of activities (not emails)')
    .option('-i, --inhibit_update', 'do not update the reminded_until field (implied by -h and -t)')
    .option('-u, --include_unconfirmed', 'spame people who have not confimed their emails')
    .option('-m, --html', 'display html markup email to console (inhibits sending')
    .option('-t, --text', 'display text email to console (inhibits sending');

program.parse(process.argv);

if (program.verbose) {
    console.log(program.opts());
    opts.verbose = true;
}

let selectCrit = '';
if (program.email) {
    if (program.verbose) { console.log(`Just sending to ${program.email}`)}
    selectCrit += `email = '${program.email}' and`;
}
opts.selectionCrit = selectCrit;

if (program.limit) {
    if (program.verbose) { console.log(`Limiting to ${program.limit} actions (may be fewer emails)`)}
    opts.limit = program.limit;
}

if (program.inhibit_update) {
    if (program.verbose) { console.log(`Limiting to ${program.limit} actions (may be fewer emails)`)}
    opts.inhibit_update = true;
}

if (program.include_unconfirmed) {
    if (program.verbose) { console.log(`Including unconfirmed emails`)}
    opts.include_unconfirmed = true;
}

if (program.text) {
    opts.inhibit_update = true;
    if (program.verbose) { console.log(`Just listing text mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.text);
        }
    }
}

if (program.html) {
    opts.inhibit_update = true;
    if (program.verbose) { console.log(`Just listing html mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.html);
        }
    }
}

if (program.inhibit_update) {
    if (program.verbose) { console.log(`Users will not have the reminded field updated`)}
}

sendReminders(opts, mailer)
.then(() =>{
    console.log('done');
    process.exit(0);
})
.catch((err) => {
    console.log(err.message);
    process.exit(1);
});
