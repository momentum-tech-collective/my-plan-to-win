import {GOTVOptions, sendBriefings} from "../GOTV-briefing-engine";
const program = require('commander');

let mailer;
let opts: GOTVOptions = {};
program
    .option('-v, --verbose', 'spits out debug info')
    .option('-e, --email <email>', 'just select one email')
    .option('-l, --limit <limit>', 'number of activities (not emails)')
    .option('-u, --include_unconfirmed', 'spame people who have not confimed their emails')
    .option('-m, --html', 'display html markup email to console (inhibits sending')
    .option('-t, --text', 'display text email to console (inhibits sending');

program.parse(process.argv);

if (program.verbose) {
    console.log(program.opts());
    opts.verbose = true;
}

let selectCrit = '';
if (program.email) {
    if (program.verbose) { console.log(`Just sending to ${program.email}`)}
    selectCrit += `email = '${program.email}' and`;
}
opts.selectionCrit = selectCrit;

if (program.limit) {
    if (program.verbose) { console.log(`Limiting to ${program.limit} actions (may be fewer emails)`)}
    opts.limit = program.limit;
}

if (program.include_unconfirmed) {
    if (program.verbose) { console.log(`Including unconfirmed emails`)}
    opts.include_unconfirmed = true;
}

if (program.text) {
    if (program.verbose) { console.log(`Just listing text mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.text);
        }
    }
}

if (program.html) {
    if (program.verbose) { console.log(`Just listing html mails`)}
    mailer = {
        send: function(msg) {
            console.log(msg.html);
        }
    }
}

sendBriefings(opts, mailer)
.then(() =>{
    console.log('done');
    process.exit(0);
})
.catch((err) => {
    console.log(err.message);
    process.exit(1);
});
