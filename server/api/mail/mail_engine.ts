import {promiseRead, doReplace, sgMail} from './mail.lib'
import {client} from "../db";
const path = require('path');

export interface GeneralEmailOpts {
    query? : string;     // a string that select
    limit? : number;
    // include_unconfirmed? : boolean;
    verbose?: boolean;
}

export interface  GeneralMailingOpts extends GeneralEmailOpts {
    subject: string;
    content: string[];
    replace?: any;
    linkObject? : any;
    preheader? : string;
    picture? : string;
}

export type LinkSpec = {text: string, url: string, cta?: boolean}

function doTextLinks(input: string, links: {[tag: string]: LinkSpec}) {
    let array = input.split('^');
    for (let i=1; i<array.length; i+=2) {
        let link = links[array[i]];
        array[i] = `<a href="${link.url}">${link.text}</a>`;
    }
    return array.join('');
}

function doHTMLLinks(input: string, links: {[tag: string]: LinkSpec}, cta_markup) {
    let array = input.split('^');
    for (let i=1; i<array.length; i+=2) {
        let link = links[array[i]];
        if (link.cta) {
            array[i] = doReplace(cta_markup, {url: link.url, text: link.text});
        } else {
            array[i] = `<a href="${link.url}">${link.text}</a>`
        }
    }
    return array.join('');
}

/*
    Send a generalised email - example text might be
      ['Dear $name$','Please ^ctalink^ or copy ^simplelink^ this url into your browser']
    with replace
      {
        name: 'Fred'
      }
    and links
      {
        ctalink: {
            text: 'Click Me',
            url:'http://blah.com',
            cta: true
            },
        simplelink: {
            text: 'http://blah.com',
            url:'http://blah.com',
            }
       }
 */
export async function generalisedEmail(
    opts: GeneralEmailOpts,
    to: string,
    subject: string,
    text: string[],            // Containing $named tags$ and %links%
    replace? : {[tag: string]: string},
    links?: {[tag: string]: LinkSpec },
    preheader?: string,
    picture?: string,
    testMailer? : any
): Promise<boolean> {
    replace = replace || {};

    let mailer = testMailer || sgMail;

    return Promise.all([
        promiseRead(path.join(__dirname, 'emails/_header.html')),
        promiseRead(path.join(__dirname, 'emails/_generalised_body.html')),
        promiseRead(path.join(__dirname, 'emails/_cta_button.html'))
    ])
        .then(results => {
            let msgText = text.join('/n/n');
            let html = `
<!doctype html>
<html>
${results[0].toString()}
${doReplace(results[1].toString(),{__text__: `<p>${text.join('</p>\n\n<p>')}</p>`}, "retain")} 
</html>`;
            msgText = doTextLinks(msgText, links);
            msgText = doReplace(msgText, replace);

            // Add HTML specific stuff
            replace.image = picture || 'http://staging-plan-to-win.herokuapp.com/My_Plan_to_Win_Email.png';
            replace.preheader = preheader || subject;
            replace.subject = subject;
            html = doHTMLLinks(html, links, results[2].toString());
            html = doReplace(html, replace);

            mailer.send({
                to,
                from: 'info@peoplesmomentum.com',
                subject,
                text: msgText,
                html
            });
            return true;
        });
}

/*
export async function remindEmailConfirm(user: IUser, mailer? : any) : Promise<any> {
    return generalisedEmail(
        user.email,
        'Reminder to confirm your email address',
        [
            'Hi Friend,',
            'Thanks for using My Plan To Win!',
            'We notice you have not yet confirmed your email.  This could have been intentional, or it could be because of a glitch we have now fixed that prevented some emails from being sent out.',
            '^click^',
            'In solidarity,',
            'Momentum'
        ],
        {otp: user.one_time_code, uid: user.id},
        {click: {url: process.env.SITE_URL + '/sign-in/$otp$', text: 'CONFIRM YOUR EMAIL', cta: true}},
        null,
        null,
        mailer
    )
}
*/

export async function generalisedEmails(opts: GeneralMailingOpts, mailer? : any, db?: any) {
    let dbClient = db || client;
    return dbClient.query(opts.query)
        .then((result) => {
            if (result.rowCount === 0) {
                console.log('Warning - no records matched the query');
            } else {
                console.log(`${result.rowCount} records selected`);
            }
            if (opts.limit) {
                result.rows.length = opts.limit;
            }
            return require('bluebird').map(
                result.rows,
                async r => {
                    return generalisedEmail(opts as GeneralEmailOpts, r.email, opts.subject, opts.content, r, opts.linkObject, null, null, mailer)
                },
                {concurrency: 5})
        })
}
