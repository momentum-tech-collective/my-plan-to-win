var fs = require("fs");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
var sslRedirect = require("heroku-ssl-redirect");
const { customSocialSharing } = require('./server/routes')

var app = express();
app.use(sslRedirect());
app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

let apiPath = __dirname + "/server/api";
let apiFolders = fs.readdirSync(apiPath);
apiFolders.forEach(function(folder) {
  let fullFolder = path.join(apiPath, folder);
  if (fs.statSync(fullFolder).isDirectory()) {
    require(fullFolder + "/index").doRouting(app);
  }
});

// Social share card customisation
app.get('/plan/:public_key', customSocialSharing)
app.get('/plans/:public_key', customSocialSharing)

app.use(express.static(path.join(__dirname, "client", "build")));

/* Serve everything else through the web app */
app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "client/build/index.html"));
});

module.exports = app;
