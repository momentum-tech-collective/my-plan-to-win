## Development

Create a Postgresql database using seed.sql

- sudo -u postgres psql
- CREATE DATABASE myplantowin;
- \q
- sudo -u postgres psql -d myplantowin -a -f ./seed.sql

If your postgres user doesn't have a password you should add one:

- sudo -u postgres psql
- \password

Set up environment variables:

- DATABASE_URL (postgres://username:password@host:port/database - 'postgres://postgres:{password}@localhost:5432/myplantowin' if you followed the steps above)
- SENDGRID_API_KEY (if you are sending emails)

Run
`npm start`

then navigate to http://localhost:3000

`npm test` will run unit tests.

The code adapted from the [Angular Seed](https://github.com/angular/angular-seed) skeleton.

You can push any branch to production with this command:

```
git push -f heroku CURRENT_BRANCH_NAME:master
```

You can access a storybook env by running `yarn storybook`

## Production

- All branch heads are deployed to the test rig at https://testing-plan-to-win.herokuapp.com to confirm they work.
- All commits to `master` are deployed to staging: https://staging-plan-to-win.herokuapp.com
- Tagged commits in `master` are deployed to production: https://myplantowin2019.com

### Seeding the database

Configure a newly provisioned database with [./seed.sql](./seed.sql):

```
heroku ps:exec
psql -d DATABASE_URL -a -f ./seed.sql
```

### Environment variables

```
REACT_APP_ANALYTICS_ID
SENTRY_DSN
```

- Link up Heroku

```
heroku git:remote -a my-plan-to-win
```

- Configure domain

(Tutorial: https://devcenter.heroku.com/articles/custom-domains)

```
heroku domains:add myplantowin2019.com
```

### FYI: What the build process looks like

We use [`app.json`](./app.json) to describe the production environment and [`package.json`](./package.json)'s `heroku-postbuild` to describe the build process beyond the buildpack.

- Build the client

```
cd client
yarn install
yarn build
```

- Run the server

```
yarn install
yarn start
```

- `DATABASE_URL` is injected into the env via Heroku automatically
