/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import { configure, addDecorator } from '@storybook/react'
import { MemoryRouter } from 'react-router';
import '@storybook/addon-console';

addDecorator(children => (
  <MemoryRouter>
    <div css={css`padding: 50px;`}>
      {children()}
    </div>
  </MemoryRouter>
))

// automatically import all files ending in *.stories.tsx
configure(require.context('../src', true, /\.stories\.tsx$/), module)
