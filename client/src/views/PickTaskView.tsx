import React from "react";
import { RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

const PickTaskView: React.FC<RouteComponentProps<{}>> = () => {
  return (
    <div>
      <h2>Big tasks</h2>
      <p>
        These are that will win us the election. Please prioritise one a day.
      </p>
      <h2>Medium tasks</h2>
      <p>Small effort, high impact. Choose one a day.</p>
      <h2>Small tasks</h2>
      <p>
        The building blocks of everything we do. Please do at least one a day.{" "}
      </p>
      <Link to="/create">GO TO DAY TWO: MONDAY</Link>
    </div>
  );
};

export default PickTaskView;
