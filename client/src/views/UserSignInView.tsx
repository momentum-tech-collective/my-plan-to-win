/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { useEffect } from "react";
import { RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

import Closebtn from "../components/Closebtn";
import CustomUpdatesForm from "../components/CustomUpdatesForm";
import ShareBar from "../components/ShareBar";
import { shareMessage } from "../data/copy";
import {
  Layout,
  layoutFlexRow,
  padding,
  pagePaddingHorizontalLimited,
  Swatch,
  typeLargeCopy
} from "../design-constants";
import { getPlansUrl } from "../helpers/url";
import { useGlobalState, useDispatch } from "../context/store";
import { useCheckToken } from "../context/actions";

const UserSignInView: React.FC<RouteComponentProps<{ OTP: string }>> = ({
  match: {
    params: { OTP }
  },
  history
}) => {
  const dispatch = useDispatch()
  const checkToken = useCheckToken(dispatch)

  const { user, userIdFromOTP } = useGlobalState()

  useEffect(() => {   
    checkToken(OTP)
  }, [OTP])

  useEffect(() => {
    if (OTP && userIdFromOTP === false) {
      // This OTP was already used once, or else doesn't work anyway
      history.push("/login/link-expired");
    }
  }, [OTP, userIdFromOTP]);

  return (
    <Layout>
      <div
        css={css`
          background: ${Swatch.darkPink};
          min-height: 100vh;
          height: 100%;
        `}
      >
        <div
          css={css`
            ${pagePaddingHorizontalLimited}
            padding-top: ${padding.pageVertical};
            padding-bottom: ${padding.pageVertical};
          `}
        >
          <div>
            {userIdFromOTP && user ? (
              <div>
                <h1
                  css={css`
                    letter-spacing: -0.01em;
                    ${layoutFlexRow}
                    justify-content: space-between;
                  `}
                >
                  <span>✅ Thank you for confirming your email.</span>
                  <Closebtn
                    onClick={() => {
                      history.push("/create");
                    }}
                  />
                </h1>
                <p css={typeLargeCopy}>
                  You can view your Plan to Win <Link to={"/create"}>here</Link>
                  . Let’s get out there and win this!
                </p>
              </div>
            ) : userIdFromOTP === false ? (
              <h1>Sign in again</h1>
            ) : (
              <h1>Loading your plan</h1>
            )}
            {!!userIdFromOTP && !!user && (
              <div>
                <div
                  css={css`
                    border-radius: 10px;
                    padding: 15px;
                    border: 2px solid black;
                  `}
                >
                  <div
                    css={css`
                      ${typeLargeCopy}
                      letter-spacing: -0.01em;
                      margin-bottom: 10px;
                    `}
                  >
                    It’s absolutely vital that you <b>share your Plan to Win</b>{" "}
                    on social media and make sure to tag your friends. ✊
                  </div>
                  <div
                    css={css`
                      text-align: center;
                    `}
                  >
                    <ShareBar message={shareMessage} url={getPlansUrl(user)} />
                  </div>
                </div>
                {(!user.mobile || !user.postcode) && (
                  <div>
                    <h1
                      css={css`
                        margin-top: 1em;
                        margin-bottom: 0.5em;
                        letter-spacing: -0.01em;
                        ${layoutFlexRow}
                        justify-content: space-between;
                      `}
                    >
                      <span>
                        Get a handy reminder of your plan each morning
                      </span>
                    </h1>
                    <p
                      css={css`
                        ${typeLargeCopy}
                        margin-top: 0;
                      `}
                    >
                      Add your contact details if you want to get custom
                      reminders by email or text. Our incredible volunteers will
                      also call you to talk through the plan and give you tips
                      on how to undertake big actions.
                    </p>
                    <CustomUpdatesForm
                      initialEmail={user.email}
                      initialName={user.name}
                      onSuccess={() => {
                        history.push("/create");
                      }}
                    />
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default UserSignInView;
