/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment } from "react";
import { useHistory } from "react-router-dom";

import { Button } from "../components/Button";
import SignInButton from "../components/SignInButton";
import {
  Layout,
  pagePaddingHorizontalLimited,
  Swatch,
  typeLargeCopy,
  typeTitle
} from "../design-constants";
import { isTodayPollingDay } from "../data/constants";
import { useGlobalState } from "../context/store";
import MobilePollingDayLanding from "../components/polling-day/Landing";

const LandingView: React.FC = () => {
  const history = useHistory();
  const { activityCount } = useGlobalState();

  return (
    <Layout>
      <div
        css={css`
          background: ${Swatch.red};
        `}
      >
        <div
          css={css`
            position: relative;
            z-index: 2;
            background: ${Swatch.red};
            ${pagePaddingHorizontalLimited}
          `}
        >
          <img
            css={css`
              width: 100%;
            `}
            src="https://i.imgur.com/HVLQ5ne.png"
            alt=""
          />
        </div>
      </div>
      <div
        css={css`
          ${pagePaddingHorizontalLimited}
          margin-top: 1em;
          margin-bottom: 1em;
          padding-bottom: 1em;
          ${typeLargeCopy}

          @media screen and (min-width: 1024px) {
            margin-top: 2em;
            margin-bottom: 2em;
          }
        `}
      >
        {isTodayPollingDay ? (
          <MobilePollingDayLanding />
        ) : (
          <Fragment>
            <div
              css={css`
                ${typeTitle}
                letter-spacing: -0.05em;
                font-weight: 600;
              `}
            >
              <p>
                Other Labour supporters have already committed to{" "}
                {activityCount} tasks.
              </p>
              <p>If we're going to win, we all need to step up.</p>
            </div>
            <p>
              <b>The next few days</b> are the most important in this campaign.
              Our chance to transform Britain for the many is here, but we all
              need to step up. <b>This is what could win us this election.</b>
            </p>
            <Button
              css={css`
                background-color: ${Swatch.darkPink};
                margin-bottom: 1rem;
              `}
              onMouseDown={() => {
                history.push("/create");
              }}
            >
              Build your Plan 🛠
            </Button>
            <SignInButton />
          </Fragment>
        )}
      </div>
    </Layout>
  );
};

export default LandingView;
