/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { Layout, padding, Swatch, typeTitle } from "../design-constants";

const LoadingView = () => {
  return (
    <Layout>
      <div
        css={css`
          background: ${Swatch.red};
          min-height: 100vh;
          height: 100%;
          color: white;
          padding-top: ${padding.pageVertical};
          padding-bottom: ${padding.pageVertical};
          display: flex;
          justify-content: center;
          align-items: center;
          align-content: center;
          font-weight: 900;
          font-style: italic;
          text-transform: uppercase;
          ${typeTitle}
        `}
      >
        Loading...
      </div>
    </Layout>
  );
};

export default LoadingView;
