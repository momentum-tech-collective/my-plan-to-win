/** @jsx jsx */

import { jsx, css } from "@emotion/core";
import { useHistory } from "react-router";

import ShareBar from "../components/ShareBar";
import Closebtn from "../components/Closebtn";

import { shareMessage } from "../data/copy";
import {
  Layout,
  typeLargeCopy,
  Swatch,
  pagePaddingHorizontalLimited,
  padding,
  layoutFlexRow
} from "../design-constants";
import { getPlansUrl } from "../helpers/url";
import { useGlobalState } from "../context/store";
import { isTodayPollingDay } from "../data/constants";

const textColor = isTodayPollingDay ? Swatch.white : Swatch.black;
const backgroundColor = isTodayPollingDay ? Swatch.red : Swatch.darkPink

const SharePlanView = () => {
  const history = useHistory();
  const { user } = useGlobalState();

  return (
    <Layout>
      <div
        css={css`
          background: ${backgroundColor};
          color: ${textColor}
          ${typeLargeCopy}
          letter-spacing: -0.01em;
          min-height: 100vh;
          height: 100%;
        `}
      >
        <div
          css={css`
            ${pagePaddingHorizontalLimited};
            padding-top: ${padding.pageVertical};
            padding-bottom: ${padding.pageVertical};
            color: ${textColor};
          `}
        >
          <h1
            css={css`
              font-weight: bold;
              font-size: 28px;
              margin: 0;
              justify-content: space-between;
              ${layoutFlexRow}
            `}
          >
            <span>Share your Plan to Win!</span>
            <Closebtn onClick={() => history.push("/")} />
          </h1>
          <p
            css={css`
              font-size: 18px;
            `}
          >
            The most useful step you could take now is to share this on social
            media and make sure to <b>tag your friends.</b>
          </p>
          <ShareBar message={shareMessage} url={getPlansUrl(user)} />
        </div>
      </div>
    </Layout>
  );
};

export default SharePlanView;
