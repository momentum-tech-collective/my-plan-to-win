/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router";

import { IUser } from "../../../server/api/db";
import SavePlanComplete from "../components/SavePlanComplete";
import SavePlanForm from "../components/SavePlanForm";
import {
  Layout,
  padding,
  pagePaddingHorizontalLimited,
  Swatch
} from "../design-constants";
import { useGlobalState } from "../context/store";

const SavePlanView: React.FC<RouteComponentProps<{}>> = ({ history }) => {
  const [userFromForm, setUser] = useState<IUser>();
  const { user, isLoggedIn } = useGlobalState()

  useEffect(() => {
    if (user && user.email_confirmed) {
      setUser(user);
    }
  }, [user]);

  useEffect(() => {
    if(isLoggedIn) {
      setUser(user)
    }
  }, [isLoggedIn])

  return (
    <Layout>
      <div
        css={css`
          background: ${Swatch.darkPink};
          min-height: 100vh;
          height: 100%;
        `}
      >
        <div
          css={css`
          ${pagePaddingHorizontalLimited}
          padding-top: ${padding.pageVertical};
          padding-bottom: ${padding.pageVertical};
        `}
        >
          {userFromForm ? (
            <SavePlanComplete
              user={userFromForm}
              onClose={() => {
                history.push("/create");
              }}
            />
          ) : (
            <SavePlanForm
              user={user}
              onClosePane={() => {
                history.push("/create");
              }}
            />
          )}
        </div>
      </div>
    </Layout>
  );
};

export default SavePlanView;
