/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { addDays, isSameDay, startOfDay, format } from "date-fns";
import { isAfter } from "date-fns/esm";
import React, { useEffect, useMemo, useState, Fragment } from "react";
import { RouteComponentProps } from "react-router";
import uuidv1 from "uuid/v1";

import { IActivity } from "../../../server/api/db";
import ActivityCalendar from "../components/ActivityCalendar";
import {
  StickyButton,
  buttonDefaultStyles,
  Button
} from "../components/Button";
import Calendar from "../components/Calendar";
import Fixed from "../components/Fixed";
import Header from "../components/Header";
import ShareBar from "../components/ShareBar";
import SignInButton from "../components/SignInButton";
import {
  BigTaskBlock,
  MediumTaskBlock,
  SmallTaskBlock,
  TaskMenu
} from "../components/Task";
import { pollingDay, isTodayPollingDay } from "../data/constants";
import { dayMessages, shareMessage } from "../data/copy";
import {
  Layout,
  pagePaddingHorizontal,
  pagePaddingHorizontalLimited,
  Swatch,
  font
} from "../design-constants";
import { hasUserReachedActivityMaxCount } from "../helpers/activity";
import { getPlansUrl } from "../helpers/url";
import { Link } from "react-router-dom";
import {
  TAB_BAR_HEIGHT_DESKTOP_PX,
  TAB_BAR_HEIGHT_MOBILE_PX
} from "../components/TabBar";
import useMedia from "../helpers/use-media";
import SignInLink from "../components/SignInLink";
import { useGlobalState, useDispatch } from "../context/store";
import {
  useDeleteActivity,
  useCreateActivity,
  useCheckToken
} from "../context/actions";
import Create from "../components/polling-day/Create";

const CreatePlanView: React.FC<RouteComponentProps<{ OTP?: string }>> = ({
  history,
  match: {
    params: { OTP }
  }
}) => {
  const dispatch = useDispatch();
  const checkToken = useCheckToken(dispatch);
  const deleteActivity = useDeleteActivity(dispatch);
  const createActivity = useCreateActivity(dispatch);

  const {
    activities,
    user,
    categories,
    isLoggedIn,
    activityCount,
    userId,
    userIdFromOTP
  } = useGlobalState();

  useEffect(() => {
    checkToken(OTP);
  }, [OTP]);

  useEffect(() => {
    if (OTP && userIdFromOTP === false) {
      history.push("/login/link-expired");
    }
  }, [OTP, userIdFromOTP]);

  const [dateEditing, setDate] = useState<null | Date>(null);

  const busyDays: Date[] = useMemo(() => {
    const setOfDays = activities.reduce((days, a) => {
      days.add(new Date(a.date));
      return days;
    }, new Set<Date>());
    return Array.from(setOfDays);
  }, [activities]);

  const selectTasksForDay = (day: Date) => {
    setDate(day);
  };

  const addTaskToDay = (id: string, date = dateEditing) => {
    if (!date) {
      return;
    }

    const newActivity: IActivity = {
      id: uuidv1(),
      category_id: id,
      user_id: userId || "",
      date
    };

    createActivity(newActivity);
  };

  const activitiesMatchingCategoryAndDate = (categoryId: string, date: Date) =>
    activities.filter(
      a => a.category_id === categoryId && isSameDay(new Date(a.date), date)
    );

  const deselectMatchingTasks = (categoryId: string) => {
    if (!dateEditing) return [];
    const activities = activitiesMatchingCategoryAndDate(
      categoryId,
      dateEditing
    );
    activities.forEach(activity => deleteActivity(activity.id));
  };

  const activityIdsToday = !dateEditing
    ? []
    : activities
        .filter(a => isSameDay(new Date(a.date), dateEditing))
        .map(a => a.category_id);

  const reachedLastDay =
    dateEditing &&
    (isAfter(dateEditing, pollingDay) || isSameDay(dateEditing, pollingDay));

  const nextDay = startOfDay(
    addDays(dateEditing && !reachedLastDay ? dateEditing : new Date(), 1)
  );

  const onSaveAndShareClick = () => {
    if (isLoggedIn) {
      return history.push("/share");
    }

    history.push("/save");
  };

  const onNextDayClick = () => setDate(nextDay);
  const onBackToCalendar = () => setDate(null);

  useEffect(() => {
    window.scroll(0, 0);
  }, [dateEditing]);

  const categoriesToShow = categories.filter(category => {
    const maxCountReached = hasUserReachedActivityMaxCount(
      category.id,
      categories,
      activities
    );

    const isSelected = activityIdsToday.includes(category.id);

    return !maxCountReached || isSelected;
  });

  const isDesktop = useMedia(["(min-width: 1024px)"], [true], false);

  return (
    <Layout>
      {isTodayPollingDay ? (
        <Create />
      ) : (
        <Fragment>
          <Fixed
            position="top"
            additionalCss={css`
              width: 100%;
              top: ${TAB_BAR_HEIGHT_MOBILE_PX}px;
              left: 0;
              z-index: 999;

              @media screen and (min-width: 1024px) {
                display: ${TAB_BAR_HEIGHT_DESKTOP_PX};
              }
            `}
          >
            <div
              css={css`
                display: block;

                @media screen and (min-width: 1024px) {
                  display: none;
                }
              `}
            >
              <Header />
            </div>
          </Fixed>
          <Fixed
            position="top"
            fixedIf={isDesktop}
            additionalCss={css`
              width: 100%;
              left: 0;
              top: 0;
              z-index: 9;

              @media screen and (min-width: 1024px) {
                top: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
              }
            `}
          >
            <Calendar
              highlightedDay={dateEditing ? dateEditing : undefined}
              busyDays={busyDays}
              onSelectDay={day => {
                if (isSameDay(day, dateEditing || new Date(1900, 1, 1))) {
                  return onBackToCalendar();
                }
                selectTasksForDay(day);
              }}
            />
          </Fixed>
          <div
            css={css`
              @media screen and (min-width: 1024px) {
                height: calc(100vh - ${97 + TAB_BAR_HEIGHT_DESKTOP_PX}px);
                display: flex;
              }
            `}
          >
            {dateEditing ? (
              <div
                css={css`
                  width: 100%;
                `}
                key={dateEditing.toString()}
              >
                <header
                  css={css`
                    padding-top: 20px;
                    padding-bottom: 20px;
                    ${pagePaddingHorizontal}

                    ${reachedLastDay &&
                      `
                  background-color: ${Swatch.darkPink};
                  color: white;
                  border-color: white;
              `}
                  `}
                >
                  <div
                    css={css`
                      display: none;

                      @media screen and (min-width: 1024px) {
                        display: block;
                        display: flex;
                        justify-content: space-between;
                      }
                    `}
                  >
                    <div
                      css={css`
                        width: 33%;
                        padding: 10px;
                      `}
                    >
                      <h1
                        css={css`
                          font-weight: 600;
                          margin-top: 0;
                        `}
                      >
                        {reachedLastDay
                          ? "It all comes down to this."
                          : "Build your plan."}
                      </h1>
                      <p
                        css={css`
                          font-weight: 500;
                          font-size: 18px;
                          line-height: 20px;
                          letter-spacing: -0.01em;
                        `}
                      >
                        {reachedLastDay ? (
                          <Fragment>
                            Our chance to transform Britain is here.{" "}
                            <b>We need as many people to vote as possible!</b>
                          </Fragment>
                        ) : (
                          <Fragment>
                            Once you’re done, Save and Share with your friends.
                            <br />
                            We’ll send your plan straight to your inbox.
                          </Fragment>
                        )}
                      </p>
                      <span
                        onClick={onBackToCalendar}
                        css={css`
                          color: inherit;
                          font-weight: 600;
                          text-decoration: none;
                          position: relative;
                          cursor: pointer;

                          &:after {
                            content: "";
                            position: absolute;
                            bottom: -3px;
                            left: 0;
                            right: 0;
                            height: 1px;
                            background-color: ${reachedLastDay
                              ? Swatch.white
                              : Swatch.red};
                          }
                        `}
                      >
                        🗓 Go to Calendar View
                      </span>
                    </div>
                    {!isLoggedIn && (
                      <div
                        css={css`
                          padding-top: 10px;
                          padding-right: 30px;
                        `}
                      >
                        <SignInLink
                          underlineColor={
                            reachedLastDay ? Swatch.white : undefined
                          }
                        />
                      </div>
                    )}
                  </div>
                  <div
                    css={css`
                  text-align: left;
                  ${font.lBold}
                  font-weight: ${reachedLastDay ? "normal" : "600"};
                  @media screen and (min-width: 1024px) {
                    display: none;
                  }
                `}
                  >
                    {reachedLastDay ? (
                      <Fragment>
                        <h2
                          css={css`
                            font-size: 28px;
                            font-weight: 600;
                          `}
                        >
                          It all comes down to this.
                        </h2>
                        <p
                          css={css`
                            font-size: 18px;
                            line-height: 1.2em;
                          `}
                        >
                          Our chance to transform Britain is here.{" "}
                          <b>We need as many people to vote as possible!</b>
                        </p>
                      </Fragment>
                    ) : (
                      (
                        dayMessages.find(m =>
                          isSameDay(m.date, dateEditing)
                        ) || {
                          content: ""
                        }
                      ).content
                    )}
                  </div>
                </header>

                <article
                  css={css`
                    ${pagePaddingHorizontal}
                    padding-top: 20px;
                    padding-bottom: 20px;
                  `}
                >
                  {isSameDay(pollingDay, dateEditing) ? (
                    <div
                      css={css`
                        @media screen and (min-width: 1024px) {
                          display: flex;
                        }
                      `}
                    >
                      <div
                        css={css`
                          @media screen and (min-width: 1024px) {
                            width: 33.3333333%;
                            padding: 0 10px;
                          }
                        `}
                      >
                        <TaskMenu
                          name="polling day actions"
                          tasks={categoriesToShow
                            .filter(t => t.size === "g" && !t.suppressed)
                            .filter((c, i, arr) => i < arr.length / 3)}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                          dontShowHideOption={true}
                        />
                      </div>
                      <div
                        css={css`
                          @media screen and (min-width: 1024px) {
                            width: 33.3333333%;
                            padding: 0 10px;
                          }
                        `}
                      >
                        <TaskMenu
                          name="polling day actions"
                          tasks={categoriesToShow
                            .filter(t => t.size === "g" && !t.suppressed)
                            .filter(
                              (c, i, arr) =>
                                i >= arr.length / 3 && i < (2 * arr.length) / 3
                            )}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                          dontShowHideOption={true}
                        />
                      </div>
                      <div
                        css={css`
                          @media screen and (min-width: 1024px) {
                            width: 33.3333333%;
                            padding: 0 10px;
                          }
                        `}
                      >
                        <TaskMenu
                          name="polling day actions"
                          tasks={categoriesToShow
                            .filter(t => t.size === "g" && !t.suppressed)
                            .filter((c, i, arr) => i >= (2 * arr.length) / 3)}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                          dontShowHideOption={true}
                        />
                      </div>
                    </div>
                  ) : (
                    <div
                      css={css`
                        @media screen and (min-width: 1024px) {
                          display: flex;
                        }
                      `}
                    >
                      <div
                        css={css`
                          @media screen and (min-width: 1024px) {
                            padding: 10px;
                            width: 33%;
                          }
                        `}
                      >
                        <BigTaskBlock />
                        <TaskMenu
                          name="big actions"
                          tasks={categoriesToShow.filter(
                            t => t.size === "b" && !t.suppressed
                          )}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                          collapseOnSelect={true}
                        />
                      </div>
                      <div
                        css={css`
                          margin-top: 2rem;
                          @media screen and (min-width: 1024px) {
                            padding: 10px;
                            width: 33%;
                            margin: 0;
                          }
                        `}
                      >
                        <MediumTaskBlock />
                        <TaskMenu
                          name="medium actions"
                          tasks={categoriesToShow.filter(
                            t => t.size === "m" && !t.suppressed
                          )}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                        />
                      </div>
                      <div
                        css={css`
                          margin-top: 2rem;
                          @media screen and (min-width: 1024px) {
                            padding: 10px;
                            width: 33%;
                            margin: 0;
                          }
                        `}
                      >
                        <SmallTaskBlock />
                        <TaskMenu
                          name="small actions"
                          tasks={categoriesToShow.filter(
                            t => t.size === "s" && !t.suppressed
                          )}
                          selectedTaskIds={activityIdsToday}
                          selectTask={addTaskToDay}
                          deselectTask={deselectMatchingTasks}
                        />
                      </div>
                    </div>
                  )}

                  {!reachedLastDay && (
                    <div>
                      <h2
                        css={css`
                          margin: 50px 0 25px 0;
                          font-size: 28px;
                          font-weight: 600;
                          cursor: pointer;
                        `}
                      >
                        You legend! Let’s plan the next day.
                      </h2>
                      <Button
                        css={css`
                          color: ${Swatch.darkPink};
                          background-color: ${Swatch.white};
                          border: 2px solid ${Swatch.darkPink};
                          text-transform: none;
                          display: flex;
                          font-size: 18px;
                          font-weight: 600;
                          justify-content: space-between;
                          white-space: nowrap;
                          text-overflow: ellipsis;
                          overflow: hidden;
                        `}
                        onClick={onNextDayClick}
                      >
                        <span>
                          What can you do on {format(nextDay, "EEEE")}?
                        </span>
                        <span>➡️</span>
                      </Button>
                    </div>
                  )}
                  <div
                    css={css`
                      margin: 40px 0 50px 0;
                      @media screen and (min-width: 1024px) {
                        display: none;
                      }
                    `}
                  >
                    <span
                      onClick={onBackToCalendar}
                      css={css`
                        color: inherit;
                        font-weight: 600;
                        text-decoration: none;
                        position: relative;
                        cursor: pointer;

                        &:after {
                          content: "";
                          position: absolute;
                          bottom: -3px;
                          left: 0;
                          right: 0;
                          height: 1px;
                          background-color: ${Swatch.red};
                        }
                      `}
                    >
                      🗓 Go to Calendar View
                    </span>
                  </div>
                </article>
              </div>
            ) : (
              <div
                css={css`
                  @media screen and (min-width: 1024px) {
                    display: flex;
                    width: 100%;
                  }
                `}
              >
                <div
                  css={css`
                    ${pagePaddingHorizontalLimited}
                    margin-bottom: 30px;

                    @media screen and (min-width: 1024px) {
                      width: 25%;
                      min-width: 25%;
                    }
                  `}
                >
                  <div
                    css={css`
                      display: block;

                      @media screen and (min-width: 1024px) {
                        display: none;
                      }
                    `}
                  >
                    <div
                      css={css`
                        margin: 1em 0 0.2em;
                        margin-bottom: 10px;
                        ${font.lBold}
                        font-weight: 600;
                      `}
                    >
                      My Plan to Win.
                    </div>
                    <div
                      css={css`
                        ${font.mRegular}
                        margin-bottom: 15px;
                      `}
                    >
                      <p>
                        Once you’re done click <b>Save and Share</b> to save
                        your plan and share with friends.
                      </p>
                      <p>
                        Every day we will send your plan straight to your inbox.
                      </p>
                    </div>
                  </div>
                  <div
                    css={css`
                      display: none;

                      @media screen and (min-width: 1024px) {
                        display: block;
                      }
                    `}
                  >
                    <p
                      css={css`
                        font-weight: 600;
                        font-size: 2rem;
                      `}
                    >
                      Other Labour supporters have already committed to{" "}
                      {activityCount} tasks.
                    </p>
                    <p
                      css={css`
                        font-weight: 600;
                        font-size: 2rem;
                      `}
                    >
                      If we're going to win, we all need to step up.
                    </p>
                    {!isTodayPollingDay && (
                      <div>
                        <p
                          css={css`
                            font-size: 18px;
                          `}
                        >
                          <b>The next few days</b> are the most important in
                          this campaign. Our chance to transform Britain for the
                          many is here, but we all need to step up.{" "}
                          <b>This is what could win us this election.</b>
                        </p>
                      </div>
                    )}
                  </div>
                  <div
                    css={css`
                      padding: 10px 0;
                      display: none;

                      @media screen and (min-width: 1024px) {
                        display: block;
                      }
                    `}
                  >
                    <SignInButton />
                  </div>
                  {!isLoggedIn && (
                    <div
                      css={css`
                        display: block;

                        @media screen and (min-width: 1024px) {
                          display: none;
                        }
                      `}
                    >
                      <SignInLink />
                    </div>
                  )}
                  <Link
                    css={css`
                  ${buttonDefaultStyles}
                  background-color: ${Swatch.darkPink};
                  height: 54px;
                  font-size: 18px;
                  font-weight: 600;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                  display: none;

                  @media screen and (min-width: 1024px) {
                    display: block;
                  }

      
                `}
                    to={isLoggedIn ? "/share" : "/save"}
                  >
                    {isLoggedIn ? "Share" : "Save and Share"} 📣
                  </Link>

                  {user && user.email_confirmed && (
                    <div
                      css={css`
                        margin: 0.5em 0;
                      `}
                    >
                      <ShareBar
                        message={shareMessage}
                        url={getPlansUrl(user)}
                      />
                    </div>
                  )}
                </div>
                <div
                  css={css`
                    padding-top: 12px;
                    margin-top: 8px;
                    background-color: ${Swatch.background};
                    @media screen and (min-width: 1024px) {
                      width: 75%;
                      min-width: 75%;
                    }
                  `}
                >
                  <ActivityCalendar
                    activities={activities}
                    selectTasksForDay={selectTasksForDay}
                  />
                </div>
              </div>
            )}
          </div>
          <div
            css={css`
              @media screen and (min-width: 1024px) {
                display: none;
              }
            `}
          >
            <StickyButton onMouseDown={onSaveAndShareClick}>
              {isLoggedIn ? "Share" : "Save and Share"} 📣
            </StickyButton>
          </div>
        </Fragment>
      )}
    </Layout>
  );
};

export default CreatePlanView;
