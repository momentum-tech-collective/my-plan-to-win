/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment, useEffect } from "react";
import { RouteComponentProps, useHistory } from "react-router";

import ActivityCalendar from "../components/ActivityCalendar";
import { Button } from "../components/Button";
import Fixed from "../components/Fixed";
import Header from "../components/Header";
import SignInButton from "../components/SignInButton";
import {
  Layout,
  pagePaddingHorizontalLimited,
  Swatch,
  typeTitle
} from "../design-constants";
import { usePublicUser } from "../helpers/activity";
import { plural } from "../helpers/string";
import useMedia from "../helpers/use-media";
import { TAB_BAR_HEIGHT_DESKTOP_PX } from "../components/TabBar";
import { useFetchPublicActivities } from "../context/actions";
import { useGlobalState, useDispatch } from "../context/store";

const PublicPlanView: React.FC<RouteComponentProps<{
  userId: string;
}>> = ({
  match: {
    params: { userId }
  }
}) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [user] = usePublicUser(userId);
  const fetchPublicActivities = useFetchPublicActivities(dispatch);

  useEffect(() => {
    fetchPublicActivities(user ? user.public_key : userId);
  }, [userId]);

  const { publicActivities: activities } = useGlobalState();

  const name =
    user && user.name !== "unknown" ? user.name : "A Labour activist";
  const numberOfQuests = activities.length;
  const isDesktop = useMedia(["(min-width: 1024px)"], [true], false);

  return (
    <Layout>
      <Fixed
        fixedIf={isDesktop}
        position="top"
        additionalCss={css`
          width: 100%;
          top: 0;
          left: 0;

          @media screen and (min-width: 1024px) {
            top: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
          }
        `}
      >
        <div
          css={css`
            background: ${Swatch.red};

            @media screen and (min-width: 728px) {
              display: none;
            }
          `}
        >
          <img
            css={css`
              max-height: 200px;
              max-width: 100%;
              margin: 0 auto;
              display: block;
            `}
            src="https://i.imgur.com/HVLQ5ne.png"
            alt=""
          />
        </div>
        <div
          css={css`
            background: ${Swatch.red};

            @media screen and (max-width: 727px) {
              display: none;
            }
          `}
        >
          <div
            css={css`
              ${pagePaddingHorizontalLimited}
            `}
          >
            <Header />
          </div>
        </div>
      </Fixed>
      <div
        css={css`
          margin-top: 1em;
          margin-bottom: 1em;

          @media screen and (min-width: 1024px) {
            margin-top: 2em;
            margin-bottom: 2em;
            display: flex;
          }
        `}
      >
        <div
          css={css`
            ${pagePaddingHorizontalLimited}
            @media screen and (min-width: 1024px) {
              width: 25%;
              min-width: 25%;
            }
          `}
        >
          {user && (
            <Fragment>
              <div
                css={css`
                  display: flex;
                  width: 100%;
                `}
              >
                {user.avatar_url && (
                  <div
                    css={css`
                      background-image: url(${user.avatar_url});
                      background-size: cover;
                      background-position: center;
                      width: 80px;
                      height: 80px;
                      border-radius: 100%;
                      flex-shrink: 0;

                      @media screen and (min-width: 1024px) {
                        width: 150px;
                        height: 150px;
                      }

                      margin-right: 10px;
                    `}
                  />
                )}
                <div>
                  <div
                    css={css`
                      font-weight: bold;
                      ${typeTitle}
                    `}
                  >
                    <span
                      css={css`
                        color: ${Swatch.red};
                        text-transform: capitalize;
                      `}
                    >
                      {name}
                    </span>
                    &nbsp;
                    {numberOfQuests > 0 ? (
                      <span>
                        has committed to {plural(numberOfQuests, "action")}!
                      </span>
                    ) : (
                      <span>hasn't committed to any actions yet 😬</span>
                    )}
                  </div>
                  <p>
                    We are entering the final and <b>most important</b> phase of
                    the campaign.
                  </p>
                  <p>
                    You’ve already read Momentum’s <b>Plan to Win</b>. Now it’s
                    time to make your own and commit to doing{" "}
                    <b>three things a day</b> to help Labour win.
                  </p>
                  <p>
                    Ready? <b>Make your plan to win</b> and then share it with
                    your friends. It takes just a few minutes.
                  </p>
                </div>
              </div>
            </Fragment>
          )}
          <Button
            css={css`
              background: ${Swatch.darkPink};
              margin-bottom: 1rem;
            `}
            onMouseDown={() => history.push("/create")}
          >
            Build your Plan 🛠
          </Button>
          <SignInButton />
        </div>
        <div
          css={css`
            margin-bottom: 1em;
          `}
        ></div>
        {user && <ActivityCalendar activities={activities} />}
      </div>
    </Layout>
  );
};

export default PublicPlanView;
