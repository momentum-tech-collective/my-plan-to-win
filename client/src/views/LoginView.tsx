/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { useState } from "react";
import { RouteComponentProps } from "react-router";
import {
  Layout,
  Swatch,
  typeLargeCopy,
  pagePaddingHorizontalLimited,
  padding,
  typeCopy
} from "../design-constants";
import SignInForm from "../components/SignInForm";
import { Button } from "../components/Button";
import { signOut } from "../data/auth";
import Closebtn from "../components/Closebtn";

const LandingView: React.FC<RouteComponentProps<{ reason?: string }>> = ({
  history,
  match: {
    params: { reason }
  }
}) => {
  const [sent, setSent] = useState(false);

  return (
    <Layout>
      <div
        css={css`
          background: ${Swatch.darkPink};
          min-height: 100vh;
          height: 100%;
        `}
      >
        <div
          css={css`
          ${pagePaddingHorizontalLimited}
          padding-top: ${padding.pageVertical};
          padding-bottom: ${padding.pageVertical};
        `}
        >
          <h2>{reason === "link-expired" && "That sign in link expired. "}</h2>
          <h1
            css={css`
              display: flex;
              justify-content: space-between;
            `}
          >
            <span>Sign in to continue with an existing plan.</span>
            <Closebtn onClick={() => history.push("/")} />
          </h1>
          <SignInForm
            onSuccess={() => {
              setSent(true);
            }}
          />
          <Button
            onClick={signOut}
            css={css`
              ${typeCopy}
              margin-top: 10px;
              background: none;
              color: ${Swatch.black};
              border: 2px solid ${Swatch.black};
            `}
          >
            Or Sign Out
          </Button>
          {sent && (
            <div
              css={css`
                ${typeLargeCopy}
                text-align: center;
                font-weight: 900;
                border: 2px solid ${Swatch.black};
                color: ${Swatch.black};
                padding: 15px;
                border-radius: 10px;
                margin-top: 10px;
              `}
            >
              Sent! Check your email. 💌
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default LandingView;
