import React from "react";
import { RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

const GetUpdatesView: React.FC<RouteComponentProps<{}>> = () => {
  return (
    <div>
      <h1>Want us to send you custom updates?</h1>
      <p>
        Add your contact details if you want to get custom reminders by email or
        text.
      </p>
      <button>SUBSCRIBE TO CUSTOM UPDATES</button>
      <Link to="/create">Go back to your plan</Link>
    </div>
  );
};

export default GetUpdatesView;
