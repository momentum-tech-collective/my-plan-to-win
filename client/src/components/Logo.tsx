/** @jsx jsx */
import { jsx, css } from "@emotion/core";

import { Swatch } from "../design-constants";
import { useGlobalState } from "../context/store";

interface ILogoProps {
  toWinColor?: string
}
const Logo: React.FC<ILogoProps> = props => {
  const { user } = useGlobalState()
  const { toWinColor } = props;

  return (
    <div
      css={css`
        text-transform: uppercase;
        font-weight: 900;
        font-style: italic;
        font-size: 1.5rem;
        line-height: 0.86em;
        letter-spacing: -0.03em;
      `}
    >
      <div
        css={css`
          color: black;
        `}
      >
        {user && user.name && user.name !== "unknown" ? `${user.name}'s` : "My"}{" "}
        Plan
      </div>
      <div
        css={css`
          color: ${toWinColor || Swatch.white};
          margin-left: 1.5em;
        `}
      >
        To Win
      </div>
    </div>
  );
};

export default Logo;
