/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import Sharetitle from "./Sharetitle";
import Sharesubtitle from "./Sharesubtitle";
import { typeLargeCopy } from "../design-constants";

function ShareHeader() {
  return (
    <div
      css={css`
        ${typeLargeCopy}
        letter-spacing: -0.01em;
        margin-bottom: 10px;
      `}
    >
      <Sharetitle text="Now share your plan with as many people as possible." />
      <Sharesubtitle
        text="You are Labour’s secret weapon. We need you to step up and go big. Share this on social media and make sure to"
        kicker="tag your friends."
      />
    </div>
  );
}

export default ShareHeader;
