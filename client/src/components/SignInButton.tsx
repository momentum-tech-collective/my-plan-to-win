/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { useHistory } from "react-router";

import { Swatch } from "../design-constants";
import { Button } from "./Button";
import { useGlobalState } from "../context/store";

const SignInButton: React.FC = () => {
  const history = useHistory();
  const { isLoggedIn } = useGlobalState()

  if (isLoggedIn) {
    return null;
  }

  return (
    <Button
      css={css`
        background-color: ${Swatch.white};
        border: 2px solid ${Swatch.darkPink};
      `}
      onMouseDown={() => {
        history.push("/login");
      }}
    >
      Sign In
    </Button>
  );
};

export default SignInButton;
