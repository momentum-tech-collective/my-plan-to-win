/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import Logo from "./Logo";

import Emoji from "a11y-react-emoji";
import { Swatch, pagePaddingHorizontal } from "../design-constants";
import { useHistory } from "react-router";
function Header() {
  const history = useHistory();

  const navigateToCreate = () => history.push("/create");

  return (
    <div
      css={css`
        background: ${Swatch.red};
        cursor: pointer;
      `}
      onClick={navigateToCreate}
    >
      <div
        css={css`
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 10px;
          ${pagePaddingHorizontal}
        `}
      >
        <Logo />
        <div
          css={css`
            font-size: 2.5rem;
            display: flex;
          `}
          onClick={navigateToCreate}
        >
          <Emoji symbol="🛠" />
          <Emoji symbol="✊" />
        </div>
      </div>
    </div>
  );
}

export default Header;
