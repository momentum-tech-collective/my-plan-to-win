/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type SharecheckboxProps = {
  id: string;
  text: string;
};

function Sharecheckbox({ id, text }: SharecheckboxProps) {
  return (
    <p css={css``}>
      <input type="checkbox" id={id} name={id} />
      {text}
    </p>
  );
}

export default Sharecheckbox;
