/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type ShareInputProps = {
  id: string;
  name: string;
};

function Sharetextinput({ id, name }: ShareInputProps) {
  return (
    <p css={css``}>
      <label>
        {name}
        <input id={id} name={id} />
      </label>
    </p>
  );
}

export default Sharetextinput;
