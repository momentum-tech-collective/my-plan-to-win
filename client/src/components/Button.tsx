/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { Swatch, typeLargeCopy } from "../design-constants";
import { DetailedHTMLProps, HTMLAttributes } from "react";
import Fixed from "./Fixed";

export const buttonDefaultStyles = css`
  cursor: pointer;
  border-radius: 15px;
  padding: 15px;
  vertical-align: middle;
  text-align: center;
  font-weight: bold;
  width: 100%;
  text-transform: uppercase;
  display: block;
  text-decoration: none;
  color: inherit;
`;

export const Button: React.FC<{ css?: any } & DetailedHTMLProps<
  HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>> = ({ css: _css, children, ...props }) => (
  <div
    {...props}
    css={css`
      ${buttonDefaultStyles}
      ${typeLargeCopy}
      ${_css}
    `}
  >
    {children}
  </div>
);

export const StickyButton: React.FC<DetailedHTMLProps<
  HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>> = ({ ...props }) => (
  <Fixed
    position="bottom"
    additionalCss={css`
      left: 0;
      cursor: pointer;
      background: ${Swatch.darkPink};
      color: black;
      padding: 15px;
      vertical-align: middle;
      text-align: center;
      width: 100%;
      text-transform: uppercase;
      font-weight: bold;
      ${typeLargeCopy}
    `}
    {...props}
  />
);
