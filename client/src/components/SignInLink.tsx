/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React from "react";
import { Link } from "react-router-dom";

import { Swatch } from "../design-constants";

interface ISignInLinkProps {
  underlineColor?: string
}

const SignInLink: React.FC<ISignInLinkProps> = ({ underlineColor }) => {
  return (
    <Link
      to="/login"
      css={css`
        color: inherit;
        font-weight: 600;
        text-decoration: none;
        position: relative;
        font-size: 18px;

        &:after {
          content: "";
          position: absolute;
          bottom: -3px;
          left: 0;
          right: 0;
          height: 1px;
          background-color: ${underlineColor ? underlineColor : Swatch.red};
        }
      `}
    >
      🔐 Sign in
    </Link>
  );
};

export default SignInLink;
