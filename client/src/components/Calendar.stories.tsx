import React from "react";
import { storiesOf } from "@storybook/react";
import { withInfo } from "@storybook/addon-info";
import Calendar from "./Calendar";

storiesOf("components/Calendar", module)
  .addDecorator(withInfo)
  .add("default", () => (
    <Calendar
      currentDay={new Date(2019, 11, 3)}
      busyDays={[
        new Date(2019, 11, 3),
        new Date(2019, 11, 5),
        new Date(2019, 11, 10)
      ]}
      onSelectDay={console.log}
    />
  ));
