/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React from "react";

import Closebtn from "../components/Closebtn";
import SignupForm from "../components/SignupForm";
import { layoutFlexRow, typeLargeCopy } from "../design-constants";
import { IUser } from "../../../server/api/db";
import ShareBar from "./ShareBar";
import { shareMessage } from "../data/copy";
import Emoji from "a11y-react-emoji";
import { getPlansUrl } from "../helpers/url";

const SavePlanForm: React.FC<{
  user: IUser | undefined | false | null;
  onClosePane: (...args: any[]) => void;
}> = ({ user, onClosePane }) => {
  return (
    <div>
      <h1
        css={css`
          letter-spacing: -0.01em;
          ${layoutFlexRow}
          justify-content: space-between;
        `}
      >
        <span>Your plan, straight to your inbox.</span>
        <Closebtn onClick={onClosePane} />
      </h1>
      <p
        css={css`
          ${typeLargeCopy}
          letter-spacing: -0.01em;
        `}
      >
        Add your details below to save your plan and get <b>custom reminders</b>{" "}
        straight to your inbox.
      </p>
      <SignupForm />
      <div
        css={css`
          padding-top: 20px;
        `}
      >
        <h2
          css={css`
            ${layoutFlexRow}
            justify-content: space-between;
          `}
        >
          <span>Share this with as many people as possible.</span>
          <Emoji symbol="📢" />
        </h2>
        <p>
          You are Labour's secret weapon. We need you to step up and go big.
          Share this on social media and make sure to <b>tag your friends.</b>
        </p>
        <ShareBar message={shareMessage} url={getPlansUrl(user)} />
      </div>
    </div>
  );
};

export default SavePlanForm;
