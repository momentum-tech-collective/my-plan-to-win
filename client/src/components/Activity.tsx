/** @jsx jsx */
import { jsx, css } from "@emotion/core";

import React from "react";

import { IActivity, ICategory } from "../../../server/api/db";
import { Swatch, font } from "../design-constants";

export const categorySizeColor = (size: ICategory["size"]) => {
  switch (size) {
    case "b":
      return Swatch.red;
    case "m":
      return Swatch.purple;
    case "s":
      return Swatch.green;
    case "g":
      return Swatch.red;
    default:
      return Swatch.red;
  }
};

type ActivityProps = {
  category: ICategory;
  onClick?: (id: string) => void;
} & IActivity;

const clickableActivityCss = css`
  display: block;
  border-radius: 5px;
  padding-top: 18px;
  padding-bottom: 18px;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;
  margin-bottom: 10px;
  cursor: pointer;
`;

export const Activity: React.FC<ActivityProps> = ({
  id,
  category,
  onClick
}) => (
  <div
    css={css`
      ${clickableActivityCss}
      color: white;
      background: ${categorySizeColor(category.size)};
    `}
    onClick={onClick ? () => onClick(id) : undefined}
  >
    <div
      css={css`
        font-weight: 900;
      `}
    >
      {category.name}
    </div>
    <div dangerouslySetInnerHTML={{ __html: category.description! }} />
  </div>
);

export const ActivityPlaceholder: React.FC<{
  children: any;
  onClick?: () => void;
}> = ({ children, onClick }) => (
  <div
    onClick={onClick}
    css={css`
      ${clickableActivityCss}
      color: ${Swatch.darkPink};
      border: 2px dashed ${Swatch.darkPink};
      display: flex;
      justify-content: space-between;
      ${font.mMedium}
    `}
  >
    {children}
  </div>
);
