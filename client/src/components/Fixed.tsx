/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import React, { Fragment } from "react";
import useDimensions from "react-use-dimensions";

/**
 * Fix an element to the screen
 * and ensure content surrounding it is still visible
 * by matching its height
 */
const Fixed: React.FC<{
  fixedIf?: boolean;
  position: "top" | "bottom";
  additionalCss?: any;
} & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>> = ({
  fixedIf = true,
  position = "top",
  children,
  additionalCss,
  ...props
}) => {
  const [ref, { height }] = useDimensions();
  const placeholder = (
    <div
      css={css`
        height: ${height}px;
      `}
    />
  );

  return (
    <Fragment>
      {fixedIf && position === "bottom" && placeholder}
      <div
        {...props}
        ref={ref}
        css={css`
          position: ${fixedIf ? "fixed" : "relative"};
          ${position}: 0;
          ${additionalCss}
        `}
      >
        {children}
      </div>
      {fixedIf && position === "top" && placeholder}
    </Fragment>
  );
};

export default Fixed;
