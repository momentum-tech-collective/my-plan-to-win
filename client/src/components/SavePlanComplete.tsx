/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import Closebtn from "./Closebtn";
import { IUser } from "../../../server/api/db";
import { layoutFlexRow, typeLargeCopy } from "../design-constants";
import ShareBar from "./ShareBar";
import { Link } from "react-router-dom";
import { shareMessage } from "../data/copy";
import { getPlansUrl } from "../helpers/url";

const headerCss = css`
  letter-spacing: -0.01em;
  ${layoutFlexRow}
  justify-content: space-between;
`;

const shareCopyCss = css`
  ${typeLargeCopy}
  letter-spacing: -0.01em;
  margin-bottom: 10px;
`;

const SavePlanComplete: React.FC<{
  user: IUser;
  onClose: () => void;
}> = ({ user, onClose }) => {
  return (
    <div>
      <h1 css={headerCss}>
        <span>
          ✅ {user.name}, you’ve completed your Plan to Win this election for
          Labour.
        </span>
        <Closebtn onClick={onClose} />
      </h1>
      <p css={typeLargeCopy}>
        So much depends on the action of thousands of people like you over the
        coming days.
      </p>
      <p css={typeLargeCopy}>
        We've sent an email confirmation. Visit{" "}
        <Link to="/login">myplantowin2019.com/login</Link> to keep editing your
        plan.
      </p>
      <div css={shareCopyCss}>
        It’s absolutely vital that you <b>share your Plan to Win</b> on social
        media and make sure to tag your friends. ✊
      </div>
      <ShareBar message={shareMessage} url={getPlansUrl(user)} />
    </div>
  );
};

export default SavePlanComplete;
