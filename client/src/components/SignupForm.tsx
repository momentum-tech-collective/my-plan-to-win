/** @jsx jsx */
import { css, jsx } from "@emotion/core";

import { useField, useForm, Validation } from "react-jeff";
import { TextInput, Form as FormElement, CheckboxInput } from "./FormElements";

import * as yup from "yup";
import { buttonDefaultStyles, Button } from "./Button";
import {
  typeCopy,
  layoutFlexColumn,
  Swatch,
  typeLargeCopy
} from "../design-constants";
import { useState } from "react";

import { useHistory } from "react-router";
import { useEffect } from "react";
import { useGlobalState, useDispatch } from "../context/store";
import { useSignUp } from "../context/actions";

const emailFieldValidation = yup.string().email();
const nameFieldValidation = yup
  .string()
  .required()
  .max(255);

const isFieldValid = async (
  schema: yup.StringSchema<string>,
  value: string,
  error: string
) => {
  const errors = [];
  const isValid = await schema.isValid(value);
  if (!isValid) {
    errors.push(error);
  }
  return errors;
};

const validateEmail: Validation<string, string> = async value => {
  return isFieldValid(
    emailFieldValidation,
    value,
    "Sorry that doesn't look like an email to me"
  );
};

const validateName: Validation<string, string> = async value => {
  return isFieldValid(
    nameFieldValidation,
    value,
    "Sorry this doesn't seem like a name to me"
  );
};

const labelStyle = css`
  font-weight: 500;
  letter-spacing: -0.01em;
  font-weight: bold;
  margin-bottom: 10px;
`;

const inputStyle = css`
  background: #ffffff;
  border-radius: 10px;
  border: 0;
  height: 3rem;
  ${typeCopy}
  font-weight: 500;
  padding: 15px;
`;

const Form: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { userId, user, signUpError } = useGlobalState();
  const signUp = useSignUp(dispatch);

  useEffect(() => {
    if (!userId) {
      history.push("/");
    }
  }, [userId, user]);

  const [error, setError] = useState<string | null>(null);
  const [localError, setLocalError] = useState<string | undefined>();

  let name = useField({
    defaultValue: "",
    required: true,
    validations: [validateName]
  });

  let email = useField<string>({
    defaultValue: "",
    required: true,
    validations: [validateEmail]
  });

  const can_contact = useField<boolean>({
    defaultValue: false,
    required: true
  });

  useEffect(() => {
    setLocalError(signUpError);
  }, [signUpError]);

  useEffect(() => {
    setLocalError(undefined);
  }, [email.value]);

  let form = useForm({
    fields: [name, email, can_contact],
    onSubmit: async () => {
      if (!form.valid) {
        console.error("Invalid form");
        setError(
          "Sorry, can you check the details above because something is wrong"
        );
        return;
      }

      if (!userId) {
        console.error("User lacks user ID cookie");
        setError("Sorry, we can't save this right now");
        return;
      }

      console.log(
        `Attempting to register user with new user ID ${userId} to ${email.value}`
      );

      signUp({
        id: userId,
        name: name.value,
        email: email.value,
        can_contact: can_contact.value
      });
    }
  });

  return (
    <div
      css={css`
        ${typeCopy}
      `}
    >
      <FormElement {...form.props}>
        <div
          css={css`
            ${layoutFlexColumn}
            margin-bottom: 20px;
          `}
        >
          <label htmlFor="name" id="name" css={labelStyle}>
            First name (required)
          </label>
          <TextInput {...name.props} css={inputStyle} />
        </div>
        <div
          css={css`
            ${layoutFlexColumn}
            margin-bottom: 20px;
          `}
        >
          <label htmlFor="email" id="email" css={labelStyle}>
            Email (required)
          </label>
          <TextInput {...email.props} css={inputStyle} />
        </div>
        <div>
          <div
            css={css`
              display: flex;
              justify-content: start;
              align-items: center;
            `}
          >
            <CheckboxInput
              id="keep-updated"
              type="checkbox"
              {...can_contact.props}
              css={css`
                -webkit-appearance: none;
                -moz-appearance: none;
                vertical-align: middle;
                width: 28px;
                height: 28px;
                font-size: 28px;
                background: white;
                border: 2px solid black;
                width: 30px;
                height: 30px;
                border-radius: 3px;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-shrink: 0;

                &:checked:after {
                  content: "✔️";
                }
              `}
            />
            <label
              htmlFor="keep-updated"
              css={css`
                margin-left: 8px;
                ${typeCopy}
                font-weight: bold;
              `}
            >
              Keep me updated
            </label>
          </div>
          <p
            css={css`
              font-style: normal;
              font-weight: normal;
              font-size: 14px;
              line-height: 17px;
            `}
          >
            By providing my email and telephone number I agree to Momentum using
            the info I provide to keep me updated via email, telephone and text
            about Momentum’s campaigns and opportunities to get involved,
            locally and nationally. For more information please see our{" "}
            <a href="https://peoplesmomentum.com/privacy-policy/">
              privacy policy
            </a>
            .
          </p>
        </div>
        <div>
          <div
            css={css`
              margin-bottom: 20px;
            `}
          >
            {form.fieldErrors}
            {error}
          </div>

          <input
            type="submit"
            value="Save My Plan"
            css={css`
            ${buttonDefaultStyles}
            background: ${Swatch.black};
            border: 0;
            border-radius: 15px;
            height: 3rem;
            color: ${Swatch.white};
            text-transform: uppercase;
          `}
            disabled={form.submitting}
          />
          {localError && (
            <div
              css={css`
                margin-top: 2em;
              `}
            >
              <div css={typeLargeCopy}>
                Looks like <b>{email.value}</b> might already have a saved plan
              </div>
              <Button
                css={css`
                  ${typeCopy}
                  ${buttonDefaultStyles}
                background: rgba(0,0,0,0.2);
                  border: 0;
                  border-radius: 15px;
                  height: 3rem;
                  color: ${Swatch.white};
                  margin-top: 10px;
                `}
                onMouseDown={() => {
                  history.push("/login");
                }}
              >
                Sign back in
              </Button>
            </div>
          )}
        </div>
      </FormElement>
    </div>
  );
};

export default Form;
