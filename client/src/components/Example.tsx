/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type ExampleProps = {
  text: string;
};

const colorDarkPink = "#FF57A6";

const standardHeightAndWidth = css`
  height: 55px;
  width: 100px;
`;

const Example: React.FC<ExampleProps> = ({ text }) => {
  return (
    <div
      css={css`
        /* Dark pink */
        background: ${colorDarkPink};

        /* Dark pink */
        border: 2px solid #ff57a6;
        box-sizing: border-box;
        border-radius: 14px;

        ${standardHeightAndWidth}
      `}
    >
      Do substitution like this {text}
    </div>
  );
};

export default Example;
