/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment } from "react";
import { isTodayPollingDay } from "../data/constants";

export const TAB_BAR_HEIGHT_DESKTOP_PX = 30;
export const TAB_BAR_HEIGHT_MOBILE_PX = 50;

const tabs = [
  {
    shortCode: "mcm",
    name: "Campaign Map",
    url: "https://mycampaignmap.com"
  },
  {
    shortCode: "mptw",
    name: "Plan to Win",
    url: "https://myplantowin2019.com"
  },
  {
    shortCode: "mpd",
    name: "Polling Day",
    url: "https://www.mypollingday.com"
  }
].slice(0, isTodayPollingDay ? 2 : 3);

interface ITabProps {
  isActive: boolean;
  textContent: string;
  url: string;
}
const Tab: React.FC<ITabProps> = props => {
  const { isActive, textContent, url } = props;
  return (
    <a
      css={css`
        background-color: ${isActive ? "#3f3f3f" : "#323232"};
        color: ${isActive ? "#ffffff" : "#767676"};
        width: ${(100 / tabs.length).toFixed(5)}%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-weight: 600;
        font-size: 12px;
        text-transform: uppercase;
        text-align: center;
        text-decoration: none;
        cursor: pointer;
      `}
      href={isActive ? undefined : url}
    >
      {textContent}
    </a>
  );
};

interface ITabBarProps {
  currentSite: "mcm" | "mptw" | "mpd";
}
const TabBar: React.FC<ITabBarProps> = props => {
  const { currentSite } = props;

  return (
    <Fragment>
      <div
        css={css`
          height: ${TAB_BAR_HEIGHT_MOBILE_PX}px;
          width: 100%;

          @media screen and (min-width: 1024px) {
            height: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
          }
        `}
      ></div>
      <nav
        css={css`
          position: fixed;
          top: 0;
          left: 0;
          right: 0;
          height: ${TAB_BAR_HEIGHT_MOBILE_PX}px;
          display: flex;
          z-index: 999;

          a:nth-child(2) {
            border-left: 2px solid #767676;
            border-right: 2px solid #767676;
          }

          @media screen and (min-width: 1024px) {
            height: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
          }
        `}
      >
        {tabs.map(({ shortCode, name, url }) => {
          return (
            <Tab
              key={`tab-bar__tab--${url}`}
              isActive={currentSite === shortCode}
              textContent={name}
              url={url}
            />
          );
        })}
      </nav>
    </Fragment>
  );
};

export default TabBar;
