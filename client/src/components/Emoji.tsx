/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type EmojiProps = {
  text: string;
};

function Emoji({ text }: EmojiProps) {
  return (
    <p css={css``}>
      <span role="img" css={css``}>
        {" "}
        {text}
      </span>
    </p>
  );
}

export default Emoji;
