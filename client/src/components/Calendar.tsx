/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { format, isBefore, isSameDay } from "date-fns";
import {
  allDays,
  pollingDay,
  yesterday,
  isTodayPollingDay
} from "../data/constants";
import { Swatch, font, pagePaddingHorizontal } from "../design-constants";
import Logo from "./Logo";
import { Link } from "react-router-dom";
import { buttonDefaultStyles } from "./Button";
import { useGlobalState } from "../context/store";

const MOBILE_ROW_COUNT = 2;

const dayBoxDimensionsCss = css`
  position: relative;
  width: ${(1 / Math.round(allDays.length / MOBILE_ROW_COUNT)) * 100}%;

  @media screen and (min-width: 768px) {
    width: ${(1 / Math.round(allDays.length)) * 100}%;
  }
`;

const dayBoxCss = css`
  border-radius: 5px;
  margin: 5px;
  padding-top: 7px;
  padding-bottom: 7px;
  text-align: center;
  cursor: pointer;
`;

const busyIndicatorCss = css`
  border-radius: 100px;
  width: 6px;
  height: 6px;
  background: ${Swatch.darkPink};
  margin: 2px auto;
`;

const Calendar: React.FC<{
  currentDay?: Date;
  highlightedDay?: Date;
  busyDays?: Date[];
  onSelectDay?: (day: Date, index: number) => void;
}> = ({
  currentDay = new Date(),
  highlightedDay,
  busyDays = [],
  onSelectDay
}) => {
  const { isLoggedIn } = useGlobalState();

  const isHighlightedDay = (day: Date) =>
    isSameDay(day, highlightedDay ? highlightedDay : currentDay);
  const isPast = (day: Date) => isBefore(day, yesterday);
  const isPollingDay = (day: Date) => isSameDay(day, pollingDay);

  return (
    <div
      css={css`
        background: ${Swatch.midGrey};
        display: flex;

        @media screen and (min-width: 1024px) {
          ${pagePaddingHorizontal}
          box-shadow: 0px 10px 8px rgba(172, 172, 172, 0.2);
        }
      `}
    >
      <div
        css={css`
          display: none;
          @media screen and (min-width: 1024px) {
            display: block;
            padding-top: 20px;
            width: 25%;
            min-width: 25%;
          }
        `}
      >
        <Link
          css={css`
            text-decoration: none;
          `}
          to="/"
        >
          <Logo toWinColor={Swatch.red} />
        </Link>
      </div>
      <div
        css={css`
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          flex-grow: 1;
          font-family: sans-serif;
          padding-top: 10px;
          padding-bottom: 15px;
          padding-left: 15px;
          padding-right: 15px;

          @media screen and (min-width: 1024px) {
            max-width: 50%;
          }
        `}
      >
        {allDays.map((day, index) => {
          const background = isHighlightedDay(day)
            ? Swatch.darkPink
            : isPollingDay(day)
            ? Swatch.red
            : Swatch.white;

          const color = isHighlightedDay(day)
            ? Swatch.white
            : isPollingDay(day)
            ? Swatch.white
            : !isPast(day)
            ? Swatch.darkGrey
            : Swatch.midGrey;

          const shouldShowTick = isPast(day);

          const onDayClick = () => {
            if (onSelectDay && !isPast(day)) {
              onSelectDay(day, index);
            }
          };

          return (
            <div key={index} css={dayBoxDimensionsCss} onMouseDown={onDayClick}>
              {shouldShowTick && (
                <div
                  css={css`
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 0;
                    bottom: 0;
                    font-size: 2rem;
                    text-align: center;
                    padding-top: 9px;
                  `}
                >
                  ✔️
                </div>
              )}
              <div
                css={css`
                  background: ${background};
                  color: ${color};
                  ${dayBoxCss}
                `}
              >
                <div
                  css={css`
                    ${font.xsRegular}
                    margin-bottom: 2px;
                  `}
                >
                  {format(day, "EEE").toUpperCase()}
                </div>
                <div
                  css={css`
                    ${font.lBold}
                  `}
                >
                  {format(day, "d")}
                </div>
              </div>
              {busyDays.some(d => isSameDay(d, day)) && (
                <div css={busyIndicatorCss} />
              )}
            </div>
          );
        })}
      </div>
      <div
        css={css`
          display: none;
          @media screen and (min-width: 1024px) {
            display: block;
            padding-top: 15px;
            padding-right: 10px;
            margin-left: auto;
          }
        `}
      >
        <Link
          css={css`
            ${buttonDefaultStyles}
            background-color: ${
              isTodayPollingDay ? Swatch.red : Swatch.darkPink
            };
            color: ${isTodayPollingDay ? Swatch.white : Swatch.black};
            height: 54px;
            font-size: 18px;
            font-weight: 600;
            display: flex;
            align-items: center;
          `}
          to={(isLoggedIn || isTodayPollingDay) ? "/share" : "/save"}
        >
          {(isLoggedIn || isTodayPollingDay) ? "Share" : "Save and Share"} 📣
        </Link>
      </div>
    </div>
  );
};

export default Calendar;
