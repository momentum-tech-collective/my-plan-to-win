/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import { useState } from "react";
import { useField, useForm, Validation } from "react-jeff";
import * as yup from "yup";

import { requestLoginEmail } from "../api";
import { layoutFlexColumn, Swatch, typeCopy } from "../design-constants";
import { buttonDefaultStyles } from "./Button";
import { Form as FormElement, TextInput } from "./FormElements";

type FormProps = {
  onSuccess: () => void;
};

const emailFieldValidation = yup.string().email();
const nameFieldValidation = yup
  .string()
  .required()
  .max(255);

const isFieldValid = async (
  schema: yup.StringSchema<string>,
  value: string,
  error: string
) => {
  const errors = [];
  const isValid = await schema.isValid(value);
  if (!isValid) {
    errors.push(error);
  }
  return errors;
};

const validateEmail: Validation<string, string> = async value => {
  return isFieldValid(
    emailFieldValidation,
    value,
    "Sorry that doesn't look like an email to me"
  );
};

const validateName: Validation<string, string> = async value => {
  return isFieldValid(
    nameFieldValidation,
    value,
    "Sorry this doesn't seem like a name to me"
  );
};

const labelStyle = css`
  font-weight: 500;
  letter-spacing: -0.01em;
  font-weight: bold;
  margin-bottom: 10px;
`;

const inputStyle = css`
  background: #ffffff;
  border-radius: 10px;
  border: 0;
  height: 3rem;
  ${typeCopy}
  font-weight: 500;
  padding: 15px;
`;

const SignInForm: React.FC<FormProps> = ({ onSuccess }) => {
  const [error, setError] = useState<string | null>(null);
  let email = useField<string>({
    defaultValue: "",
    required: true,
    validations: [validateEmail]
  });

  let form = useForm({
    fields: [email],
    onSubmit: async () => {
      setError(null);

      if (!form.valid) {
        console.error("Invalid form");
        setError(
          "Sorry, can you check the details above because something is wrong"
        );
        return;
      }

      try {
        await requestLoginEmail(email.value);
        onSuccess();
      } catch (e) {
        setError(
          "Email address not found. To create an account, first go back and Save your plan"
        );
      }
    }
  });

  return (
    <div
      css={css`
        ${typeCopy}
      `}
    >
      <FormElement {...form.props}>
        <div
          css={css`
            ${layoutFlexColumn}
            margin-bottom: 20px;
          `}
        >
          <label htmlFor="email" id="email" css={labelStyle}>
            Email (required)
          </label>
          <TextInput {...email.props} css={inputStyle} />
        </div>
        <div>
          <div
            css={css`
              margin-bottom: 20px;
            `}
          >
            {form.fieldErrors}
            {form.fieldErrors.length ? error && <br /> : null}
            {error}
          </div>

          <input
            type="submit"
            value="Send me a sign-in email"
            css={css`
            ${buttonDefaultStyles}
            background: ${Swatch.black};
            border: 0;
            border-radius: 15px;
            height: 3rem;
            color: ${Swatch.white};
            text-transform: uppercase;
          `}
            disabled={form.submitting}
          />
        </div>
      </FormElement>
    </div>
  );
};

export default SignInForm;
