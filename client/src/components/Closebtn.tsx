/** @jsx jsx */
import { jsx, css } from "@emotion/core";

interface ClosebtnProps {
  onClick: () => void;
}

const Closebtn: React.FC<ClosebtnProps> = ({ onClick }) => {
  return (
    <button
      css={css`
        height: 36px;
        padding: 3px 0;
        background: transparent;
        font-size: large;
        border: none;
        color: inherit;
      `}
      onClick={onClick}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="30"
        height="30"
        viewBox="0 0 30 30"
        fill="none"
      >
        <path
          d="M24 7.81286L22.1871 6L15 13.1871L7.81286 6L6 7.81286L13.1871 15L6 22.1871L7.81286 24L15 16.8129L22.1871 24L24 22.1871L16.8129 15L24 7.81286Z"
          fill="currentColor"
        />
      </svg>
    </button>
  );
};

export default Closebtn;
