/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { Swatch } from "../design-constants";
import ShareHeader from "./ShareHeader";

function Sharemodal() {
  return (
    <div
      css={css`
        background: ${Swatch.darkPink};
        width: 375px;
        height: 1081px;
      `}
    >
      <ShareHeader />
    </div>
  );
}

export default Sharemodal;
