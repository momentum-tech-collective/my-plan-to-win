/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import Postcode from "postcode";
import { useState } from "react";
import { useField, useForm, Validation } from "react-jeff";
import * as yup from "yup";

import { IUser } from "../../../server/api/db";
import { getUser, patchUserData } from "../api";
import { useUserId } from "../data/auth";
import { layoutFlexColumn, Swatch, typeCopy } from "../design-constants";
import { buttonDefaultStyles } from "./Button";
import { CheckboxInput, Form as FormElement, TextInput } from "./FormElements";

type CustomUpdatesFormProps = {
  initialName: string;
  initialEmail: string;
  onSuccess: (createdUser: IUser) => void;
};

const emailFieldValidation = yup.string().email();
const nameFieldValidation = yup
  .string()
  .required()
  .max(255);

const isFieldValid = async (
  schema: yup.StringSchema<string>,
  value: string,
  error: string
) => {
  const errors = [];
  const isValid = await schema.isValid(value);
  if (!isValid) {
    errors.push(error);
  }
  return errors;
};

export const validatePostcode = (postcode: string) => {
  const p = new Postcode(postcode.trim());
  return Boolean(p.valid() && p.incode())
    ? []
    : ["That doesn't look like a valid postcode"];
};

const validateEmail: Validation<string, string> = async value => {
  return isFieldValid(
    emailFieldValidation,
    value,
    "Sorry that doesn't look like an email to me"
  );
};

const validateName: Validation<string, string> = async value => {
  return isFieldValid(
    nameFieldValidation,
    value,
    "Sorry this doesn't seem like a name to me"
  );
};

const labelStyle = css`
  font-weight: 500;
  letter-spacing: -0.01em;
  font-weight: bold;
  margin-bottom: 10px;
`;

const inputStyle = css`
  background: #ffffff;
  border-radius: 10px;
  border: 0;
  height: 3rem;
  ${typeCopy}
  font-weight: 500;
  padding: 15px;
`;

const CustomUpdatesForm: React.FC<CustomUpdatesFormProps> = ({
  initialName,
  initialEmail,
  onSuccess
}) => {
  const userId = useUserId();

  const [error, setError] = useState<string | null>(null);

  const name = useField({
    defaultValue: initialName,
    required: true,
    validations: [validateName]
  });

  const email = useField<string>({
    defaultValue: initialEmail,
    required: true,
    validations: [validateEmail]
  });

  const mobile = useField({
    defaultValue: "",
    required: true
  });

  const postcode = useField<string>({
    defaultValue: "",
    required: true,
    validations: [validatePostcode]
  });

  const driver_with_car = useField<boolean>({
    defaultValue: false
  });

  let form = useForm({
    fields: [name, email, mobile, postcode, driver_with_car],
    onSubmit: async () => {
      if (!form.valid) {
        setError(
          "Sorry, can you check the details above because something is wrong"
        );
        return;
      }

      if (!userId) {
        console.error("User lacks user ID");
        setError("Sorry, we can't save this right now");
        return;
      }

      console.log(
        `Attempting to register user with new user ID ${userId} to ${email.value}`
      );

      try {
        await patchUserData(userId, {
          id: userId,
          name: name.value,
          email: email.value,
          mobile: mobile.value,
          postcode: postcode.value,
          driver_with_car: driver_with_car.value
        });
      } catch (e) {}

      console.log("User successfully registered");

      const userResponse = await getUser(userId);

      if (userResponse.status !== 200) {
        console.error("User could not be retrieved");
        return;
      }

      onSuccess(userResponse.data);
    }
  });

  return (
    <div
      css={css`
        ${typeCopy}
      `}
    >
      <FormElement {...form.props}>
        {/* <div
                    css={css`
                        ${layoutFlexColumn}
                        margin-bottom: 20px;
                    `}
                >
                    <label htmlFor="name" id="name" css={labelStyle}>
                        First name (required)
                    </label>
                    <TextInput {...name.props} css={inputStyle} />
                </div>
                <div
                    css={css`
                        ${layoutFlexColumn}
                        margin-bottom: 20px;
                    `}
                >
                    <label htmlFor="email" id="email" css={labelStyle}>
                        Email (required)
                    </label>
                    <TextInput {...email.props} css={inputStyle} />
                </div> */}
        <div
          css={css`
            ${layoutFlexColumn}
            margin-bottom: 20px;
          `}
        >
          <label htmlFor="mobile" id="mobile" css={labelStyle}>
            Mobile
          </label>
          <TextInput {...mobile.props} css={inputStyle} />
        </div>
        <div
          css={css`
            ${layoutFlexColumn}
            margin-bottom: 20px;
          `}
        >
          <label htmlFor="postcode" id="postcode" css={labelStyle}>
            Postcode
          </label>
          <TextInput {...postcode.props} css={inputStyle} />
        </div>
        <div>
          <div
            css={css`
              display: flex;
              justify-content: start;
              align-items: center;
            `}
          >
            <CheckboxInput
              id="can-drive"
              type="checkbox"
              {...driver_with_car.props}
              css={css`
                -webkit-appearance: none;
                -moz-appearance: none;
                vertical-align: middle;
                width: 28px;
                height: 28px;
                font-size: 28px;
                background: white;
                border: 2px solid black;
                width: 30px;
                height: 30px;
                border-radius: 3px;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-shrink: 0;

                &:checked:after {
                  content: "✔️";
                }
              `}
            />
            <label
              htmlFor="can-drive"
              css={css`
                margin-left: 8px;
                ${typeCopy}
                font-weight: bold;
              `}
            >
              Can you drive and do you have a car? To get you to the marginal
              that needs you most, we may need you to take a road trip to
              canvass!
            </label>
          </div>
        </div>
        <div>
          <div
            css={css`
              margin-bottom: 20px;
            `}
          >
            {form.fieldErrors}
            {error}
          </div>

          <input
            type="submit"
            value="GET CUSTOM UPDATES"
            css={css`
                            ${buttonDefaultStyles}
                            background: ${Swatch.black};
                            border: 0;
                            border-radius: 15px;
                            height: 3rem;
                            color: ${Swatch.white};
                            text-transform: uppercase;
                        `}
            disabled={form.submitting || form.submitted}
          />
        </div>
      </FormElement>
    </div>
  );
};

export default CustomUpdatesForm;
