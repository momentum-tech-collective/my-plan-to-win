/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type SubtitleProps = {
  text: string;
  kicker: string;
};

function Sharetitle({ text, kicker }: SubtitleProps) {
  return (
    <p css={css``}>
      {text}
      <span
        css={css`
          font-weight: bold;
        `}
      >
        {" "}
        {kicker}
      </span>
    </p>
  );
}

export default Sharetitle;
