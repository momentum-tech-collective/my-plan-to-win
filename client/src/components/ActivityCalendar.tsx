/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import React from "react";

import { daysLeft, pollingDay } from "../data/constants";
import { Swatch, pagePaddingHorizontal, font } from "../design-constants";
import { format, isSameDay, differenceInDays } from "date-fns";
import { IActivity } from "../../../server/api/db";
import { ActivityPlaceholder, Activity } from "./Activity";
import { pluckActivityCategory } from "../helpers/activity";
import { checkpointsByDaysLeft } from "../data/copy";
import { useGlobalState } from "../context/store";

const ActivityCalendar: React.FC<{
  activities?: IActivity[];
  selectTasksForDay?: (day: Date) => void;
}> = ({ activities = [], selectTasksForDay }) => {
  const { categories } = useGlobalState();

  return (
    <div
      css={css`
        ${pagePaddingHorizontal}

        @media screen and (min-width: 1024px) {
          display: flex;
          flex-wrap: wrap;
        }
      `}
    >
      {daysLeft.map((day, index) => {
        const daysBeforePollingDay = differenceInDays(pollingDay, day);
        const checkpoint = checkpointsByDaysLeft[daysBeforePollingDay];

        return (
          <div
            key={index}
            css={css`
              @media screen and (min-width: 1024px) {
                width: 33.333%;
                padding: 10px;
                min-height: 200px;
              }
            `}
          >
            <div
              css={css`
                border-top: 1px solid ${Swatch.darkGrey};
                padding-top: 10px;
              `}
            >
              {/* Calendar items */}
              <div
                css={css`
                  display: flex;
                  justify-content: space-between;
                `}
              >
                <div
                  css={css`
                    display: flex;
                    justify-content: start;
                    flex-direction: column;
                    min-width: 50px;
                    width: 10%;
                    color: ${Swatch.darkGrey};
                  `}
                >
                  <div
                    css={css`
                      ${font.xsRegular}
                    `}
                  >
                    {format(new Date(day), "E")}
                  </div>
                  <div
                    css={css`
                      ${font.lBold}
                    `}
                  >
                    {format(new Date(day), "d")}
                  </div>
                </div>
                <div
                  css={css`
                    display: flex;
                    justify-content: space-between;
                    flex-direction: column;
                    width: 100%;
                  `}
                >
                  {!!categories.length &&
                    activities.length > 0 &&
                    activities
                      .filter(activity =>
                        isSameDay(new Date(activity.date), day)
                      )
                      .map(activity => (
                        <Activity
                          key={activity.id}
                          {...activity}
                          category={pluckActivityCategory(
                            activity.category_id,
                            categories
                          )}
                          onClick={
                            selectTasksForDay
                              ? () => selectTasksForDay(day)
                              : undefined
                          }
                        />
                      ))}
                  {selectTasksForDay &&
                    (activities.filter(activity =>
                      isSameDay(new Date(activity.date), day)
                    ).length === 0 ? (
                      <div
                        css={css`
                          display: flex;
                          justify-content: space-between;
                          width: 100%;
                        `}
                        onClick={() => void selectTasksForDay(day)}
                      >
                        <ActivityPlaceholder>
                          <span>
                            What can you do on {format(day, "EEEE") as any}?
                          </span>
                          <span>✊</span>
                        </ActivityPlaceholder>
                      </div>
                    ) : (
                      <ActivityPlaceholder
                        onClick={() => void selectTasksForDay(day)}
                      >
                        <span>Add some more</span>
                        <span>✊</span>
                      </ActivityPlaceholder>
                    ))}
                </div>
              </div>
              {/* Special banners */}
              <div
                css={css`
                  margin: 10px 0 15px;
                  @media screen and (min-width: 1024px) {
                    display: none;
                  }
                `}
              >
                {checkpoint && checkpoint.content}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ActivityCalendar;
