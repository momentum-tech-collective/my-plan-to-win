/** @jsx jsx */
import { jsx, css } from "@emotion/core";

import FacebookShareIcon from "./social-network-icons/FacebookIcon";
import TwitterShareIcon from "./social-network-icons/TwitterIcon";
import WhatsAppShareIcon from "./social-network-icons/WhatsappIcon";
import EmailShareIcon from "./social-network-icons/EmailIcon";

export const smallSpacing = "15px";

// Sharing URL functionality adapted from react-social-sharing
// https://github.com/SaraVieira/react-social-sharing
function facebookShareUrl(link: string) {
  return `https://facebook.com/sharer/sharer.php?u=${encodeURIComponent(link)}`;
}

function twitterShareUrl(link: string, message: string) {
  return `https://twitter.com/intent/tweet/?text=${encodeURIComponent(
    message
  )}&url=${encodeURIComponent(link)}`;
}

function whatsAppShareUrl(link: string, message: string) {
  return `whatsapp://send?text=${encodeURIComponent(
    message
  )}%20${encodeURIComponent(link)}`;
}

function emailShareUrl(link: string, subject: string, body?: string) {
  return `mailto:?subject=${encodeURIComponent(
    subject || ""
  )}&body=${encodeURIComponent((body && `${body}\n\n${link}`) || link)}`;
}

type ShareBarProps = {
  message: string;
  url: string;
};

const shareIconCSS = css`
  a {
    margin-right: ${smallSpacing};
  }
`;

const ShareBar: React.FC<ShareBarProps> = ({ message, url }) => {
  return (
    <div css={shareIconCSS}>
      <a href={facebookShareUrl(url)} target="_blank" rel="noopener noreferrer">
        <FacebookShareIcon />
      </a>
      <a
        href={twitterShareUrl(url, `${message} #MyPlanToWin #GetOutTheVote`)}
        target="_blank"
        rel="noopener noreferrer"
      >
        <TwitterShareIcon />
      </a>
      <a
        href={whatsAppShareUrl(url, message)}
        target="_blank"
        rel="noopener noreferrer"
      >
        <WhatsAppShareIcon />
      </a>
      <a
        href={emailShareUrl(
          url,
          "This is my plan for the next 12 days",
          message
        )}
        target="_blank"
        rel="noopener noreferrer"
      >
        <EmailShareIcon />
      </a>
    </div>
  );
};

export default ShareBar;
