/** @jsx jsx */
import { jsx, css } from "@emotion/core";

type HeadingProps = {
  text: string;
};

function Sharetitle({ text }: HeadingProps) {
  return (
    <p
      css={css`
        font-weight: bold;
        font-size: x-large;
        margin: 0;
      `}
    >
      {text}
    </p>
  );
}

export default Sharetitle;
