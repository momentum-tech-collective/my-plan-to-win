/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment } from "react";
import { Swatch, pagePaddingHorizontalLimited } from "../../design-constants";
import { useHistory } from "react-router-dom";

import { StickyButton } from "../Button";
import { TAB_BAR_HEIGHT_MOBILE_PX } from "../TabBar";

import Countdown from "./Countdown";
import Copy from "./Copy";
import Tasks from "./Tasks";
import { useGlobalState } from "../../context/store";

const MobilePollingDayLanding: React.FC = () => {
  const history = useHistory();
  const { categories } = useGlobalState();

  const pollingDayTasks = categories.filter(
    t => t.size === "g" && !t.suppressed
  );

  return (
    <Fragment>
      <header
        css={css`
          position: fixed;
          top: ${TAB_BAR_HEIGHT_MOBILE_PX}px;
          left: 0;
          right: 0;
          z-index: 1;
        `}
      >
        <Countdown />
      </header>
      <Copy />
      <Tasks tasks={pollingDayTasks} />
      <StickyButton
        css={css`
          color: white;
          background-color: ${Swatch.red};
          font-weight: 600;
        `}
        onClick={() => history.push("/share")}
      >
        Share 📣
      </StickyButton>
    </Fragment>
  );
};

export default MobilePollingDayLanding;
