/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment } from "react";
import { isSameDay } from "date-fns";
import uuidv1 from "uuid/v1";

import { ICategory, IActivity } from "../../../../server/api/db";
import { useDispatch, useGlobalState } from "../../context/store";
import { useDeleteActivity, useCreateActivity } from "../../context/actions";
import { pollingDay } from "../../data/constants";
import Emoji from "a11y-react-emoji";
import { Swatch } from "../../design-constants";
import { reorderTasksInImportance } from "../../helpers/polling-day";

interface CheckMarkProps {
  isChecked: boolean;
}

const CheckMark: React.FC<CheckMarkProps> = props => {
  const { isChecked } = props;

  return (
    <div
      css={css`
        width: 25px;
        height: 25px;
        border: 2px solid ${Swatch.red};
        border-radius: 4px;
        position: absolute;
        top: 3px;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 28px;
      `}
    >
      {isChecked && <Emoji symbol="✔" />}
    </div>
  );
};

interface TasksProps {
  tasks: ICategory[];
}

const Tasks: React.FC<TasksProps> = props => {
  const dispatch = useDispatch();
  const deleteActivity = useDeleteActivity(dispatch);
  const createActivity = useCreateActivity(dispatch);

  const { activities, userId } = useGlobalState();
  const { tasks } = props;

  const activitiesMatchingCategoryAndDate = (categoryId: string, date: Date) =>
    activities.filter(
      a => a.category_id === categoryId && isSameDay(new Date(a.date), date)
    );

  const deselectTask = (categoryId: string) => {
    const activities = activitiesMatchingCategoryAndDate(
      categoryId,
      pollingDay
    );
    activities.forEach(activity => deleteActivity(activity.id));
  };

  const selectedTasks = activities
    .filter(a => isSameDay(new Date(a.date), pollingDay))
    .map(a => a.category_id);

  const selectTask = (id: string) => {
    const date = pollingDay;

    const newActivity: IActivity = {
      id: uuidv1(),
      category_id: id,
      user_id: userId || "",
      date
    };

    createActivity(newActivity);
  };

  const isSelected = (category: ICategory) => {
    return selectedTasks.includes(category.id);
  };

  const resortedTasks = reorderTasksInImportance(tasks);

  return (
    <Fragment>
      {resortedTasks.map(category => {
        const { name, description } = category;
        const selected = isSelected(category);
        const onClick = selected
          ? () => deselectTask(category.id)
          : () => selectTask(category.id);

        return (
          <div
            css={css`
              position: relative;
              margin-bottom: 20px;
              cursor: pointer;
            `}
            onClick={onClick}
          >
            <CheckMark isChecked={selected} />
            <div
              css={css`
                margin-left: 35px;
              `}
            >
              <h2
                css={css`
                  font-weight: 600;
                  font-size: 28px;
                  line-height: 100%;
                  letter-spacing: -0.01em;

                  margin-bottom: 5px;
                `}
              >
                {name}
              </h2>
              {description && (
                <div
                  css={css`
                    font-weight: 500;
                    font-size: 15px;
                    line-height: 17px;
                    letter-spacing: -0.01em;
                  `}
                  dangerouslySetInnerHTML={{ __html: description }}
                />
              )}
            </div>
          </div>
        );
      })}
    </Fragment>
  );
};

export default Tasks;
