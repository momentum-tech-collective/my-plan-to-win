/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment } from "react";
import { isSameDay } from "date-fns";
import { Swatch } from "../../design-constants";
import { useGlobalState } from "../../context/store";

const Copy = () => {
  const { activityCount } = useGlobalState()
  return (
    <Fragment>
      <h1
        css={css`
          font-weight: 600;
          font-size: 28px;
          line-height: 100%;
          letter-spacing: -0.01em;
        `}
      >
        Want Labour to win?
        <br />
        <span
          css={css`
            color: ${Swatch.red};
          `}
        >
          It’s up to you.
        </span>
      </h1>
      <div
        css={css`
          margin-bottom: 40px;
        `}
      >
        <p>
          Winning all comes down to one thing: enough{" "}
          <b>voters in key marginals</b> putting a cross next to Labour in the
          ballot box today.
        </p>
        <p>
          Other Labour supporters have already committed to {activityCount}{" "}
          tasks. Every single one of us must play our part in{" "}
          <b>getting the vote out today.</b>
        </p>
      </div>
    </Fragment>
  );
};

export default Copy;
