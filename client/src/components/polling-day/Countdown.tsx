/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import React, { Fragment, useState, useEffect } from "react";
import { isSameDay } from "date-fns";
import { TAB_BAR_HEIGHT_MOBILE_PX } from "../TabBar";
import { Swatch, pagePaddingHorizontalLimited } from "../../design-constants";
import { getTimeUntilPollsClose } from "../../data/constants";
import useMedia from "../../helpers/use-media";

const Countdown = () => {
  const [timeLeft, setTimeLeft] = useState(getTimeUntilPollsClose());

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(getTimeUntilPollsClose());
    }, 1000);
  });

  const isDesktop = useMedia(["(min-width: 1024px)"], [true], false);

  const { hours, minutes, seconds } = timeLeft;

  return (
    <div
      css={css`
        width: 100%;
        padding: 15px 0;
        background-color: ${Swatch.red};
        color: ${Swatch.white};
      `}
    >
      <div
        css={css`
          ${pagePaddingHorizontalLimited}
        `}
      >
        <h2
          css={css`
            font-weight: 900;
            font-size: 8vw;
            line-height: 90%;
            letter-spacing: -0.01em;
            text-transform: uppercase;
            margin-bottom: 5px;

            @media screen and (min-width: 1024px) {
              font-size: 32px;
            }
          `}
        >
          {hours > 0 && (
            <Fragment>
              {hours}{" "}
              <span
                css={css`
                  color: ${Swatch.black};
                `}
              >
                hour
                {hours === 1 ? "" : "s"}
              </span>{" "}
              {isDesktop && <br />}
            </Fragment>
          )}
          {minutes}{" "}
          <span
            css={css`
              color: ${Swatch.black};
            `}
          >
            minute
            {minutes === 1 ? "" : "s"}
          </span>
          {isDesktop && <br />}
          {hours < 1 && (
            <Fragment>
              {seconds}{" "}
              <span
                css={css`
                  color: ${Swatch.black};
                `}
              >
                second
                {seconds === 1 ? "" : "s"}
              </span>
            </Fragment>
          )}
        </h2>
        <p
          css={css`
            font-weight: 600;
            font-size: 20px;
            line-height: 110%;
            letter-spacing: -0.01em;
            margin: 0;
          `}
        >
          until polls close! 🚀 
        </p>
      </div>
    </div>
  );
};

export default Countdown;
