/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import Emoji from "a11y-react-emoji";
import { useGlobalState } from "../../context/store";
import { pollingDay } from "../../data/constants";
import { Swatch } from "../../design-constants";
import useMedia from "../../helpers/use-media";
import { buttonDefaultStyles } from "../Button";
import Calendar from "../Calendar";
import Fixed from "../Fixed";
import { TAB_BAR_HEIGHT_DESKTOP_PX } from "../TabBar";
import Copy from "./Copy";
import Countdown from "./Countdown";
import Tasks from "./Tasks";
import { reorderTasksInImportance } from "../../helpers/polling-day";

const Create = () => {
  const isDesktop = useMedia(["(min-width: 1024px)"], [true], false);
  const { categories } = useGlobalState();
  const pollingDayTasks = categories.filter(
    t => t.size === "g" && !t.suppressed
  );

  const resortedTasks = reorderTasksInImportance(pollingDayTasks);

  return (
    <div>
      <Fixed
        position="top"
        fixedIf={isDesktop}
        additionalCss={css`
          width: 100%;
          left: 0;
          top: 0;
          z-index: 9;

          @media screen and (min-width: 1024px) {
            top: ${TAB_BAR_HEIGHT_DESKTOP_PX}px;
          }
        `}
      >
        <Calendar highlightedDay={pollingDay} onSelectDay={() => {}} />
      </Fixed>
      <div
        css={css`
          display: flex;
          height: calc(100vh - ${89 + TAB_BAR_HEIGHT_DESKTOP_PX}px);
        `}
      >
        <div
          css={css`
            width: 25%;
            min-width: 25%;
            display: flex;
            flex-direction: column;
          `}
        >
          <Countdown />
          <div
            css={css`
              padding: 20px;
              flex-grow: 1;
              display: flex;
              flex-direction: column;
              justify-content: space-between;
            `}
          >
            <div>
              <Copy />
            </div>
            <div>
              <h2
                css={css`
                  font-weight: 600;
                  font-size: 28px;
                  line-height: 100%;
                  letter-spacing: -0.01em;
                `}
              >
                🤔 New to campaigning?
              </h2>
              <p
                css={css`
                  font-weight: 500;
                  font-size: 15px;
                  line-height: 17px;
                  letter-spacing: -0.01em;
                `}
              >
                We’ve put together a guide with{" "}
                <b>everything you need to know</b> about Polling Day.
              </p>
              <a
                css={css`
                  ${buttonDefaultStyles};
                  background-color: ${Swatch.white};
                  color: ${Swatch.red};
                  border: 2px solid ${Swatch.red};
                  display: flex;
                  height: 54px;
                  font-size: 18px;
                  font-weight: 600;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                  margin-bottom: 12px;
                `}
                href={
                  "https://peoplesmomentum.com/wp-content/uploads/2019/11/Canvassing-to-Win-FINAL-FINAL.pdf"
                }
                target="_blank"
              >
                <span>Read the Guide</span>
                <Emoji symbol="➡️" />
              </a>
              <a
                css={css`
                  ${buttonDefaultStyles};
                  background-color: ${Swatch.white};
                  color: ${Swatch.red};
                  border: 2px solid ${Swatch.red};
                  display: flex;
                  height: 54px;
                  font-size: 18px;
                  font-weight: 600;
                  display: flex;
                  align-items: center;
                  justify-content: space-between;
                `}
                href={
                  "https://docs.google.com/document/d/1R4olixCWAW9OoH8ZoRQsJ0yftExVzbMAHd4--XzVOek/edit"
                }
                target="_blank"
              >
                <span>Campaign from Home</span>
                <Emoji symbol="🏡️" />
              </a>
            </div>
          </div>
        </div>
        <div
          css={css`
            width: 75%;
            min-width: 75%;
            background-color: ${Swatch.background};
            padding: 20px;
            display: flex;
          `}
        >
          <div
            css={css`
              width: 33.33333%;
              padding-right: 20px;
            `}
          >
            <Tasks
              tasks={resortedTasks.filter((task, i, arr) => i < arr.length / 2)}
            />
          </div>
          <div
            css={css`
              width: 33.333333%;
              padding-right: 20px;
            `}
          >
            <Tasks
              tasks={resortedTasks.filter(
                (task, i, arr) => i >= arr.length / 2
              )}
            />
          </div>
          <div
            css={css`
              width: 33.333333%;
              padding-right: 20px;
              display: flex;
              align-items: flex-end;
            `}
          >
            <img
              css={css`
                width: 100%;
              `}
              src={`/Polling-Day-Gif.gif`}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Create;
