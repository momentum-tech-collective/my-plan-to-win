/** @jsx jsx */
import { css, jsx } from "@emotion/core";
import Emoji from "a11y-react-emoji";
import React, { useEffect, useState } from "react";

import { ICategory } from "../../../server/api/db";
import { Swatch, textButtonCss, font } from "../design-constants";

type TaskProps = ICategory & {
  selected?: boolean;
  onClick?: () => void;
  altPresentation?: boolean;
};

const checkBoxCss = css`
  background: white;
  border: 2px solid black;
  width: 30px;
  height: 30px;
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const checkBoxSelectedCss = css`
  &:after {
    content: "✔️";
  }
`;

export const Task: React.FC<TaskProps> = ({
  size,
  name,
  description,
  selected,
  url,
  onClick,
  altPresentation
}) => {
  const sizeColor =
    size === "s" ? Swatch.green : size === "m" ? Swatch.purple : Swatch.red;

  return (
    <div
      onClick={onClick}
      css={css`
        border: 2px solid ${sizeColor};
        border-radius: 10px;
        cursor: pointer;
        background: ${selected === true ? sizeColor : undefined};
        color: ${selected === true ? "white" : undefined};
      `}
    >
      {size !== "s" && (
        <div
          css={css`
            background-image: url(${url
              ? url
              : size === "b"
              ? "https://i.imgur.com/UnzcIhI.png"
              : "https://i.imgur.com/5Mr9kyY.png"});
            background-size: cover;
            background-repeat: none;
            background-position: center;
            height: ${size === "b" ? 70 : 50}px;
            @media screen and (min-width: 1024px) {
              height: 100px;
            }
            position: relative;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
          `}
        >
          {typeof selected === "boolean" && (
            <div
              css={css`
            position: absolute;
            top: 10px;
            left: 10px;
            ${checkBoxCss}
            ${selected && checkBoxSelectedCss}
            border-color: ${sizeColor};
          `}
            />
          )}
        </div>
      )}
      <div
        css={css`
          padding: 15px;
        `}
      >
        {typeof selected === "boolean" && size === "s" && (
          <div
            css={css`
              ${checkBoxCss}
              ${selected &&
                checkBoxSelectedCss}
              margin-bottom: 10px;
              border-color: ${sizeColor};
            `}
          />
        )}
        <div
          css={css`
            ${font.mMedium}
            margin-bottom: 10px;
          `}
        >
          {name}
        </div>
        {description && (
          <p
            css={css`
              margin: 0;
              ${font.sRegular}
            `}
            dangerouslySetInnerHTML={{ __html: description }}
          />
        )}
      </div>
    </div>
  );
};

export const TaskMenu: React.FC<{
  name?: string;
  tasks: ICategory[];
  selectedTaskIds?: string[];
  selectTask: (categoryId: string) => void;
  deselectTask: (categoryId: string) => void;
  initiallyExpanded?: boolean;
  collapseOnSelect?: boolean;
  dontShowHideOption?: boolean;
  altPresentation?: boolean;
}> = ({
  name = "tasks",
  tasks = [],
  selectedTaskIds = [],
  selectTask,
  deselectTask,
  initiallyExpanded = true,
  collapseOnSelect = false,
  dontShowHideOption = false,
  altPresentation
}) => {
  const [expanded, setExpanded] = useState(initiallyExpanded);
  const [requestExpanded, setRequestExpanded] = useState<boolean>();

  useEffect(() => {
    if (collapseOnSelect && requestExpanded === undefined) {
      const selectedTasks = tasks.filter(category =>
        selectedTaskIds.includes(category.id)
      );
      if (selectedTasks.length > 0) {
        setExpanded(false);
      } else {
        setExpanded(true);
      }
    }
  }, [expanded, tasks, selectedTaskIds]);

  const collapsed =
    requestExpanded === false ||
    (requestExpanded === undefined && expanded === false);

  const categories = tasks.slice().map((t, index) => {
    const selected = selectedTaskIds.includes(t.id);
    return { ...t, selected, index };
  });

  if (collapsed) {
    categories.sort((a, b) =>
      a.selected === b.selected ? b.index - a.index : a.selected ? -1 : 1
    );
  }

  const nameToTaskColor = (name: string) => {
    if (name.includes("big")) {
      return Swatch.red;
    }

    if (name.includes("medium")) {
      return Swatch.purple;
    }

    if (name.includes("small")) {
      return Swatch.green;
    }

    return Swatch.red;
  };

  return (
    <div
      css={css`
        user-select: none;
      `}
    >
      {categories.slice(0, collapsed ? 1 : 1000).map(category => {
        return (
          <div
            key={category.id}
            css={css`
              margin-bottom: 10px;
            `}
          >
            <Task
              {...category}
              onClick={() => {
                if (!category.selected) {
                  selectTask(category.id);
                } else {
                  deselectTask(category.id);
                }
              }}
              altPresentation={altPresentation}
            />
          </div>
        );
      })}
      <div
        css={css`
          height: 1em;
        `}
      />
      {!dontShowHideOption && (
        <span
          css={css`
          ${textButtonCss}
          border-bottom: 2px solid ${nameToTaskColor(name)};
        `}
          onMouseDown={() =>
            void setRequestExpanded(e => (e === undefined ? !expanded : !e))
          }
        >
          {(expanded || expanded === undefined ? "Hide all" : "Show all") +
            " " +
            name}
        </span>
      )}
    </div>
  );
};

const taskIconCss = css`
  font-size: 1.8rem;
  float: right;
`;

const taskCategoryNameCss = css`
  ${font.lBold}
`;

const taskCategoryDescriptionCss = css`
  ${font.mMedium}
`;

export function BigTaskBlock() {
  return (
    <div
      css={css`
        color: ${Swatch.red};
      `}
    >
      <div css={taskIconCss}>
        <Emoji symbol="🏆" />
      </div>
      <div css={taskCategoryNameCss}>Big actions</div>
      <p css={taskCategoryDescriptionCss}>
        These will win us the election – prioritise one a day.
      </p>
    </div>
  );
}

export function MediumTaskBlock() {
  return (
    <div
      css={css`
        color: ${Swatch.purple};
      `}
    >
      <div css={taskIconCss}>
        <Emoji symbol="🎖" />
      </div>
      <div css={taskCategoryNameCss}>Medium actions</div>
      <p css={taskCategoryDescriptionCss}>
        Small effort, high impact. Choose one a day.
      </p>
    </div>
  );
}

export function SmallTaskBlock() {
  return (
    <div
      css={css`
        color: ${Swatch.green};
      `}
    >
      <div css={taskIconCss}>
        <Emoji symbol="👍" />
      </div>
      <div css={taskCategoryNameCss}>Small actions</div>
      <p css={taskCategoryDescriptionCss}>
        The building blocks of everything we do. Do at least one a day.
      </p>
    </div>
  );
}
