import {
  eachDayOfInterval,
  subDays,
  isSameDay,
  differenceInHours,
  differenceInMinutes,
  differenceInSeconds
} from "date-fns";

export const today = new Date();
export const yesterday = subDays(today, 1);
export const startDay = new Date(2019, 11, 1);
export const pollingDay = new Date(2019, 11, 12);

export const isTodayPollingDay = isSameDay(today, pollingDay);

export const daysComplete = eachDayOfInterval({
  start: startDay,
  end: yesterday
});

export const daysLeft = isTodayPollingDay ? [] : eachDayOfInterval({
  start: today,
  end: pollingDay
});

export const allDays = [...daysComplete, ...daysLeft];


export const VOTE_CATEGORY_ID = "26deabf0-145e-11ea-ac95-afb5b367201d";

export const getTimeUntilPollsClose = () => {
  const pollsCloseAt = new Date(2019, 11, 12, 22);
  const now = new Date();

  const hours = differenceInHours(pollsCloseAt, now);
  const minutes = differenceInMinutes(pollsCloseAt, now);
  const seconds =
    differenceInSeconds(pollsCloseAt, now) - minutes * 60;

  return {
    hours,
    minutes: minutes  - hours * 60,
    seconds
  };
};
