/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { subDays, differenceInDays } from "date-fns";
import { pollingDay } from "./constants";
import { pagePaddingHorizontal } from "../design-constants";
import { ReactElement, Fragment } from "react";

export const shareMessage =
  "Today is Election Day. I'm voting for Labour, and I'm campaigning too! Click the link to see what you can do.\n\n";

export const dayMessages = [
  {
    date: subDays(pollingDay, 11),
    content: (
      <span>
        Eleven days to go! Make sure you book as much time off work as possible.
      </span>
    )
  },
  {
    date: subDays(pollingDay, 10),
    content: (
      <span>
        Have you finished your plan yet? Make sure you do! We’ve only got 10
        days.
      </span>
    )
  },
  {
    date: subDays(pollingDay, 9),
    content: <span>Nine days to go! Have you booked time off work yet? </span>
  },
  {
    date: subDays(pollingDay, 8),
    content: (
      <span>
        With eight days to go, will you pledge to take the day off work on Dec.
        12?
      </span>
    )
  },
  {
    date: subDays(pollingDay, 7),
    content: (
      <span>
        Just one week to go! Make sure you’ve planned every day for the final
        week.
      </span>
    )
  },
  {
    date: subDays(pollingDay, 6),
    content: <span>Plan something big every day for the next six days.</span>
  },
  {
    date: subDays(pollingDay, 5),
    content: <span>The final weekend. We need you to give it everything.</span>
  },
  {
    date: subDays(pollingDay, 4),
    content: (
      <span>
        The final Sunday before the election. Can you go canvassing today?
      </span>
    )
  },
  {
    date: subDays(pollingDay, 3),
    content: (
      <span>Just three days to go! This week will go down in history.</span>
    )
  },
  {
    date: subDays(pollingDay, 2),
    content: <span>Only two days to go! Every hour counts now. </span>
  },
  {
    date: subDays(pollingDay, 1),
    content: <span>This is it. One day to go.</span>
  },
];

const bannerCss = css`
  ${pagePaddingHorizontal}
  padding: 0;
  overflow: hidden;
`;

const imageBannerCss = css`
  display: block;
  width: 100%;
`;

interface ICheckPoint {
  id: string;
  date: Date;
  content: ReactElement;
}
export const checkpoints: Array<ICheckPoint> = [
  {
    id: "we_can_do_it_again",
    date: subDays(pollingDay, 11),
    content: (
      <div key={1} css={bannerCss}>
        <img
          src="https://i.imgur.com/sEAXOxk.png"
          css={imageBannerCss}
          alt="We can do it again!"
        />
      </div>
    )
  },
  {
    id: "lets_do_this",
    date: subDays(pollingDay, 8),
    content: (
      <div key={2} css={bannerCss}>
        <img
          src="https://i.imgur.com/nZK7VhD.png"
          css={imageBannerCss}
          alt="Let's do this!"
        />
      </div>
    )
  },
  {
    id: "one_last_push",
    date: subDays(pollingDay, 6),
    content: (
      <div key={3} css={bannerCss}>
        <img
          src="https://i.imgur.com/FQLxZk0.png"
          css={imageBannerCss}
          alt="One last push!"
        />
      </div>
    )
  },
  {
    id: "this_is_it",
    date: subDays(pollingDay, 3),
    content: (
      <div key={4} css={bannerCss}>
        <img
          src="https://i.imgur.com/v9FYN3t.png"
          css={imageBannerCss}
          alt="This Is It"
        />
      </div>
    )
  },
  {
    id: "it_all_comes_down_to_this",
    date: subDays(pollingDay, 1),
    content: (
      <div key={5} css={bannerCss}>
        <img
          src="https://i.imgur.com/x1zoyK8.png"
          css={imageBannerCss}
          alt="It all comes down to this"
        />
      </div>
    )
  }
];

interface ICheckPointsByDaysLeft {
  [noDaysLeft: number]: ICheckPoint;
}
export const checkpointsByDaysLeft = checkpoints.reduce((acc, cur) => {
  const noDaysLeft = differenceInDays(pollingDay, cur.date);
  acc[noDaysLeft] = cur;

  return acc;
}, {} as ICheckPointsByDaysLeft);
