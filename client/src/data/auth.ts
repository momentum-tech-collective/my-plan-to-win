import Cookies from "js-cookie";
import { useCookie } from "@use-hook/use-cookie";

const USER_ID_COOKIE_NAME = "anonymous_session";

export function useUserId() {
  const [userId] = useCookie(USER_ID_COOKIE_NAME);

  return userId as string | null;
}

export function signOut() {
  Cookies.remove(USER_ID_COOKIE_NAME);
  window.location.href = "/";
}
