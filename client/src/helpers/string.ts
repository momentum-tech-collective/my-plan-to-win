export const plural = (n: number, label: string, suffix: string = "s") =>
  n + " " + label + (n > 1 ? suffix : "");
