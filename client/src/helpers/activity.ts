import { ICategory, IUser, IActivity } from "../../../server/api/db";
import { useEffect, useState, Dispatch, SetStateAction } from "react";
import { getUserByPublicKey } from "../api";

export const hasUserReachedActivityMaxCount = (
  categoryId: string,
  categories: ICategory[],
  activities: IActivity[]
) => {
  const category = pluckActivityCategory(categoryId, categories);

  if (category.max_count === null) {
    return false;
  }

  const maxCount = category.max_count;
  const currentCount = activities.filter(
    activity => activity.category_id === categoryId
  ).length;

  return currentCount >= maxCount;
};

export const pluckActivityCategory = (
  categoryId: string,
  categories: ICategory[]
) => {
  return categories.find(({ id }) => id === categoryId) as ICategory;
};

// TODO: Persist this across views...
export function usePublicUser(userPublicKey: string) {
  return useApiState<IUser | null>(
    null,
    set => {
      getUserByPublicKey(userPublicKey).then(result => {
        set(result.data);
      });
    },
    [userPublicKey]
  );
}

export function useApiState<T>(
  initial: T,
  apiCall: (setState: Dispatch<SetStateAction<T>>) => void,
  cache: any[] = []
): [T, () => void, Dispatch<SetStateAction<T>>] {
  const [state, setState] = useState<T>(initial);
  const query = () => {
    try {
      return apiCall(setState);
    } catch (e) {
      return console.log(e);
    }
  };
  useEffect(() => {
    query();
  }, cache);

  return [state, query, setState];
}
