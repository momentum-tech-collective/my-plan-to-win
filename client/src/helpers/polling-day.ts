import { ICategory } from "../../../server/api/db";

export const reorderTasksInImportance = (tasks: Array<ICategory>) => {
  const mostImportantTaskIdsInOrder = [
    "26deabf0-145e-11ea-ac95-afb5b367201d",
    "26de5dd0-145e-11ea-ac95-afb5b367201d",
    "26deabf2-145e-11ea-ac95-afb5b367201d",
    "26deabf1-145e-11ea-ac95-afb5b367201d",
    "361d7700-1c64-11ea-978f-2e728ce88125",
    "26deabf4-145e-11ea-ac95-afb5b367201d"
  ];

  const mostImportantTasks: Array<ICategory> = [];

  mostImportantTaskIdsInOrder.forEach(taskId => {
    const importantTask = tasks.find(({ id }) => id === taskId);

    importantTask && mostImportantTasks.push(importantTask);
  });

  const lessImportantTasks = tasks.filter(
    ({ id }) => !mostImportantTaskIdsInOrder.includes(id)
  );

  return [...mostImportantTasks, ...lessImportantTasks];
};
