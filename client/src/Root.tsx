import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import * as routes from "./routes";
import LoadingView from "./views/LoadingView";
import useMedia from "./helpers/use-media";
import TabBar from "./components/TabBar";
import { Provider } from "./context/store";

export default function() {
  const isDesktop = useMedia(["(min-width: 1024px)"], [true], false);

  return (
    <React.Suspense fallback={<LoadingView />}>
      <Provider>
        <TabBar currentSite="mptw" />
        <Switch>
          <Route
            exact
            path={`/get-updates`}
            component={routes.GetUpdatesView}
          />
          <Route exact path={`/login/:reason?`} component={routes.LoginView} />
          <Route exact path={`/save`} component={routes.SavePlanView} />
          <Route exact path={`/share`} component={routes.SharePlanView} />
          <Route exact path={`/pick-task`} component={routes.PickTaskView} />
          <Route
            path={`/create/:OTP?`}
            render={props =>
              !isDesktop ? (
                <Redirect to="/" />
              ) : (
                <routes.CreatePlanView {...props} />
              )
            }
          />
          <Route
            exact
            path={`/plan/:userId`}
            component={routes.PublicPlanView}
          />
          <Route
            exact
            path={`/plans/:userId`}
            component={routes.PublicPlanView}
          />
          <Route
            exact
            path={`/sign-in/:OTP`}
            component={routes.UserSignInView}
          />
          <Route
            exact
            path={`/`}
            render={() =>
              isDesktop ? <Redirect to="/create" /> : <routes.LandingView />
            }
          />
          <Redirect to="/" />
        </Switch>
      </Provider>
    </React.Suspense>
  );
}
