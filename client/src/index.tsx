import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { analytics, AnalyticsProvider } from "./data/analytics";
import Root from "./Root";
import { Router } from "react-router-dom";
const h = createBrowserHistory();

analytics.logView(window.location.pathname);

h.listen(l => {
  analytics.logView(l.pathname);
});

ReactDOM.render(
  <AnalyticsProvider value={analytics}>
    <Router history={h}>
      <Root />
    </Router>
  </AnalyticsProvider>,
  document.getElementById("root")
);
