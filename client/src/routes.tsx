import React from "react";
export type LazyRouteDef = ReturnType<typeof createLazy>;
export type LazyRouteDefWithData = ReturnType<typeof createLazy>;

/** Wrap a route def in react.lazy and add helper for preloading the route and one for preloading its data */
function createLazy<T extends React.ComponentType<any>>(
  moduleFn: () => Promise<{ default: T }>
) {
  let Eager: T | undefined;
  const Lazy = React.lazy(moduleFn);

  const Component = (props: any) => {
    if (Eager) {
      return <Eager {...props} />;
    }
    return <Lazy {...props} />;
  };

  return Object.assign(Component, {
    preloadModule: async () => {
      const { default: component } = await moduleFn();
      Eager = component;
    }
  });
}

/** Declare the routes a screen might navigate to so that they can be optimistically loaded */
export const usePreloadRoutes = (...routes: (LazyRouteDef | undefined)[]) => {
  React.useEffect(() => {
    for (const route of routes) {
      if (route) {
        route.preloadModule();
      }
    }
  }, [routes]);
};

export const LandingView = createLazy(() =>
  import(/* webpackChunkName: 'view-LandingView' */ "./views/LandingView")
);

export const PublicPlanView = createLazy(() =>
  import(/* webpackChunkName: 'view-PublicPlanView' */ "./views/PublicPlanView")
);

export const UserSignInView = createLazy(() =>
  import(/* webpackChunkName: 'view-UserSignInView' */ "./views/UserSignInView")
);

export const CreatePlanView = createLazy(() =>
  import(/* webpackChunkName: 'view-CreatePlanView' */ "./views/CreatePlanView")
);

export const PickTaskView = createLazy(() =>
  import(/* webpackChunkName: 'view-PickTaskView' */ "./views/PickTaskView")
);

export const LoginView = createLazy(() =>
  import(/* webpackChunkName: 'view-LoginView' */ "./views/LoginView")
);

export const SavePlanView = createLazy(() =>
  import(/* webpackChunkName: 'view-SavePlanView' */ "./views/SavePlanView")
);

export const GetUpdatesView = createLazy(() =>
  import(/* webpackChunkName: 'view-GetUpdatesView' */ "./views/GetUpdatesView")
);

export const SharePlanView = createLazy(() =>
  import(/* webpackChunkName: 'view-SharePlanView' */ "./views/SharePlanView")
);
