import { Dispatch } from "react";
import { useCookie } from "@use-hook/use-cookie";

import {
  getUser,
  getActivities,
  getCategories,
  deleteActivity,
  createActivity,
  BasicUserData,
  postBasicUserData,
  getAllActivityCount,
  checkToken
} from "../api";
import { Action } from "./reducer";
import { IActivity } from "../../../server/api/db";
const USER_ID_COOKIE_NAME = "anonymous_session";

export const useFetchUser = (dispatch: Dispatch<Action>) => {
  return (userId: string | undefined) => {
    if (!userId) {
      return;
    }
    dispatch({ type: "GET_USER_START", payload: userId });

    getUser(userId)
      .then(({ data: user }) => {
        dispatch({ type: "GET_USER_SUCCESS", payload: user });
      })
      .catch(error => {
        dispatch({ type: "GET_USER_FAILURE" });
      });
  };
};

export const useFetchActivities = (dispatch: Dispatch<Action>) => {
  return (userId: string | undefined) => {
    if (!userId) {
      return;
    }

    getActivities(userId)
      .then(({ data: activities }) => {
        dispatch({ type: "GET_ACTIVITIES_SUCCESS", payload: activities });
      })
      .catch(error => {
        dispatch({ type: "GET_ACTIVITIES_FAILURE" });
      });
  };
};

export const useFetchPublicActivities = (dispatch: Dispatch<Action>) => {
  return (userId: string) => {
    getActivities(userId)
      .then(({ data: activities }) => {
        dispatch({
          type: "GET_PUBLIC_ACTIVITIES_SUCCESS",
          payload: activities
        });
      })
      .catch(error => {
        dispatch({ type: "GET_PUBLIC_ACTIVITIES_FAILURE" });
      });
  };
};

export const useFetchCategories = (dispatch: Dispatch<Action>) => {
  return () => {
    getCategories()
      .then(({ data: categories }) => {
        dispatch({
          type: "GET_CATEGORIES_SUCCESS",
          payload: categories
        });
      })
      .catch(error => {
        dispatch({ type: "GET_CATEGORIES_FAILURE" });
      });
  };
};

export const useDeleteActivity = (dispatch: Dispatch<Action>) => {
  return (activityId: string) => {
    dispatch({
      type: "DELETE_ACTIVITY_START",
      payload: activityId
    });

    deleteActivity(activityId)
      .then(() => {
        dispatch({
          type: "DELETE_ACTIVITY_SUCCESS"
        });
      })
      .catch(error => {
        dispatch({ type: "DELETE_ACTIVITY_FAILURE" });
      });
  };
};

export const useCreateActivity = (dispatch: Dispatch<Action>) => {
  const fetchActivities = useFetchActivities(dispatch);

  return (activity: IActivity) => {
    dispatch({
      type: "CREATE_ACTIVITY_START",
      payload: activity
    });

    createActivity(activity)
      .then(() => {
        dispatch({
          type: "CREATE_ACTIVITY_SUCCESS"
        });

        fetchActivities(activity.user_id);
      })
      .catch(error => {
        dispatch({ type: "DELETE_ACTIVITY_FAILURE" });
      });
  };
};

export const useSignUp = (dispatch: Dispatch<Action>) => {
  const fetchUser = useFetchUser(dispatch);

  return (user: BasicUserData) => {
    dispatch({ type: "SIGN_UP_START" });

    postBasicUserData(user)
      .then(() => {
        const userId = user.id;

        fetchUser(userId);
      })
      .catch(error => {
        dispatch({ type: "SIGN_UP_FAILURE", payload: error.message });
      });
  };
};

export const useFetchActivityCount = (dispatch: Dispatch<Action>) => {
  return () => {
    getAllActivityCount().then(result => {
      dispatch({
        type: "FETCH_ACTIVITY_COUNT_SUCCESS",
        payload: result.data.count
      });
    });
  };
};

export const useCheckToken = (dispatch: Dispatch<Action>) => {
  const [userIdCookieValue, setUserIdCookie] = useCookie(USER_ID_COOKIE_NAME);
  const fetchUser = useFetchUser(dispatch);

  return (OTP: string | undefined) => {
    if (!OTP) {
      return;
    }
    dispatch({
      type: "CHECK_TOKEN_START"
    });

    checkToken(OTP)
      .then(result => {
        const userId = result.data;

        if (!userId) {
          return Promise.reject("No user id");
        }

        setUserIdCookie(userId);

        dispatch({
          type: "CHECK_TOKEN_SUCCESS",
          payload: userId
        });

        fetchUser(userId);
      })
      .catch(() => {
        return dispatch({
          type: "CHECK_TOKEN_FAILURE"
        });
      });
  };
};
