import React, { createContext, useContext, useReducer, useEffect } from "react";
import { useCookie } from "@use-hook/use-cookie";
import uuidv1 from "uuid/v1";

import { Action, initialState, reducer } from "./reducer";
import {
  useFetchUser,
  useFetchActivities,
  useFetchCategories,
  useFetchActivityCount,
  useCreateActivity
} from "./actions";
import { VOTE_CATEGORY_ID, pollingDay } from "../data/constants";

const USER_ID_COOKIE_NAME = "anonymous_session";

const stateCtx = createContext(initialState);
const { Provider: StateProvider } = stateCtx;
const dispatchCtx = createContext((() => 0) as React.Dispatch<Action>);
const { Provider: DispatchProvider } = dispatchCtx;

export const Provider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { userId: userIdFromStore } = state;
  const fetchUser = useFetchUser(dispatch);
  const fetchActivities = useFetchActivities(dispatch);
  const createActivity = useCreateActivity(dispatch)
  const fetchCategories = useFetchCategories(dispatch);
  const fetchActivityCount = useFetchActivityCount(dispatch);
  const [userIdCookieValue, setUserIdCookie] = useCookie(USER_ID_COOKIE_NAME);

  useEffect(() => {
    if (!userIdCookieValue) {
      const newUserId = uuidv1();
      console.log(`Setting up new user ID ${newUserId}`);
      setUserIdCookie(newUserId);
      // New users should have VOTE category by default
      createActivity({
        id: uuidv1(),
        category_id: VOTE_CATEGORY_ID,
        user_id: newUserId,
        date: pollingDay
      })
    } else {
      console.log(`User ID from cookie ${userIdCookieValue}`);
    }
  }, []);

  useEffect(() => {
    fetchUser(userIdCookieValue);
  }, [userIdCookieValue]);

  useEffect(() => {
    fetchActivities(userIdFromStore);
    fetchCategories();
    fetchActivityCount();
  }, [userIdFromStore]);

  return (
    <DispatchProvider value={dispatch}>
      <StateProvider value={state}>{children}</StateProvider>
    </DispatchProvider>
  );
};

export const useDispatch = () => {
  const dispatch = useContext(dispatchCtx);

  return dispatch;
};

export const useGlobalState = () => {
  const state = useContext(stateCtx);

  return state;
};
