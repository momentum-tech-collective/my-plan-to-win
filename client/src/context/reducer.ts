import {
  IUser,
  IActivity,
  ICategory,
  IActivityCount
} from "../../../server/api/db";

export interface State {
  userId: string | undefined;
  user: IUser | undefined;
  isLoggedIn: boolean;
  activities: IActivity[];
  categories: ICategory[];
  publicActivities: IActivity[];
  activityCount: number;
  userIdFromOTP: string | undefined | boolean;
  signUpError: string | undefined;
}

export const initialState: State = {
  userId: undefined,
  user: undefined,
  isLoggedIn: false,
  activities: [],
  categories: [],
  publicActivities: [],
  activityCount: 0,
  userIdFromOTP: undefined,
  signUpError: undefined
};

export type Action =
  | { type: "GET_USER_START"; payload: string }
  | { type: "GET_USER_SUCCESS"; payload: IUser }
  | { type: "GET_USER_FAILURE" }
  | { type: "GET_ACTIVITIES_SUCCESS"; payload: IActivity[] }
  | { type: "GET_ACTIVITIES_FAILURE" }
  | { type: "DELETE_ACTIVITY_START"; payload: string }
  | { type: "DELETE_ACTIVITY_SUCCESS" }
  | { type: "DELETE_ACTIVITY_FAILURE" }
  | { type: "CREATE_ACTIVITY_START"; payload: IActivity }
  | { type: "CREATE_ACTIVITY_SUCCESS" }
  | { type: "CREATE_ACTIVITY_FAILURE" }
  | { type: "GET_PUBLIC_ACTIVITIES_SUCCESS"; payload: IActivity[] }
  | { type: "GET_PUBLIC_ACTIVITIES_FAILURE" }
  | { type: "GET_CATEGORIES_SUCCESS"; payload: ICategory[] }
  | { type: "GET_CATEGORIES_FAILURE" }
  | { type: "FETCH_ACTIVITY_COUNT_SUCCESS"; payload: number }
  | { type: "CHECK_TOKEN_START" }
  | { type: "CHECK_TOKEN_SUCCESS"; payload: string }
  | { type: "CHECK_TOKEN_FAILURE" }
  | { type: "SIGN_UP_START" }
  | { type: "SIGN_UP_FAILURE", payload: string };

export const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "GET_USER_START": {
      const userId = action.payload;

      return {
        ...state,
        userId
      };
    }
    case "GET_USER_SUCCESS": {
      const user = action.payload;
      const isLoggedIn = user ? user.name !== "unknown" : false;

      return {
        ...state,
        user: action.payload,
        isLoggedIn
      };
    }
    case "GET_USER_FAILURE": {
      return {
        ...state,
        isLoggedIn: false
      };
    }
    case "GET_ACTIVITIES_SUCCESS": {
      return {
        ...state,
        activities: action.payload
      };
    }
    case "CREATE_ACTIVITY_START": {
      return {
        ...state,
        activities: [...state.activities, action.payload]
      };
    }
    case "DELETE_ACTIVITY_START": {
      const idToDelete = action.payload;

      const activities = state.activities.filter(
        activity => activity.id !== idToDelete
      );

      return {
        ...state,
        activities
      };
    }
    case "GET_PUBLIC_ACTIVITIES_SUCCESS": {
      return {
        ...state,
        publicActivities: action.payload
      };
    }
    case "GET_CATEGORIES_SUCCESS": {
      return {
        ...state,
        categories: action.payload
      };
    }
    case "SIGN_UP_START": {
      return {
        ...state,
        signUpError: undefined
      };
    }
    case "SIGN_UP_FAILURE": {
      return {
        ...state,
        signUpError: action.payload
      };
    }
    case "CHECK_TOKEN_START": {
      return {
        ...state,
        userIdFromOTP: undefined
      };
    }
    case "CHECK_TOKEN_SUCCESS": {
      return {
        ...state,
        userIdFromOTP: action.payload,
        userId: action.payload
      };
    }
    case "CHECK_TOKEN_FAILURE": {
      return {
        ...state,
        userIdFromOTP: false
      };
    }
    case "FETCH_ACTIVITY_COUNT_SUCCESS": {
      return {
        ...state,
        activityCount: action.payload
      };
    }
    default: {
      return {
        ...state
      };
    }
  }
};
