import axios, { AxiosResponse } from "axios";

import { IActivity, uuid, IUser, ICategory, IActivityCount } from "../../server/api/db";

export type BasicUserData = {
  id: uuid;
  name: string;
  email: string;
} & Partial<IUser>;

export function getEndpoint(): string {
  return process.env.REACT_APP_API_ENDPOINT || "http://localhost:3000";
}

export function getCategories(): Promise<AxiosResponse<ICategory[]>> {
  return axios.get(`${getEndpoint()}/categories/`);
}

export function createActivity(
  record: IActivity
): Promise<AxiosResponse<null>> {
  return axios.post(`${getEndpoint()}/activity`, record);
}

export function getActivities(
  userId: string
): Promise<AxiosResponse<IActivity[]>> {
  return axios.get(`${getEndpoint()}/activities/${userId}`);
}

export function getUserByPublicKey(
  userPublicKey: string
): Promise<AxiosResponse<IUser>> {
  return axios.get(`${getEndpoint()}/publicuser/${userPublicKey}`);
}

export function checkToken(token: string): Promise<AxiosResponse<string>> {
  return axios.get(`${getEndpoint()}/usertype/${token}`);
}

export function postBasicUserData(
  record: BasicUserData
): Promise<AxiosResponse<null>> {
  return axios.post(`${getEndpoint()}/send-confirm-email`, record);
}

export function requestLoginEmail(email: string): Promise<AxiosResponse<null>> {
  return axios.get(`${getEndpoint()}/email-login/${email}`);
}

export function patchUserData(
  id: string,
  record: Partial<IUser>
): Promise<AxiosResponse<null>> {
  return axios.patch(`${getEndpoint()}/user/${id}`, record);
}

export function deleteActivity(activityId: string) {
  return axios.delete(`${getEndpoint()}/activity/${activityId}`);
}

export function getUser(userId: string) {
  return axios.get(`${getEndpoint()}/user/${userId}`);
}

export function getAllActivityCount(): Promise<AxiosResponse<IActivityCount>> {
  return axios.get(`${getEndpoint()}/activity/count`)
}