/** @jsx jsx */
import { css, jsx, Global } from "@emotion/core";
import { Fragment } from "react";
import emotionNormalize from "emotion-normalize";

export const Swatch = {
  background: "#FAFAFA",
  pink: "#FFD3FD",
  red: "#E20613",
  purple: "#B1048E",
  green: "#04B14A",
  midGrey: "#F0F0F0",
  darkGrey: "#ACACAC",
  black: "#000000",
  darkPink: "#FF0F80",
  white: "#FFFFFF"
};

const defaultLetterSpacing = css`
  letter-spacing: -0.01em;
`;

function calculateRem(size: number) {
  const remSize = size / 16;
  return `${remSize}rem;`;
}

export const font = {
  xlBoldItalic: css`
    font-style: italic;
    font-weight: bold;
    font-size: 30px;
    line-height: 75%;
    ${defaultLetterSpacing}
    text-transform: uppercase;
  `,
  lBold: css`
    font-style: normal;
    font-weight: 500;
    font-size: ${calculateRem(28)};
    line-height: 100%;
    ${defaultLetterSpacing}
  `,
  lRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 28px;
    line-height: 100%;
    ${defaultLetterSpacing}
  `,
  mBold: css`
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 18px;
    ${defaultLetterSpacing}
  `,
  mMedium: css`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 20px;
    ${defaultLetterSpacing}
  `,
  mRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 20px;
    ${defaultLetterSpacing}
  `,
  sMedium: css`
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 16px;
    ${defaultLetterSpacing}
  `,
  sRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    line-height: 17px;
    ${defaultLetterSpacing}
  `,
  xsRegular: css`
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    letter-spacing: 0.05em;
    text-transform: uppercase;
  `
};

export const typeTitle = css`
  font-size: 1.8rem;
`;

export const typeSubtitle = css`
  font-size: 1.5rem;
`;

export const typeLargeCopy = css`
  font-size: 1.3rem;
`;

export const typeCopy = css`
  font-size: 1rem;
`;

export const typeSmallCopy = css`
  font-size: 0.8rem;
`;

export const layoutFlexColumn = css`
  display: flex;
  flex-direction: column;
`;

export const layoutFlexRow = css`
  display: flex;
  flex-direction: row;
`;

export const Layout: React.FC = ({ children }) => (
  <Fragment>
    <Global
      styles={css`
        ${emotionNormalize}

        html {
          height: 100%;
        }
        body {
          min-height: 100%;
        }
        html,
        body {
          font-family: "neue-haas-grotesk-display";
          font-size: 16px;
          overflow-wrap: break-word;
        }

        // Set box-sizing to border-box universally
        // See https://css-tricks.com/inheriting-box-sizing-probably-slightly-better-best-practice/
        html {
          box-sizing: border-box;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          font-weight: 600;
          font-size: normal;
          margin: 0;
        }

        a {
          color: inherit;
        }

        *,
        *:before,
        *:after {
          box-sizing: inherit;
        }

        .description-link {
          color: inherit;
        }
      `}
    />
    <div>{children}</div>
  </Fragment>
);

export const padding = {
  pageHorizontal: "20px",
  pageVertical: "20px"
};

export const pagePaddingHorizontal = css`
  padding-left: ${padding.pageHorizontal};
  padding-right: ${padding.pageHorizontal};
  width: 100%;
  margin-left: auto;
  margin-right: auto;
`;

export const pagePaddingHorizontalLimited = css`
  ${pagePaddingHorizontal}
  max-width: 600px;
`;

export const PaddingHorizontal: React.FC = ({ children }) => (
  <div css={pagePaddingHorizontal}>{children}</div>
);

export const PaddingVertical: React.FC = ({ children }) => (
  <div
    css={css`
      padding-top: ${padding.pageVertical};
      padding-bottom: ${padding.pageVertical};
      width: 100%;
    `}
  >
    {children}
  </div>
);

export const textButtonCss = css`
  font-weight: bold;
  letter-spacing: -0.03em;
  cursor: pointer;
  border-bottom: 2px solid ${Swatch.red};
`;
